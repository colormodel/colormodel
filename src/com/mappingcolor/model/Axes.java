package com.mappingcolor.model;

import com.jogamp.common.nio.Buffers;

import javax.media.opengl.GL2;
import java.awt.Color;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class Axes {
    public static final MyColor northAxisColor = MyColor.fromColor(new Color(255, 255, 0)); //yellow
    public static final MyColor southAxisColor = MyColor.fromColor(new Color(150, 150, 255)); // blue
    public static final MyColor westAxisColor = MyColor.fromColor(new Color(150, 255, 150)); // green
    public static final MyColor eastAxisColor = MyColor.fromColor(new Color(255, 150, 150)); // red
    public static final MyColor valueAxisColor = MyColor.fromColor(new Color(255, 255, 255)); //white

    private static final float AXIS_LENGTH = Camera.Z_FAR;

    private static IntBuffer vbo = IntBuffer.allocate(1);

    public static void initialize(GL2 gl, int id) {
        int totalBufferSize = 6 * 3 * Buffers.SIZEOF_FLOAT + 6 * 3 * Buffers.SIZEOF_FLOAT;
        FloatBuffer vertices = Buffers.newDirectFloatBuffer(totalBufferSize);

        gl.glGenBuffers(1, vbo);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbo.get(0));
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, totalBufferSize, null, GL2.GL_STATIC_DRAW);

        //x
        vertices.put(0); vertices.put(0); vertices.put(0);
        vertices.put(westAxisColor.getRedf()); vertices.put(westAxisColor.getGreenf()); vertices.put(westAxisColor.getBluef());
        vertices.put(-AXIS_LENGTH); vertices.put(0); vertices.put(0);
        vertices.put(westAxisColor.getRedf()); vertices.put(westAxisColor.getGreenf()); vertices.put(westAxisColor.getBluef());

        vertices.put(0); vertices.put(0); vertices.put(0);
        vertices.put(eastAxisColor.getRedf()); vertices.put(eastAxisColor.getGreenf()); vertices.put(eastAxisColor.getBluef());
        vertices.put(AXIS_LENGTH); vertices.put(0); vertices.put(0);
        vertices.put(eastAxisColor.getRedf()); vertices.put(eastAxisColor.getGreenf()); vertices.put(eastAxisColor.getBluef());

        //y
        vertices.put(0); vertices.put(AXIS_LENGTH); vertices.put(0);
        vertices.put(northAxisColor.getRedf()); vertices.put(northAxisColor.getGreenf()); vertices.put(northAxisColor.getBluef());
        vertices.put(0); vertices.put(0); vertices.put(0);
        vertices.put(northAxisColor.getRedf()); vertices.put(northAxisColor.getGreenf()); vertices.put(northAxisColor.getBluef());

        vertices.put(0); vertices.put(-AXIS_LENGTH); vertices.put(0);
        vertices.put(southAxisColor.getRedf()); vertices.put(southAxisColor.getGreenf()); vertices.put(southAxisColor.getBluef());
        vertices.put(0); vertices.put(0); vertices.put(0);
        vertices.put(southAxisColor.getRedf()); vertices.put(southAxisColor.getGreenf()); vertices.put(southAxisColor.getBluef());

        //z
        vertices.put(0); vertices.put(0); vertices.put(-AXIS_LENGTH);
        vertices.put(valueAxisColor.getRedf()); vertices.put(valueAxisColor.getGreenf()); vertices.put(valueAxisColor.getBluef());
        vertices.put(0); vertices.put(0); vertices.put(AXIS_LENGTH);
        vertices.put(valueAxisColor.getRedf()); vertices.put(valueAxisColor.getGreenf()); vertices.put(valueAxisColor.getBluef());

        vertices.rewind();

        gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, 0, vertices.capacity() * Buffers.SIZEOF_FLOAT, vertices);
    }

    public static void drawVBO(GL2 gl) {
        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL2.GL_COLOR_ARRAY);
        gl.glCullFace(GL2.GL_BACK);

        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbo.get(0));
        gl.glVertexPointer(VBOHelper.POINTS_PER_VERTEX, GL2.GL_FLOAT, 7 * Buffers.SIZEOF_FLOAT, 0);
        gl.glColorPointer(VBOHelper.POINTS_PER_COLOR, GL2.GL_FLOAT, 7 * Buffers.SIZEOF_FLOAT, 3 * Buffers.SIZEOF_FLOAT);

        gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINES);
        gl.glDrawArrays(GL2.GL_LINE, 0, VBOHelper.shapeVertices.size());

        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL2.GL_COLOR_ARRAY);
    }

    public static void draw(GL2 gl)
    {
        gl.glPushMatrix();
        // x
        gl.glColor3f(westAxisColor.getRedf(), westAxisColor.getGreenf(), westAxisColor.getBluef());
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(-AXIS_LENGTH, 0, 0);
        gl.glEnd();

        gl.glColor3f(eastAxisColor.getRedf(), eastAxisColor.getGreenf(), eastAxisColor.getBluef());
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(AXIS_LENGTH, 0, 0);
        gl.glEnd();

        // y
        gl.glColor3f(northAxisColor.getRedf(), northAxisColor.getGreenf(), northAxisColor.getBluef());
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3f(0, AXIS_LENGTH, 0);
        gl.glVertex3f(0, 0, 0);
        gl.glEnd();

        gl.glColor3f(southAxisColor.getRedf(), southAxisColor.getGreenf(), southAxisColor.getBluef());
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3f(0, -AXIS_LENGTH, 0);
        gl.glVertex3f(0, 0, 0);
        gl.glEnd();

        // z
        gl.glColor3f(valueAxisColor.getRedf(), valueAxisColor.getGreenf(), valueAxisColor.getBluef());
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3f(0, 0, -AXIS_LENGTH);
        gl.glVertex3f(0, 0, AXIS_LENGTH);
        gl.glEnd();

        gl.glPopMatrix();
    }
}
