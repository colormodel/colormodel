package com.mappingcolor.model;

import com.jogamp.opengl.math.FloatUtil;
import com.mappingcolor.model.shape.Circle;

abstract public class VectorMath
{
    public static final float DEG_TO_RAD = FloatUtil.PI / 180;
    public static final float RAD_TO_DEG = 180 / FloatUtil.PI;

    private static float[][] MX = new float[3][3];
    private static float[][] MY = new float[3][3];
    private static float[][] MZ = new float[3][3];

    private VectorMath() {} // explicitly states that you cannot instantiate this class

    public static float raySphereIntersect(Vector rayOrigin, Vector rayVector, Vector spherePosition)
    {
        Vector Q = new Vector(spherePosition);
        Q = Q.subtract(spherePosition);


        float c = Q.magnitude();
        float v = Q.dot(rayVector);

        float d = Circle.getRadius() * Circle.getRadius() - (c*c - v*v);

        // if there is no intersection, return -1
        if (d < 0)
            return -1;

        // return the distance to the [first] intersecting point on the sphere
        return v - FloatUtil.sqrt(d);
    }

    // the angle parameters are in degrees (not radians)
    public static Vector rotate(Vector position, float xAngle, float yAngle, float zAngle)
    {
        if(xAngle != 0) {
            transformX(xAngle);
            multiplication(position, MX);
        }
        if(yAngle != 0) {
            transformY(yAngle);
            multiplication(position, MY);
        }
        if(zAngle != 0) {
            transformZ(zAngle);
            multiplication(position, MZ);
        }

        return position;
    }

    private static void transformX(float xAngle)
    {
        float radians = xAngle * DEG_TO_RAD;
        MX[0][0] = 1;
        MX[1][1] = FloatUtil.cos(radians);
        MX[1][2] = FloatUtil.sin(radians);
        MX[2][1] = -FloatUtil.sin(radians);
        MX[2][2] = FloatUtil.cos(radians);
    }

    private static void transformY(float yAngle)
    {
        float radians = yAngle * DEG_TO_RAD;
        MY[0][0] = FloatUtil.cos(radians);
        MY[0][2] = FloatUtil.sin(radians);
        MY[1][1] = 1;
        MY[2][0] = FloatUtil.sin(radians);
        MY[2][2] = FloatUtil.cos(radians);
    }

    private static void transformZ(float zAngle)
    {
        float radians = zAngle *  DEG_TO_RAD;
        MZ[0][0] = FloatUtil.cos(radians);
        MZ[0][1] = FloatUtil.sin(radians);
        MZ[1][0] = -FloatUtil.sin(radians);
        MZ[1][1] = FloatUtil.cos(radians);
        MZ[2][2] = 1;
    }

    private static void multiplication(Vector vector, float[][] M)
    {
        float[] v = vector.getArray();
        float[] u = new float[3];
        for(int j = 0; j < v.length; j++)
        {
            u[j] = 0;
            for(int k=0; k<3; k++)
                u[j] += v[k] * M[k][j];
        }

        vector.set(u);
    }
}