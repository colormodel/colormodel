package com.mappingcolor.model;

import com.jogamp.opengl.math.FloatUtil;

import java.awt.Color;
import java.util.Comparator;

public class MyColor
{
    public enum ModelType {VCH, LAB, RGB}

    public static final MyColor C = MyColor.fromLAB(62, -44, -50);
    public static final MyColor M = MyColor.fromLAB(52, 81, -7);
    public static final MyColor Y = MyColor.fromLAB(95, -6, 95);
    public static final MyColor K = MyColor.fromLAB(0, 0, 0);
    public static final MyColor W = MyColor.fromLAB(100, 0, 0);
    public static final MyColor[] CMYKc = new MyColor[]{C,M,Y,K,W};

    private static final CountComparator CC = new CountComparator();

    public static final int MAX_VALUE = 100;
    public static final int MAX_CHROMA = 175;
    public static final int MAX_HUE = 360; // full circle in degrees

    private int rgbId;
    private int[] RGB;
    private float[] RGBf;
    private float[] VCH;
    private float[] LAB;
    private float[] CMYK;
    private boolean existsInRGB;
    private boolean existsInCMYK;

    private int count; // keeps colorSelected of how many times a color occurs in an image
    private String name = "";

    public static MyColor fromVCH(float value, float chroma, float hue)
    {
        return new MyColor(value, chroma, hue);
    }

    public static MyColor fromLAB(float L, float A, float B)
    {
        float[] VCH = ColorTransform.LABtoVCH(L, A, B);
        return new MyColor(VCH[0], VCH[1], VCH[2]);
    }

    public static MyColor fromRGB(float redf, float greenf, float bluef)
    {
        float[] VCH = ColorTransform.RGBtoVCH(redf, greenf, bluef);
        return new MyColor(VCH[0], VCH[1], VCH[2]);
    }

    public static MyColor fromRGB(float[] RGB)
    {
        return fromRGB(RGB[0], RGB[1], RGB[2]);
    }

    public static MyColor fromColor(final Color color)
    {
        float redf = ((float)color.getRed()) / 255f;
        float greenf = ((float)color.getGreen()) / 255f;
        float bluef = ((float)color.getBlue()) / 255f;
        //float alphaf = ((float)color.getAlpha()) / 255f; // alphaf expected to be 1

        float[] VCH = ColorTransform.RGBtoVCH(redf, greenf, bluef);
        return new MyColor(VCH[0], VCH[1], VCH[2]);
    }

    public static CountComparator getCountComparator(){
        return CC;
    }

    private MyColor(float value, float chroma, float hue)
    {
        create(value, chroma, hue);
    }

    public boolean existsInRGB()
    {
        return existsInRGB;
    }

    public boolean existsInCMYK() {
        return existsInCMYK;
    }

    public void incrementCount()
    {
        count++;
    }

    public int getCount(){
        return count;
    }

    public Color getColor() {
        return new Color(RGB[0], RGB[1], RGB[2]);
    }

    public float getL()
    {
        return LAB[0];
    }

    public float getA()
    {
        return LAB[1];
    }

    public float getB()
    {
        return LAB[2];
    }

    public float getRedf()
    {
        return RGBf[0];
    }

    public float getGreenf()
    {
        return RGBf[1];
    }

    public float getBluef()
    {
        return RGBf[2];
    }

    public float getValue()
    {
        return VCH[0];
    }

    public float getChroma()
    {
        return VCH[1];
    }

    public float getHue()
    {
        float h =VCH[2];
        if(VCH[1] < 1.0) // check chroma
            h = 0;
        return (h != MAX_HUE) ? h : 0;
    }

    public float getC() {
        return CMYK[0];
    }

    public float getM() {
        return CMYK[1];
    }

    public float getY() {
        return CMYK[2];
    }

    public float getK() {
        return CMYK[3];
    }

    //public int[] getRGB() { return RGB; }
    public float[] getRGBf() { return RGBf; }
    public float[] getLAB() { return LAB; }
    public float[] getVCH() { return VCH; }

    public int getRed() {
        return RGB[0];
    }

    public int getGreen() {
        return RGB[1];
    }

    public int getBlue() {
        return RGB[2];
    }


    // returns the 3 values of RGB into a single integer
    public int getId()
    {
        return rgbId;
    }

    public Vector getPosition(){
        return new Vector(LAB[1], LAB[2], LAB[0]);
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public String getName(){ return name; }

    public boolean hasName() { return (name.length() > 0); }

    public void setName(String name) { this.name = name; }

    /*public String toStringRGB()
    {
        return "(" + RGB[0] + ", " + RGB[1] + ", " + RGB[2] + ")";
    }*/

    public String toStringVCH()
    {
        return "(" + Math.round(VCH[0]) + ", " + Math.round(VCH[1]) + ", " + Math.round(VCH[2]) + ")";
    }

    public String toStringLAB()
    {
        return "(" + Math.round(LAB[0]) + ", " + Math.round(LAB[1]) + ", " + Math.round(LAB[2]) + ")";
    }

    private void create(float value, float chroma, float hue)
    {
        VCH = new float[]{value, chroma, hue};
        LAB = ColorTransform.VCHtoLAB(value, chroma, hue);
        RGBf = ColorTransform.VCHtoRGB(value, chroma, hue);
        CMYK = ColorTransform.RGBtoCYMK(RGBf);

        existsInRGB = true;
        existsInCMYK = false;

        if(!isLegalRGBColor(RGBf[0], RGBf[1], RGBf[2]))
        {
            existsInRGB = false;

            if (RGBf[0] < 0f)
                RGBf[0] = 0f;
            else if (RGBf[0] > 1f)
                RGBf[0] = 1f;

            if (RGBf[1] < 0f)
                RGBf[1] = 0f;
            else if (RGBf[1] > 1f)
                RGBf[1] = 1f;

            if (RGBf[2] < 0f)
                RGBf[2] = 0f;
            else if (RGBf[2] > 1f)
                RGBf[2] = 1f;
        }

        if(C != null && M != null && Y != null && K != null && W != null)
            if(isLegalCMYKColor(this))
                existsInCMYK = true;

        int red = (int)(RGBf[0]*255+0.5);
        int green = (int)(RGBf[1]*255+0.5);
        int blue = (int)(RGBf[2]*255+0.5);
        int alpha = 255;

        rgbId = ((alpha & 0xFF) << 24) | ((red & 0xFF) << 16) | ((green & 0xFF) << 8)  | ((blue & 0xFF) << 0);

        RGB = new int[]{red, green, blue};

        count++; // count starts at 1
    }

    private static boolean isLegalRGBColor(float r, float g, float b)
    {
        return r >= 0.0f && r <= 1.0f
                && g >= 0.0f && g <= 1.0f
                && b >= 0.0f && b <= 1.0f;
    }

    private static boolean isLegalCMYKColor(MyColor color)
    {
        //System.out.println(calculateSurfaceNormal(CMYKc));

        /*float angle = getAngleBetweenXY(Y.getPosition(), M.getPosition(), new Vector(0, 0, 0), true);
        angle += getAngleBetweenXY(Y.getPosition(), C.getPosition(), new Vector(0,0,0), false);
        angle += getAngleBetweenXY(C.getPosition(), M.getPosition(), new Vector(0,0,0), false);*/

        /*float angle = getAngleBetweenXY(Y.getPosition(), M.getPosition(), color.getPosition(), true);
        angle += getAngleBetweenXY(Y.getPosition(), C.getPosition(), color.getPosition(), false);
        angle += getAngleBetweenXY(C.getPosition(), M.getPosition(), color.getPosition(), false);

        //130.22755f, -109.273895f, 0f
        if(angle == 360) {
            System.out.println(color.getPosition());
            return true;
        }

        return false;*/

        /*if(intpoint_inside_trigon(color.getPosition(), Y.getPosition(), C.getPosition(), M.getPosition())
                && (intpoint_inside_trigon2(color.getPosition(), Y.getPosition(), K.getPosition(), W.getPosition())
                || intpoint_inside_trigon2(color.getPosition(), C.getPosition(), K.getPosition(), W.getPosition()))
                && (intpoint_inside_trigon3(color.getPosition(), K.getPosition(), C.getPosition(), M.getPosition())
                || intpoint_inside_trigon3(color.getPosition(), W.getPosition(), C.getPosition(), M.getPosition()))) {
            return pointOverPoly(K.getPosition(), Y.getPosition(), C.getPosition(), color.getPosition())
                    && pointOverPoly(K.getPosition(), Y.getPosition(), M.getPosition(), color.getPosition());
                    /*&& pointOverPoly(K.getPosition(), M.getPosition(), C.getPosition(), color.getPosition());
        }

        return false;*/
        //System.out.println(color + " " + color.getPosition());
        boolean in1 = true;
        //boolean in1 = pointOverPoly(K.getPosition(), Y.getPosition(), C.getPosition(), color.getPosition(), false)
        //        && pointOverPoly(K.getPosition(), Y.getPosition(), M.getPosition(), color.getPosition(), false);

        boolean in2 = true;
        //if(color.getPosition().getX() <= W.getPosition().getX() && color.getPosition().getZ() >= C.getPosition().getZ())
        //    in2 = (pointOverPoly(W.getPosition(), Y.getPosition(), C.getPosition(), color.getPosition()));

        //else if(color.getPosition().getX() >= Y.getPosition().getX() && color.getPosition().getZ() >= M.getPosition().getZ())
        //    in2 = pointOverPoly(Y.getPosition(), W.getPosition(), M.getPosition(), color.getPosition());

        boolean in3 = true;

        //Vector temp = new Vector(W.getPosition().getX(), W.getPosition().getY(), 0);
        //if(color.getPosition().getY() <= 0) {
            if (color.getPosition().getZ() >= C.getPosition().getZ())
                in3 = pointOverPoly(W.getPosition(), C.getPosition(), M.getPosition(), color.getPosition(), false);

            if (color.getPosition().getZ() <= M.getPosition().getZ())
                in3 = pointOverPoly(K.getPosition(), C.getPosition(), M.getPosition(), color.getPosition(), false);
        //}

        return in1 && in2 && in3;
    }

    public static boolean pointOverPoly(Vector a, Vector b, Vector c, Vector p, boolean positive) {
        //Let N = (B-A) X (C-A), N1 = (B-A) X (P-A), N2 = (C-B) X (P-B), N3 = (A-C) X (P-C)
        //return N1 * N >= 0 and N2 * N >= 0 and N3 * N >= 0;

        //p = new Vector(30.6f, -25.9f, 80f);
        //p = new Vector(-5.2f,-29.6f, 39.9f);

        Vector N = Vector.cross(Vector.subtract(b, a), Vector.subtract(c, a));

        //System.out.println(Vector.subtract(b, a) + " " + Vector.subtract(c, a) + " " + Vector.cross(Vector.subtract(b, a), Vector.subtract(p, a)));

        Vector N1 = Vector.cross(Vector.subtract(b, a), Vector.subtract(p, a));
        Vector N2 = Vector.cross(Vector.subtract(c, b), Vector.subtract(p, b));
        Vector N3 = Vector.cross(Vector.subtract(a, c), Vector.subtract(p, c));

        float x = Vector.dot(N1, N);
        float y = Vector.dot(N2, N);
        float z = Vector.dot(N2, N);

        //System.out.println((Vector.dot(N1, N) >= 0) + " " + (Vector.dot(N2, N) >= 0) + " " + (Vector.dot(N3, N) >= 0) + " --- ");
        return Vector.dot(N1, N) >= 0 && ((positive) ? Vector.dot(N2, N) <= 0 : Vector.dot(N2, N) >= 0) && Vector.dot(N3, N) >= 0;
    }

    private static Vector calculateSurfaceNormal(MyColor CMYK[]) {

        Vector normal = new Vector(0,0,0);
        Vector current;
        Vector next;

        for(int i = 0; i < CMYK.length; i++) {
            current = CMYK[i].getPosition();
            next = CMYK[(i+1)%CMYK.length].getPosition();

            normal.addX((current.getY() - next.getY()) * (current.getZ() + next.getZ()));
            normal.addY((current.getZ() - next.getZ()) * (current.getX() + next.getX()));
            normal.addZ((current.getX() - next.getX()) * (current.getY() + next.getY()));
        }

        return normal.normalize();
    }


    public static boolean intpoint_inside_trigon(Vector s, Vector a, Vector b, Vector c)
    {
        float as_x = s.getX()-a.getX();
        float as_y = s.getY()-a.getY();

        boolean s_ab = (b.getX()-a.getX())*as_y-(b.getY()-a.getY())*as_x > 0;

        if((c.getX()-a.getX())*as_y-(c.getY()-a.getY())*as_x > 0 == s_ab) return false;

        if((c.getX()-b.getX())*(s.getY()-b.getY())-(c.getY()-b.getY())*(s.getX()-b.getX()) > 0 != s_ab) return false;

        return true;
    }

    public static boolean intpoint_inside_trigon2(Vector s, Vector a, Vector b, Vector c)
    {
        float as_x = s.getZ()-a.getZ();
        float as_y = s.getY()-a.getY();

        boolean s_ab = (b.getZ()-a.getZ())*as_y-(b.getY()-a.getY())*as_x > 0;

        if((c.getZ()-a.getZ())*as_y-(c.getY()-a.getY())*as_x > 0 == s_ab) return false;

        if((c.getZ()-b.getZ())*(s.getY()-b.getY())-(c.getY()-b.getY())*(s.getZ()-b.getZ()) > 0 != s_ab) return false;

        return true;
    }

    public static boolean intpoint_inside_trigon3(Vector s, Vector a, Vector b, Vector c) {
        float as_x = s.getX()-a.getX();
        float as_y = s.getZ()-a.getZ();

        boolean s_ab = (b.getX()-a.getX())*as_y-(b.getZ()-a.getZ())*as_x > 0;

        if((c.getX()-a.getX())*as_y-(c.getZ()-a.getZ())*as_x > 0 == s_ab) return false;

        if((c.getX()-b.getX())*(s.getZ()-b.getZ())-(c.getZ()-b.getZ())*(s.getX()-b.getX()) > 0 != s_ab) return false;

        return true;
    }

    public static float getAngleBetweenXY(Vector v1, Vector v2, Vector p, boolean wrapAroundAxis) {
        float angle1 = FloatUtil.atan2(v1.getY() - p.getY(), v1.getX() - p.getX());
        float angle2 = FloatUtil.atan2(v2.getY() - p.getY(), v2.getX() - p.getX());

        angle1 *= VectorMath.RAD_TO_DEG;
        angle2 *= VectorMath.RAD_TO_DEG;

        float angleBetween;
        if(wrapAroundAxis)
            angleBetween =  angle1 + FloatUtil.abs(angle2);
        else {
            if(angle1 < 0) angle1 += 360f;
            if(angle2 < 0) angle2 += 360f;

            angleBetween = angle2 - angle1;
        }

        return angleBetween;
    }

    public static float calcRotationAngleInDegrees(Vector centerPt, Vector targetPt) {
        double theta = Math.atan2(FloatUtil.abs(targetPt.getY()) + FloatUtil.abs(centerPt.getY()),
                FloatUtil.abs(targetPt.getX()) + FloatUtil.abs(centerPt.getX()));
        theta += Math.PI / 2.0;
        double angle = Math.toDegrees(theta);

        if (angle < 0) {
            angle += 360;
        }

        return (float) angle;
    }

    /*public static double angleBetween2Lines(Line2D line1, Line2D line2)
    {
        double angle1 = Math.atan2(line1.getY1() - line1.getY2(),
                line1.getX1() - line1.getX2());
        double angle2 = Math.atan2(line2.getY1() - line2.getY2(),
                line2.getX1() - line2.getX2());
        return angle1-angle2;
    }*/

    public static Vector calcNormalVector(Vector a, Vector b, Vector c) {
        Vector direction = Vector.cross(Vector.subtract(b, a), Vector.subtract(c, a));
        //return Vector.cross(Vector.subtract(b, a), Vector.subtract(c, a));
        return Vector.normalize(direction);
    }

    private boolean PointInTriangle(Vector[] TriangleVectors, Vector P)
    {
        Vector A = TriangleVectors[0], B = TriangleVectors[1], C = TriangleVectors[2];
        if (SameSide(P, A, B, C) && SameSide(P, B, A, C) && SameSide(P, C, A, B))
        {
            Vector vc1 = Vector.cross(Vector.subtract(A, B), Vector.subtract(A, C));
            if (Math.abs(Vector.dot(Vector.subtract(A, P), vc1)) <= .01f)
                return true;
        }

        return false;
    }

    private boolean SameSide(Vector p1, Vector p2, Vector A, Vector B)
    {
        Vector cp1 = Vector.cross(Vector.subtract(B, A), Vector.subtract(p1, A));
        Vector cp2 = Vector.cross(Vector.subtract(B, A), Vector.subtract(p2, A));
        if (Vector.dot(cp1, cp2) >= 0) return true;
        return false;

    }

    private static final class CountComparator implements Comparator<MyColor> {
        public int compare(MyColor c1, MyColor c2) {
            if(c1.getCount() > c2.getCount())
                return 1;
            else if(c1.getCount() < c2.getCount())
                return -1;
            else
                return 0;
        }
    }

    /*@Override
    public boolean equals(Object o)
    {
        if (!(o instanceof MyColor) && !(o instanceof Color)) return false;
        Color oColor = o instanceof MyColor ? ((MyColor)o).getColor() : (Color) o;
        return getColor().equals(oColor);
    }*/

    @Override
    public boolean equals(Object o)
    {
        MyColor color = o instanceof MyColor ? (MyColor)o : null;

        if(color != null)
            return rgbId == color.getId();
        else
            return false;
    }
}
