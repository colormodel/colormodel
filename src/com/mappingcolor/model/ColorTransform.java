package com.mappingcolor.model;//http://www.easyrgb.com/index.php?X=MATH&H=01#text1
// LCH (VCh) -> LAB -> XYZ -> RGB

import com.jogamp.opengl.math.FloatUtil;

public class ColorTransform
{
    private static float refX = 95.047f;
    private static float refY = 100f;
    private static float refZ = 108.883f;

    private ColorTransform() {}

    public static float[] VCHtoRGB(float... VCH)
    {
        float[] LAB = ColorTransform.VCHtoLAB(VCH);
        float[] XYZ = ColorTransform.LABtoXYZ(LAB);
        return ColorTransform.XYZtoRGB(XYZ);
    }

    public static float[] RGBtoVCH(float... RGB)
    {
        float[] XYZ = RGBtoXYZ(RGB);
        float[] LAB = XYZtoLAB(XYZ);
        return LABtoVCH(LAB);
    }

    public static float[] RGBtoXYZ(float... RGB)
    {
        float R = RGB[0];
        float G = RGB[1];
        float B = RGB[2];


        if(R > 0.04045f)
            R = (float)Math.pow((R + 0.055f) / 1.055f, 2.4);
        else
            R /= 12.92f;

        if(G > 0.04045f)
            G = (float)Math.pow((G + 0.055f) / 1.055f, 2.4);
        else
            G /= 12.92f;

        if(B > 0.04045f)
            B = (float)Math.pow((B + 0.055f) / 1.055f, 2.4);
        else
            B /= 12.92f;

        R *= 100f;
        G *= 100f;
        B *= 100f;

        //Observer. = 2°, Illuminant = D65
        float[] XYZ = new float[3];
        XYZ[0] = R * 0.4124f + G * 0.3576f + B * 0.1805f;
        XYZ[1] = R * 0.2126f + G * 0.7152f + B * 0.0722f;
        XYZ[2] = R * 0.0193f + G * 0.1192f + B * 0.9505f;

        return XYZ;
    }

    public static float[] RGBtoCMY(float... RGBf)
    {
        float[] CMY = new float[3];
        CMY[0] = 1 - RGBf[0];
        CMY[1] = 1 - RGBf[1];
        CMY[2] = 1 - RGBf[2];

        return CMY;
    }

    public static float[] CMYtoCMYK(float... CMY)
    {
        float[] CMYK = new float[4];
        float K = 1;

        if ( CMY[0] < K )   K = CMY[0];
        if ( CMY[1] < K )   K = CMY[1];
        if ( CMY[2] < K )   K = CMY[2];
        if ( K == 1 ) { //Black
            CMYK[0] = 0;
            CMYK[1] = 0;
            CMYK[2] = 0;
        }
        else {
            CMYK[0] = ( CMY[0] - K ) / ( 1 - K );
            CMYK[1] = ( CMY[1] - K ) / ( 1 - K );
            CMYK[2] = ( CMY[2] - K ) / ( 1 - K );
        }
        CMYK[3] = K;

        return CMYK;
    }

    public static float[] RGBtoCYMK(float... RGBf)
    {
        float[] CMY = RGBtoCMY(RGBf);
        return CMYtoCMYK(CMY);
    }

    public static float[] XYZtoLAB(float... XYZ){
        float X = XYZ[0];
        float Y = XYZ[1];
        float Z = XYZ[2];
        X /= refX;
        Y /= refY;
        Z /= refZ;

        if ( X > 0.008856f )
            X = (float)Math.pow(X, ( 1.0/3.0 ));
        else
            X = ( 7.787f * X ) + ( 16f / 116f );

        if ( Y > 0.008856f )
            Y = (float)Math.pow(Y, ( 1.0/3.0 ));
        else
            Y = ( 7.787f * Y ) + ( 16f / 116f );

        if ( Z > 0.008856f )
            Z = (float)Math.pow(Z, ( 1.0/3.0 ));
        else
            Z = ( 7.787f * Z ) + ( 16f / 116f );

        float[] LAB = new float[3];
        LAB[0] = ( 116f * Y ) - 16f;
        LAB[1] = 500f * ( X - Y );
        LAB[2] = 200f * ( Y - Z );

        return LAB;
    }

    public static float[] LABtoVCH(float... LAB)
    {
        float L = LAB[0];
        float A = LAB[1];
        float B = LAB[2];
        float H = (float)StrictMath.atan2( B, A );  //Quadrant by signs

        if ( H > 0 )
            H = ( H / (float)Math.PI ) * 180f;
        else
            H = 360f - ( Math.abs( H ) / (float)Math.PI ) * 180f;

        float[] VCH = new float[3];
        VCH[0] = L;
        VCH[1] = (float)Math.sqrt( A*A + B*B );
        VCH[2] = H;

        return VCH;
    }

    public static float[] VCHtoLAB(float... VCH) {
        float[] LAB = new float[3];
        LAB[0] = VCH[0];
        LAB[1] = (float) (StrictMath.cos(StrictMath.toRadians(VCH[2])) * VCH[1]);
        LAB[2] = (float) (StrictMath.sin(StrictMath.toRadians(VCH[2])) * VCH[1]);

        for(int i = 0; i < LAB.length; i++)
            if(LAB[i] == -0.0)
                LAB[i] = FloatUtil.abs(LAB[i]);

        return LAB;
    }

    public static float[] LABtoXYZ(float... LAB) {
        float Y = (LAB[0] + 16f) / 116f;
        float X = (LAB[1] / 500f) + Y;
        float Z = Y - LAB[2] / 200f;

        if (StrictMath.pow(Y, 3.0) > 0.008856)
            Y = (float) StrictMath.pow(Y, 3.0);
        else
            Y = (Y - 16f / 116f) / 7.787f;

        if (StrictMath.pow(X, 3.0) > 0.008856)
            X = (float) StrictMath.pow(X, 3);
        else
            X = (X - 16f / 116f) / 7.787f;

        if (StrictMath.pow(Z, 3.0) > 0.008856)
            Z = (float) StrictMath.pow(Z, 3);
        else
            Z = (Z - 16f / 116f) / 7.787f;

        float[] XYZ = new float[3];
        XYZ[0] = refX * X;
        XYZ[1] = refY * Y;
        XYZ[2] = refZ * Z;
        return XYZ;
    }

    public static float[] XYZtoRGB(float... XYZ) {
        float X = XYZ[0] / 100;
        float Y = XYZ[1] / 100;
        float Z = XYZ[2] / 100;
        float R = X * 3.2406f + Y * -1.5372f + Z * -0.4986f;
        float G = X * -0.9689f + Y * 1.8758f + Z * 0.0415f;
        float B = X * 0.0557f + Y * -0.2040f + Z * 1.0570f;

        if (R > 0.0031308f)
            R = (float) (1.055f * (StrictMath.pow(R, (1.0 / 2.4))) - 0.055f);
        else
            R *= 12.92f;

        if (G > 0.0031308f)
            G = (float) (1.055f * (StrictMath.pow(G, (1.0 / 2.4))) - 0.055f);
        else
            G *= 12.92f;

        if (B > 0.0031308f)
            B = (float) (1.055f * (StrictMath.pow(B, (1.0 / 2.4))) - 0.055f);
        else
            B *= 12.92f;

        float[] RGB = new float[3];
        RGB[0] = round10000th(R);
        RGB[1] = round10000th(G);
        RGB[2] = round10000th(B);

        return RGB;
    }

    private static float round10000th(float number)
    {
        return Math.round(number * 10000f) / 10000f;
    }
}
