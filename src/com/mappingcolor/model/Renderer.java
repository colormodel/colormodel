package com.mappingcolor.model;

import com.jogamp.opengl.util.gl2.GLUT;
import com.mappingcolor.R;
import com.mappingcolor.controller.Keyboard;
import com.mappingcolor.gui.MyView;
import com.mappingcolor.controller.Mouse;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import java.nio.FloatBuffer;
import java.util.ArrayList;

public class Renderer implements GLEventListener {
    private static final GLU glu = new GLU();
    private static final GLUT glut = new GLUT();

    private static final int ONE_PIXEL = 1;
    private static final int HEIGHT_INDEX = 3; // for the viewport[n=4] array
    private static final int RGB_BYTE_SIZE = 3;

    private MyView view;
    private ColorModel colorModel;
    private ColorPointSelector colorPointSelector;

    private FloatBuffer pixels = FloatBuffer.allocate(RGB_BYTE_SIZE);
    private int[] viewport = new int[4]; // 0->x, 1->y, 2->width, 3->height

    private static ColorTracker[] colorTrackers = new ColorTracker[2];

    private boolean firstRun = true;

    public static Ray ray = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));

    public Renderer(MyView view, ColorModel colorModel) {
        this.view = view;
        this.colorModel = colorModel;

        colorPointSelector = new ColorPointSelector(colorModel);

        for(int i = 0; i < colorTrackers.length; i++)
            colorTrackers[i] = new ColorTracker();
    }

    public ColorTracker[] getColorTrackers() {
        return colorTrackers;
    }

    @Override
    public void init(GLAutoDrawable glautodrawable) {
        GL2 gl = glautodrawable.getGL().getGL2();

        glautodrawable.getAnimator().setUpdateFPSFrames(60, null);

        //Camera.orbit(0,0);

        gl.glEnable(GL2.GL_DEPTH_TEST);
        gl.glViewport(0, 0, view.getWidth(), view.getHeight());
        gl.glMatrixMode(GL2.GL_PROJECTION);

        glu.gluPerspective(Camera.VIEW_ANGLE, (float)view.getWidth() / (float)view.getHeight(), Camera.Z_NEAR, Camera.Z_FAR);

        Camera.reset();

        glu.gluLookAt(
                Camera.offset.getX(), Camera.offset.getY(), Camera.offset.getZ(),
                Camera.rotation.getX(), Camera.rotation.getY(), Camera.rotation.getZ(),
                0, 1, 0);

        //gl.glTranslatef(Camera.focus.getX(), Camera.focus.getY(), Camera.focus.getZ());
        //gl.glMultMatrixf(Camera.getLookAtMatrix(), 0);

        // enables transparency
        gl.glEnable(GL2.GL_BLEND);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

        // anti-aliasing
        gl.glEnable(GL2.GL_LINE_SMOOTH);
        gl.glHint(GL2.GL_LINE_SMOOTH_HINT, GL2.GL_NICEST);
        gl.glHint(GL2.GL_POLYGON_SMOOTH_HINT, GL2.GL_NICEST);

        //check VBO support
        R.supportVBO =
                gl.isFunctionAvailable("glGenBuffersARB") &&
                gl.isFunctionAvailable("glBindBufferARB") &&
                gl.isFunctionAvailable("glBufferDataARB") &&
                gl.isFunctionAvailable("glDeleteBuffersARB") &&
                gl.isExtensionAvailable("GL_ARB_vertex_buffer_object");

        R.createVBOs = R.updateVBOs = R.enabledVBO = R.supportVBO;

        colorModel.shouldDraw = !R.supportVBO;

        colorModel.build();
    }

    @Override
    public void reshape(GLAutoDrawable glautodrawable, int x, int y, int width, int height) {
        GL2 gl = glautodrawable.getGL().getGL2();

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glViewport(0, 0, width, height);
        gl.glLoadIdentity();
        glu.gluPerspective(Camera.VIEW_ANGLE, (float) width / (float) height, Camera.Z_NEAR, Camera.Z_FAR);
    }

    @Override
    public void dispose(GLAutoDrawable glautodrawable) {}

    @Override
    public void display(GLAutoDrawable glautodrawable) {
        GL2 gl = glautodrawable.getGL().getGL2();

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glClearColor(R.backgroundColor.getRedf(), R.backgroundColor.getGreenf(), R.backgroundColor.getBluef(), 0f);
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

        gl.glPushMatrix();

        gl.glTranslatef(Camera.position.getX(), Camera.position.getY(), Camera.position.getZ());
        gl.glRotatef(Camera.rotation.getX(), 1,0,0);
        gl.glRotatef(Camera.rotation.getY(), 0,1,0);

        //gl.glTranslatef(Camera.focus.getX(), Camera.focus.getY(), Camera.focus.getZ());
        //gl.glMultMatrixf(Camera.getLookAtMatrix(), 0);

        /*glu.gluLookAt(
                Camera.position.getX(), Camera.position.getY(), Camera.position.getZ(),
                Camera.focus.getX(),    Camera.focus.getY(),    Camera.focus.getZ(),
                Camera.up.getX(),       Camera.up.getY(),       Camera.up.getZ());*/

        // looks better if axes are drawn before colorPoints
        // needs to be drawn before colorPoints, or it becomes overshadowed by it
        if(R.displayAxes)
            Axes.draw(gl);

        colorTrackers[ColorTracker.LEFT].draw(gl);
        colorTrackers[ColorTracker.RIGHT].draw(gl);

        colorModel.draw(gl);
        //ray.draw(gl);

        if(!firstRun)
            updateFlaggedEvents(gl);

        if(R.debugData) {
            displayDebugData(gl, glautodrawable.getAnimator().getLastFPS());
            displayCMYK(gl);
        }

        gl.glPopMatrix();

        firstRun = false;
    }

    private void displayDebugData(GL2 gl, float time) {
        String fps = "FPS: " + Math.round(time * 100) / 100;
        String colorTracker1 = (colorTrackers[0].isColorSelected()) ? "CT1 " + colorTrackers[0].getColor().getPosition() + "" : "";
        String colorTracker2 = (colorTrackers[1].isColorSelected()) ? "CT2 " + colorTrackers[0].getColor().getPosition() + "" : "";

        gl.glPushMatrix();
        gl.glLoadIdentity();
        gl.glGetIntegerv (GL2.GL_VIEWPORT, viewport,0);
        glu.gluOrtho2D (0,viewport[2], viewport[3], 0);
        gl.glDepthFunc(GL2.GL_ALWAYS);
        gl.glColor4f(1, 1, 1, 1);
        gl.glRasterPos2d(10,20);
        glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, fps);
        gl.glRasterPos2d(10,35);
        glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, colorTracker1);
        gl.glRasterPos2d(10,50);
        glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, colorTracker2);
        gl.glDepthFunc(GL2.GL_LESS);
        gl.glPopMatrix();
    }

    private void displayCMYK(GL2 gl) {
        ColorPoint[] CMYK = ColorModel.CMYK;

        CMYK[ColorModel.C].draw(gl);
        CMYK[ColorModel.M].draw(gl);
        CMYK[ColorModel.Y].draw(gl);
        CMYK[ColorModel.K].draw(gl);
        CMYK[ColorModel.W].draw(gl);

        gl.glPushMatrix();
        for(int i = 0; i < CMYK.length; i++) {
            Vector from = CMYK[i].getPosition();
            for (int j = 0; j < CMYK.length; j++) {
                Vector to = CMYK[j].getPosition();
                if (i != j) {
                    gl.glColor3f(ColorPoint.WHITE_OUTLINE.getRedf(), ColorPoint.WHITE_OUTLINE.getGreenf(), ColorPoint.WHITE_OUTLINE.getBluef());
                    gl.glBegin(GL2.GL_LINES);
                    gl.glVertex3f(from.getX(), from.getY(), from.getZ());
                    gl.glVertex3f(to.getX(), to.getY(), to.getZ());
                    gl.glEnd();
                }
            }
        }
        gl.glPopMatrix();
    }

    private void updateFlaggedEvents(GL2 gl) {
        if(Keyboard.isKeyPressed()) {
            if(Keyboard.isUpPressed())
                Camera.rotate(10, 0);
            if(Keyboard.isRightPressed())
                Camera.rotate(0, -10);
            if(Keyboard.isDownPressed())
                Camera.rotate(-10, 0);
            if(Keyboard.isLeftPressed())
                Camera.rotate(0, 10);

            /*if(Keyboard.isUpPressed())
                Camera.orbit(0, -10);
            if(Keyboard.isRightPressed())
                Camera.orbit(-10, 0);
            if(Keyboard.isDownPressed())
                Camera.orbit(0, 10);
            if(Keyboard.isLeftPressed())
                Camera.orbit(10, 0);*/
        }

        if (R.selectColorFlag) {
            if(Mouse.isButton1Down())
                selectPixelColor(gl, ColorTracker.LEFT);
            else if(Mouse.isButton3Down())
                selectPixelColor(gl, ColorTracker.RIGHT);
        }

        if (R.deselectColorFlag)
            deselectPixelColor();

        if (R.resetCameraFlag) { //can be moved
            Camera.reset();
            R.resetCameraFlag = false;
        }

        if(R.clearPlotsFlag) { //can be moved
            colorModel.clearPlots();
            R.clearPlotsFlag = false;
        }

        if (R.resetModelFlag) {
            view.slidePanel.reset();
            view.settingsPopup.reset();
            colorModel.reset();
            colorModel.build();
            R.updateVBOs = true;
            R.createVBOs = true;
            R.resetModelFlag = false;
        }

        if (R.stepSizeChangedFlag) {
            colorModel.build();

            if(R.imageData != null || R.videoData != null || R.fileData != null)
                R.dataChanged = true;

            if(R.enabledVBO)
                R.updateVBOs = R.createVBOs = true;

            R.stepSizeChangedFlag = false;
        }

        if (R.transparencyModeChangedFlag) {
            if (ColorModel.transparencyMode == ColorModel.TransparencyMode.ALL)
                colorModel.setAllColorPointsTransparent();
            else if (ColorModel.transparencyMode == ColorModel.TransparencyMode.NONE)
                colorModel.setAllColorPointsOpaque();
            else {
                colorModel.setAllColorPointsOpaque();
                colorModel.setNonRGBColorPointsTransparent();
            }
        }

        if(R.dataChanged) {
            dataChanged();
            R.dataChanged = false;
        }

        if (R.updateVBOs) {
            updateVBOs(gl);
            R.updateVBOs = false;
        }
    }

    private void dataChanged() {
        ArrayList<MyColor> colors = null;

        if(R.imageData != null)
            colors = R.imageData.getColors();
        else if(R.videoData != null)
            colors = R.videoData.getColors();
        else if(R.fileData != null)
            colors = R.fileData.getColors();

        if(R.dataLoaded) {
            colorModel.setColorPointsTransparent(colorModel.getAllColorPoints());
            R.dataLoaded = false;
        }

        if(R.dataUnloaded) {
            colorModel.reset();
            R.dataUnloaded = false;
        }

        //data loaded
        if(colors != null && R.currentFrame.get() % R.frameSkip == 0) {
            if (ColorModel.mappingMethod == ColorModel.MappingMethod.NEAREST_NEIGHBOR)
                colorModel.mapNearestNeighborParallel(colors, R.countThreshold, R.currentFrame.get());
            else if(ColorModel.mappingMethod == ColorModel.MappingMethod.EXACT_LOCATION
                    && colorModel.mapExactLocationParallel(colors, R.countThreshold, R.currentFrame.get())) {
                if(R.enabledVBO) {
                    R.updateVBOs = true;

                    if (VBOHelper.pigmentSize < colorModel.exactLocation.size())
                        R.createVBOs = true;
                    else
                        R.updatePigmentVBOs = true;
                }
            }

            if (R.imageData != null)
                R.repaintImagePanel(view);
            else if (R.fileData != null)
                R.repaintImagePanel(view);
        }
    }

    private void updateVBOs(GL2 gl) {
        if(R.supportVBO) {
            if(R.createVBOs) {
                VBOHelper.createVBOs(gl, colorModel.allColorPoints, colorModel.getExactLocation());
                R.createVBOs = false;
            }

            if(R.updatePigmentVBOs) {
                VBOHelper.updatePigmentVBOs(gl, colorModel.getExactLocation(), true);
                R.updateColorPointVertexColorVBOs = false;
            }

            if(R.updateColorPointVertexColorVBOs) {
                VBOHelper.updateColorPointVertexColorVBOs(gl, colorModel.allColorPoints);
                R.updateColorPointVertexColorVBOs = false;
            }

            if(R.updateWireframeVertexColorVBOs) {
                VBOHelper.updateWireframeVertexColorVBOs(gl, colorModel.allColorPoints);
                R.updateWireframeVertexColorVBOs = false;
            }
        }

        colorModel.shouldDraw = true;
    }

    public void selectPixelColor(GL2 gl, int colorTrackerIndex) {
        gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);
        gl.glReadBuffer(GL2.GL_BACK);
        gl.glReadPixels(Mouse.getLocationX(), viewport[HEIGHT_INDEX] - Mouse.getLocationY(), ONE_PIXEL, ONE_PIXEL, GL2.GL_RGB, GL2.GL_FLOAT, pixels);

        MyColor color = MyColor.fromRGB(pixels.array());

        colorTrackers[colorTrackerIndex].setColor(color);

        if(color.equals(R.backgroundColor)) {
            if (colorTrackerIndex == ColorTracker.LEFT)
                clearColorTrackerLeft();
            else if (colorTrackerIndex == ColorTracker.RIGHT)
                clearColorTrackerRight();
        }
        else {
            if(colorTrackerIndex == ColorTracker.LEFT) {
                R.selectedColorOne = color;
                R.selectedColorOne.setName(colorModel.getPigmentName(R.selectedColorOne));
                R.updateColorFocusWidgetOne(view, R.selectedColorOne);
            }
            else if(colorTrackerIndex == ColorTracker.RIGHT) {
                R.selectedColorTwo = color;
                R.selectedColorTwo.setName(colorModel.getPigmentName(R.selectedColorOne));
                R.updateColorFocusWidgetTwo(view, R.selectedColorTwo);
            }

            colorTrackers[colorTrackerIndex].colorSelected(true);
        }
    }

    public void deselectPixelColor() {
        if(R.selectedColorOne.equals(R.backgroundColor))
            R.clearColorFocusWidgetOne(view);

        if(R.selectedColorTwo.equals(R.backgroundColor))
            R.clearColorFocusWidgetTwo(view);

        for(ColorTracker colorTracker : colorTrackers) {
            if(colorTracker.getColor().equals(R.backgroundColor))
                colorTracker.colorSelected(!colorTracker.getColor().equals(R.backgroundColor));
        }

        R.deselectColorFlag = false;
    }

    public static void clearColorTrackerLeft() {
        colorTrackers[ColorTracker.LEFT].setColor(R.backgroundColor);
        R.selectedColorOne = R.backgroundColor;
        R.deselectColorFlag = true;
    }

    public static void clearColorTrackerRight() {
        colorTrackers[ColorTracker.RIGHT].setColor(R.backgroundColor);
        R.selectedColorTwo = R.backgroundColor;
        R.deselectColorFlag = true;
    }

    public static void clearColorTrackers() {
        for(ColorTracker colorTracker : colorTrackers) {
            if(colorTracker != null)
                colorTracker.setColor(R.backgroundColor);
        }

        R.selectedColorOne = R.backgroundColor;
        R.selectedColorTwo = R.backgroundColor;
        R.deselectColorFlag = true;
    }

    public static void setEnableVBO(boolean enableVBO) {
        R.enabledVBO = enableVBO && R.supportVBO;

        if(R.enabledVBO) {
            R.updateVBOs = true;
            R.createVBOs = true;
        }

        System.out.println("VBO " + ((R.enabledVBO) ? "enabled" : "disabled or not supported") + ".");
    }
}