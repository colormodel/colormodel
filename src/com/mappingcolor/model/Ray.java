package com.mappingcolor.model;

import javax.media.opengl.GL2;

public class Ray {
    public Vector origin;
    public Vector end;

    public Ray(Vector origin, Vector end)
    {
        this.origin = new Vector(origin);
        this.end = new Vector(end);
    }

    public void draw(GL2 gl) {
        gl.glPushMatrix();

        gl.glColor3f(1f, 1f, 1f);
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3f(origin.getX(), origin.getY(), origin.getZ());
        gl.glVertex3f(end.getX(), end.getY(), end.getZ());
        gl.glEnd();

        gl.glPopMatrix();
    }
}
