package com.mappingcolor.model;

 import com.mappingcolor.R;

import javax.media.opengl.GL2;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ColorModel {
    public enum MappingMethod { NEAREST_NEIGHBOR, EXACT_LOCATION }
    public enum DrawMode {RGB, CMYK, ALL }
    public enum TransparencyMode { NONE, NON_RGB, ALL }

    public static boolean wireframeMode = false; // only works on transparent colorPoints
    public static DrawMode drawMode = DrawMode.RGB;
    public static MappingMethod mappingMethod = MappingMethod.NEAREST_NEIGHBOR;
    public static TransparencyMode transparencyMode = TransparencyMode.NONE;

    public static int valueStepSize = R.DEFAULT_STEP_SIZE; // 10, 11 layers
    public static int chromaStepSize = R.DEFAULT_STEP_SIZE; // 10, 11 layers
    public static int hueStepSize = R.DEFAULT_STEP_SIZE; // 10, 36 layers
    public static int valueLowerBound = 0;
    public static int valueUpperBound = MyColor.MAX_VALUE;
    public static boolean valueCompliment = false;
    public static int chromaLowerBound = 0;
    public static int chromaUpperBound = MyColor.MAX_CHROMA;
    public static boolean chromaCompliment = false;
    public static int hueLowerBound = 0;
    public static int hueUpperBound = MyColor.MAX_HUE;
    public static boolean hueCompliment = false;

    public static int nearestCountThreshold = -1;
    public static int exactCountThreshold = -1;

    public static final int MAX_VALUE_HALF = MyColor.MAX_VALUE / 2;

    public Map<ColorPoint, Integer> allColorPoints = new ConcurrentHashMap<ColorPoint, Integer>();
    public Map<Integer, ConcurrentHashMap<Integer, ColorPoint>> nearestNeighbor = new HashMap<Integer, ConcurrentHashMap<Integer, ColorPoint>>();
    public Map<Integer, ConcurrentHashMap<ColorPoint, Integer>> exactLocation = new HashMap<Integer, ConcurrentHashMap<ColorPoint, Integer>>();

    public Map<String, ColorPoint> colorPointMapVCH = new ConcurrentHashMap<String, ColorPoint>(); // used for searching for a specific sphere, much faster
    public Map<ColorPoint, Integer> colorPointsRGB = new ConcurrentHashMap<ColorPoint, Integer>();
    public Map<ColorPoint, Integer> colorPointsCMYK = new ConcurrentHashMap<ColorPoint, Integer>();
    public Map<Integer, ColorPoint> exactLocationMap = new ConcurrentHashMap<Integer, ColorPoint>(); // used for searching for a specific sphere, much faster

    public boolean shouldDraw = false;

    private int lastFrame = 0;

    private AtomicInteger pigmentCounter;

    public static ColorPoint[] CMYK = new ColorPoint[5];
    public static final int C = 0;
    public static final int Y = 1;
    public static final int M = 2;
    public static final int K = 3;
    public static final int W = 4;

    public ColorModel() {
        CMYK[0] = new ColorPoint(MyColor.C.getPosition(), MyColor.C);
        CMYK[1] = new ColorPoint(MyColor.M.getPosition(), MyColor.M);
        CMYK[2] = new ColorPoint(MyColor.Y.getPosition(), MyColor.Y);
        CMYK[3] = new ColorPoint(MyColor.K.getPosition(), MyColor.K);
        CMYK[4] = new ColorPoint(MyColor.W.getPosition(), MyColor.W);

        for(ColorPoint colorPoint : CMYK)
            colorPoint.getPosition().subtractZ(MAX_VALUE_HALF);
    }

    public Map<ColorPoint, Integer> getExactLocation() {
        if(exactLocation.get(lastFrame) == null)
            return new HashMap<ColorPoint, Integer>();

        return exactLocation.get(lastFrame);
    }

    // color input is from the ColorPicker module
    public String getPigmentName(MyColor color) {
        ColorPoint colorPoint = exactLocationMap.get(color.getId());
        return (colorPoint == null) ? "" : colorPoint.getColor().getName();
    }

    public void draw(GL2 gl) {
        if(shouldDraw) {
            if(mappingMethod == MappingMethod.EXACT_LOCATION && exactLocation.get(lastFrame) != null) {
                for (Map.Entry<ColorPoint, Integer> entry : exactLocation.get(lastFrame).entrySet()) {
                    if (withinSliderBounds(entry.getKey().getColor()))
                        if (R.enabledVBO)
                            entry.getKey().drawPigmentVBO(gl, entry.getValue());
                        else
                            entry.getKey().draw(gl);
                }
            }

            if (drawMode == DrawMode.RGB)
                drawHelper(gl, colorPointsRGB);
            else if(drawMode == DrawMode.CMYK)
                drawHelper(gl, colorPointsCMYK);
            else if(drawMode == DrawMode.ALL)
                drawHelper(gl, allColorPoints);
        }
    }

    private void drawHelper(GL2 gl, Map<ColorPoint, Integer> colorPoints) {
        for (Map.Entry<ColorPoint, Integer> entry : colorPoints.entrySet()) {
            if (withinSliderBounds(entry.getKey().getColor()))
                if (R.enabledVBO)
                    entry.getKey().drawVBO(gl, entry.getValue());
                else
                    entry.getKey().draw(gl);
        }
    }

<<<<<<< HEAD
    public static boolean withinSliderBounds(MyColor color) {
        /*return color.getValue() >= valueLowerBound && color.getValue() <= valueUpperBound &&
                color.getChroma() >= chromaLowerBound && color.getChroma() <= chromaUpperBound &&
                color.getHue() >= hueLowerBound && color.getHue() <= hueUpperBound;*/

        return ((valueCompliment) ? color.getValue() <= valueLowerBound || color.getValue() >= valueUpperBound :
                        color.getValue() >= valueLowerBound && color.getValue() <= valueUpperBound) &&
                ((chromaCompliment) ? color.getChroma() <= chromaLowerBound || color.getChroma() >= chromaUpperBound :
                        color.getChroma() >= chromaLowerBound && color.getChroma() <= chromaUpperBound) &&
                ((hueCompliment) ? color.getHue() <= hueLowerBound || color.getHue() >= hueUpperBound :
                        color.getHue() >= hueLowerBound && color.getHue() <= hueUpperBound);
=======
    private boolean withinSliderBounds(MyColor color) {
        boolean hueInBounds;
        if (hueLowerBound < hueUpperBound)
            hueInBounds = color.getHue() >= hueLowerBound && color.getHue() <= hueUpperBound;
        else if (hueLowerBound != hueUpperBound)
            hueInBounds = color.getHue() >= hueLowerBound || color.getHue() <= hueUpperBound;
        else
            hueInBounds = color.getHue() == hueLowerBound;

        return color.getValue() >= valueLowerBound && color.getValue() <= valueUpperBound &&
                color.getChroma() >= chromaLowerBound && color.getChroma() <= chromaUpperBound &&
                hueInBounds;
>>>>>>> origin/colorwheel
    }

    public int getValueStep() {
        return valueStepSize;
    }

    public int getChromaStep() {
        return chromaStepSize;
    }

    public int getHueStep() {
        return hueStepSize;
    }

    public Collection<ColorPoint> getAllColorPoints() {
        return colorPointMapVCH.values();
    }

    public void setAllColorPointsTransparent() {
        setColorPointsOpaque(colorPointMapVCH.values());
    }

    public void setAllColorPointsOpaque() {
        setColorPointsOpaque(colorPointMapVCH.values());
    }

    private void setColorPointsRBGOpaque() {
        for (Map.Entry<ColorPoint, Integer> entry : colorPointsRGB.entrySet())
            entry.getKey().setOpaque();
    }

    private void setColorPointsOpaque(Collection<ColorPoint> colorPoints) {
        for(ColorPoint colorPoint : colorPoints)
            colorPoint.setOpaque();
    }

    public void setColorPointsTransparent(Collection<ColorPoint> colorPoints) {
        for(ColorPoint colorPoint : colorPoints)
            colorPoint.setTransparent();
    }

    public void setNonRGBColorPointsTransparent() {
        for(ColorPoint colorPoint : colorPointMapVCH.values())
            if (!colorPoint.getColor().existsInRGB())
                colorPoint.setTransparent();
    }

    public void clearPlots() {
        exactLocationMap.clear();
    }

    public void reset() {
        setColorPointsRBGOpaque();
        setNonRGBColorPointsTransparent();
        exactLocation.clear();
        nearestNeighbor.clear();
        exactCountThreshold = -1;
        nearestCountThreshold = -1;
        clearPlots();
    }

    public boolean mapNearestNeighborParallel(List<MyColor> colors, int countThreshold, int frame) {
        if(nearestCountThreshold != countThreshold)
            nearestNeighbor.clear();

        ConcurrentHashMap<Integer, ColorPoint> mapNearest = nearestNeighbor.get(frame);
        Map<Integer, ColorPoint> lastNearest = nearestNeighbor.get(lastFrame);

        if(nearestCountThreshold == countThreshold && mapNearest != null) {
            if(lastNearest != null)
                setColorPointsTransparent(lastNearest.values());

            setColorPointsOpaque(mapNearest.values());

            lastFrame = frame;
        }
        else {
            if(lastNearest != null)
                setColorPointsTransparent(lastNearest.values());

            mapNearest = new ConcurrentHashMap<Integer, ColorPoint>();

            ExecutorService threadPool = Executors.newFixedThreadPool((int) R.processor_cores);
            CompletionService<String> pool = new ExecutorCompletionService<String>(threadPool);

            int chunk = (int) Math.ceil(colors.size() / R.processor_cores);

            int i;
            int nextChunk;
            for(i = 0; i < colors.size() - chunk; i = nextChunk + 1) {
                nextChunk = i + chunk;
                pool.submit(new MapNearestNeighborTask(colors, countThreshold, mapNearest, i, nextChunk));
            }
            pool.submit(new MapNearestNeighborTask(colors, countThreshold, mapNearest, i, colors.size() - 1));

            for(i = 0; i < R.processor_cores; i++)
                try {
                    pool.take().get();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                catch (ExecutionException e) {
                    e.printStackTrace();
                }

            threadPool.shutdown();

            nearestCountThreshold = countThreshold;
            nearestNeighbor.put(frame, mapNearest);
            lastFrame = frame;

            return true;
        }

        return false;
    }

    private synchronized ColorPoint getNearestSphere(MyColor color, int countThreshold) {
        Vector position = color.getPosition();
        double minDistance = Double.MAX_VALUE;
        ColorPoint colorPoint;
        ColorPoint nearestColorPoint = null;

        int vMin = (int) color.getValue() - valueStepSize;
        int cMin = (int) color.getChroma() - chromaStepSize;
        int hMin = (int) color.getHue() - hueStepSize;

        vMin = (vMin < 0) ? 0 : vMin;
        cMin = (cMin < 0) ? 0 : cMin;
        hMin = (hMin < 0) ? 0 : hMin;

        vMin -= vMin % valueStepSize;
        cMin -= cMin % chromaStepSize;
        hMin -= hMin % hueStepSize;

        for (int value = vMin; value <= (vMin + valueStepSize); value += valueStepSize)
            for (int chroma = cMin; chroma <= (cMin + chromaStepSize); chroma += chromaStepSize)
                for (int hue = hMin; hue <= (hMin + hueStepSize); hue += hueStepSize) {
                    String key = value + "," + chroma + "," + hue;
                    colorPoint = colorPointMapVCH.get(key);

                    if(colorPoint != null && color.getCount() >= countThreshold) {
                        double distance = Vector.subtract(position, colorPoint.getPosition()).magnitude();

                        if (minDistance > distance) {
                            minDistance = distance;
                            nearestColorPoint = colorPoint;
                        }
                    }
                }

        return nearestColorPoint;
    }

    public boolean mapExactLocationParallel(List<MyColor> colors, int countThreshold, int frame) {
        ConcurrentHashMap<Integer, ColorPoint> mapNearest = nearestNeighbor.get(lastFrame);

        if(exactCountThreshold == countThreshold && mapNearest != null)
            setColorPointsTransparent(mapNearest.values());

        lastFrame = frame;

        ConcurrentHashMap<ColorPoint, Integer> mapExact = exactLocation.get(frame);

        if(exactCountThreshold != countThreshold || mapExact == null) {
            if(mapExact == null)
                mapExact = new ConcurrentHashMap<ColorPoint, Integer>();

            ExecutorService threadPool = Executors.newFixedThreadPool((int) R.processor_cores);
            CompletionService<String> pool = new ExecutorCompletionService<String>(threadPool);

            pigmentCounter = new AtomicInteger(0);

            int chunk = (int) Math.ceil(colors.size() / R.processor_cores);

            int i;
            int nextChunk;
            for(i = 0; i < colors.size() - chunk; i = nextChunk + 1) {
                nextChunk = i + chunk;
                pool.submit(new MapExactLocationTask(colors, countThreshold, mapExact, i, nextChunk));
            }
            pool.submit(new MapExactLocationTask(colors, countThreshold, mapExact, i, colors.size() - 1));

            for(i = 0; i < R.processor_cores; i++)
                try {
                    pool.take().get();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                catch (ExecutionException e) {
                    e.printStackTrace();
                }

            threadPool.shutdown();

            exactCountThreshold = countThreshold;

            exactLocation.put(frame, mapExact);

            return true;
        }

        return false;
    }

    public void build() {
        colorPointMapVCH.clear();
        allColorPoints.clear();
        colorPointsRGB.clear();
        nearestNeighbor.clear();
        exactLocation.clear();

        for (int value = 0; value <= MyColor.MAX_VALUE; value += valueStepSize)
            for (int chroma = 0; chroma <= MyColor.MAX_CHROMA; chroma += chromaStepSize)
                for (int hue = 0; hue <= MyColor.MAX_HUE; hue += hueStepSize) {
                    MyColor color = MyColor.fromVCH(value, chroma, hue);

                    Vector position = color.getPosition();
                    position.subtractZ(MAX_VALUE_HALF); // center color model at (0, 0, 0) coordinates

                    String vchKey = value + "," + chroma + "," + hue;

                    if(!colorPointMapVCH.containsKey(vchKey)) {
                        ColorPoint colorPoint = new ColorPoint(position, color);
                        colorPointMapVCH.put(vchKey, colorPoint);
                        allColorPoints.put(colorPoint, colorPointMapVCH.size() - 1);

                        if (color.existsInRGB())
                            colorPointsRGB.put(colorPoint, colorPointMapVCH.size() - 1);

                        if(color.existsInCMYK())
                            colorPointsCMYK.put(colorPoint, colorPointMapVCH.size() - 1);
                    }
                }
    }

    private final class MapNearestNeighborTask implements Callable<String> {
        private List<MyColor> colors;
        private int countThreshold;
        private int begin;
        private int end;
        private ConcurrentHashMap<Integer, ColorPoint> mapNearest;

        public MapNearestNeighborTask(List<MyColor> colors, int countThreshold,
                                      ConcurrentHashMap<Integer, ColorPoint> mapNearest, int begin, int end) {
            this.colors = colors;
            this.countThreshold = countThreshold;
            this.begin = begin;
            this.end = end;
            this.mapNearest = mapNearest;
        }

        public String call(){
            for (int i = begin; i <= end; i++) {
                ColorPoint nearestSphere = getNearestSphere(colors.get(i), countThreshold); //returns reference

                if (nearestSphere != null) {
                    mapNearest.put(allColorPoints.get(nearestSphere), nearestSphere);
                    nearestSphere.setOpaque();
                }
            }

            return "Run";
        }
    }

    private final class MapExactLocationTask implements Callable<String> {
        private List<MyColor> colors;
        private int countThreshold;
        private int begin;
        private int end;
        private ConcurrentHashMap<ColorPoint, Integer> mapExact;

        public MapExactLocationTask(List<MyColor> colors, int countThreshold,
                                    ConcurrentHashMap<ColorPoint, Integer> mapExact, int begin, int end) {
            this.colors = colors;
            this.countThreshold = countThreshold;
            this.begin = begin;
            this.end = end;
            this.mapExact = mapExact;
        }

        public String call(){
            for (int i = begin; i <= end; i++) {
                MyColor color = colors.get(i);
                if (color.getCount() >= countThreshold) {
                    Vector position = color.getPosition();

                    position.subtractZ(MAX_VALUE_HALF); // center color model at (0, 0, 0) coordinates

                    ColorPoint colorPoint = new ColorPoint(position, color, false);

                    exactLocationMap.put(color.getId(), colorPoint);
                    mapExact.put(colorPoint, pigmentCounter.getAndIncrement());
                }
            }

            return "Run";
        }
    }
}