package com.mappingcolor.model.shape;

import com.jogamp.opengl.math.FloatUtil;
import com.mappingcolor.R;
import com.mappingcolor.model.Vector;

import java.util.ArrayList;
import java.util.List;

public class Circle {
    public static final int VERTICES = 8;

    private static List<Vector> vertices = new ArrayList<Vector>(VERTICES);
    private static float radius = R.DEFAULT_SPHERE_RADIUS / 10f;

    static {
        build();
    }

    public static float getRadius()
    {
        return radius;
    }

    public static void setRadius(float radius) {
        Circle.radius = radius;
        build();
    }

    public static List<Vector> getVertices() {
        return vertices;
    }

    private static void build() {
        vertices.clear();
        float theta = 2 * FloatUtil.PI / VERTICES;
        float cos = FloatUtil.cos(theta); //pre-calculate the sine and cosine
        final float sin = FloatUtil.sin(theta);

        if(radius == 0)
            radius = .1f;

        float t;
        float x = radius;//we start at angle = 0
        float y = 0;

        for (int ii = 0; ii < VERTICES; ii++) {
            vertices.add(new Vector(x, y, 0));
            //apply the rotation matrix
            t = x;
            x = cos * x - sin * y;
            y = sin * t + cos * y;
        }
    }
}
