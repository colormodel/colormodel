package com.mappingcolor.model.shape;

import com.mappingcolor.R;
import com.mappingcolor.model.Vector;

import java.util.ArrayList;
import java.util.List;

public class Icosahedron {

    private static List<Vector> vertexNormalsList = new ArrayList<Vector>();
    private static List<Vector> vertices = new ArrayList<Vector>();

    private static int resolution = 0;
    private static float radius = R.DEFAULT_SPHERE_RADIUS /10f;

    static {
        build();
    }

    public static float getRadius()
    {
        return radius;
    }

    public static void setRadius(float radius) {
        Icosahedron.radius = radius;
        build();
    }

    public static List<Vector> getVertices() {
        return vertices;
    }

    private static void add(Vector vector) {
        vertices.add(vector);
        vertexNormalsList.add(vector);
    }

    private static void build() {
        final float X = 0.525731112119133606f;
        final float Z = 0.850650808352039932f;

        final Vector[] vertexData = {
                new Vector(-X, 0.0f, Z), new Vector(X, 0.0f, Z), new Vector(-X, 0.0f, -Z), new Vector(X, 0.0f, -Z),
                new Vector(0.0f, Z, X), new Vector(0.0f, Z, -X), new Vector(0.0f, -Z, X), new Vector(0.0f, -Z, -X),
                new Vector(Z, X, 0.0f), new Vector(-Z, X, 0.0f), new Vector(Z, -X, 0.0f), new Vector(-Z, -X, 0.0f)
        };

        final int[][] tindices = {
                {0, 4, 1}, {0, 9, 4}, {9, 5, 4}, {4, 5, 8}, {4, 8, 1},
                {8, 10, 1}, {8, 3, 10}, {5, 3, 8}, {5, 2, 3}, {2, 7, 3},
                {7, 10, 3}, {7, 6, 10}, {7, 11, 6}, {11, 0, 6}, {0, 1, 6},
                {6, 1, 10}, {9, 0, 11}, {9, 11, 2}, {9, 2, 5}, {7, 2, 11}
        };

        vertices.clear();
        for (int i = 0; i < 20; ++i)
            subdivide(vertexData[tindices[i][0]], vertexData[tindices[i][1]], vertexData[tindices[i][2]], resolution);

        //for(Vector vertex : vertices)
        //    vertex.scale(1.2f);
    }

    private static void subdivide(Vector v1, Vector v2, Vector v3, int depth) {
        if (depth == 0) {
            add(v1);
            add(v2);
            add(v3);
            return;
        }

        Vector v12 = Vector.add(v1, v2).normalize();
        Vector v23 = Vector.add(v2, v3).normalize();
        Vector v31 = Vector.add(v3, v1).normalize();

        subdivide(v1, v12, v31, depth - 1);
        subdivide(v2, v23, v12, depth - 1);
        subdivide(v3, v31, v23, depth - 1);
        subdivide(v12, v23, v31, depth - 1);
    }
}
