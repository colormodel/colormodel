package com.mappingcolor.model;

import com.jogamp.opengl.math.FloatUtil;
import com.mappingcolor.R;

public class Camera {
    public static final float HORIZONTAL_MOVE = Camera.TRANSLATE_DELTA / R.WINDOW_WIDTH;
    public static final float VERTICAL_MOVE = Camera.TRANSLATE_DELTA / R.WINDOW_HEIGHT;
    public static final Vector INITIAL_POSITION = new Vector(0, 0, -200);

    public static Vector offset = new Vector(INITIAL_POSITION);
    public static Vector position = new Vector(offset);
    public static Vector focus = new Vector(); // arcballCenter
    public static Vector rotation = new Vector();
    public static Vector forward = new Vector();

    public static Vector up = new Vector(0,1,0);
    public static Vector right = new Vector(1,0,0);

    public static final float VIEW_ANGLE = 60; // field of build angle (fovy), often used with gluPerspective
    public static final float Z_NEAR = 1;
    public static final float Z_FAR = 1000;
    public static final float MAX_DISTANCE = Z_FAR / 2;
    public static final float MIN_DISTANCE = -MAX_DISTANCE;

    public static final float ROTATION_DELTA = 0.2f;
    public static final float TRANSLATE_DELTA = 200;
    public static final float ZOOM_DELTA = 10;

    public static Vector pos = new Vector(INITIAL_POSITION);

    //public static ArcBall arcBall = new ArcBall(R.WINDOW_WIDTH, R.WINDOW_HEIGHT);

    public static void reset() {
        position.set(INITIAL_POSITION);
        focus.set(Vector.ZERO_VECTOR);
        rotation.set(Vector.ZERO_VECTOR);
    }

    public static void zoom(float z) {
        position.addZ(z);

        if(position.getZ() > MAX_DISTANCE)
            position.setZ(MAX_DISTANCE);
        if(position.getZ() < MIN_DISTANCE)
            position.setZ(MIN_DISTANCE);

        //System.out.println("POS: " + position);
    }

    public static void move(float x, float y) {
        x *= HORIZONTAL_MOVE;
        y *= VERTICAL_MOVE;

        position.addX(x);
        position.addY(y);

        if(position.getX() > MAX_DISTANCE)
            position.setX(MAX_DISTANCE);
        if(position.getX() < MIN_DISTANCE)
            position.setX(MIN_DISTANCE);

        if(position.getY() > MAX_DISTANCE)
            position.setY(MAX_DISTANCE);
        if(position.getY() < MIN_DISTANCE)
            position.setY(MIN_DISTANCE);

        focus.setX(position.getX() - offset.getX());
        focus.setY(position.getY() - offset.getY());

        //System.out.println("POS: " + position + "\tFOC:" + focus);
    }

    public static void rotate(float x, float y) {
        x *= ROTATION_DELTA;
        y *= ROTATION_DELTA;

        float[] r = rotation.getArray();
        r[0] = (r[0] + x) % 360;
        r[1] = (r[1] + y) % 360;

        //getRotationNormal();
    }

    public static Vector getRotationNormal()
    {
        Vector v = VectorMath.rotate(new Vector(offset), rotation.getX(), rotation.getY(), 0);
        Vector rotationNormal;

        //pos = VectorMath.rotate(new Vector(v), 0, 180, 0);
        pos = new Vector(v);

        if ((rotation.getY() > 90f && rotation.getY() < 270f) || (rotation.getY() < -90f && rotation.getY() > -270f)) {
            Vector u = new Vector(offset).scale(-1);
            rotationNormal = getAngleVector(v, u);
        }
        else
            rotationNormal = getAngleVector(v, new Vector(Camera.offset));

        return rotationNormal.scale(-1);
    }

    private static Vector getAngleVector(Vector vector1, Vector vector2)
    {
        Vector v1 = new Vector(vector1);
        Vector v2 = new Vector(vector2);

        if(Vector.subtract(v2, v1).magnitude() > 0.0001f)
        {
            v2.normalize();
            v1.normalize();

            Vector rotationAxis = Vector.cross(v2, v1);
            rotationAxis.normalize();

            double val = Vector.dot(v2, v1);

            //(val > (1-1E-10) ? val=1.0f : val=val ;
            if(val > (1-0.0000000001))
                val = 1.0;

            float rotationAngle = (float)(Math.acos(val) * 180/Math.PI);
            float r = rotationAngle;
            return new Vector(r * rotationAxis.getX(), r * rotationAxis.getY(), r * rotationAxis.getZ());
        }
        else
            return new Vector(0, 0, 0);
    }

    public static void orbit(float x, float y) {
        x *= ROTATION_DELTA;
        y *= ROTATION_DELTA;

        float xCos = FloatUtil.cos(x * VectorMath.DEG_TO_RAD);
        float xSin = FloatUtil.sin(x * VectorMath.DEG_TO_RAD);
        float yCos = FloatUtil.cos(y * VectorMath.DEG_TO_RAD);
        float ySin = FloatUtil.sin(y * VectorMath.DEG_TO_RAD);

        //xz plane
        float xPos = xCos * position.getX() - xSin * position.getZ();
        float zPos = xSin * position.getX() + xCos * position.getZ();

        //yz plane
        float yPos = yCos * position.getY() - ySin * zPos;
        zPos = ySin * position.getY() + yCos * zPos;

        position.set(xPos, yPos, zPos);

        forward = Vector.subtract(position, focus).normalize();
        right = Vector.cross(Vector.Y_AXIS, forward).normalize();
        up = Vector.cross(forward, right);

        System.out.println("POS: " + position + "\tFOCUS:" + focus  + "\tUP:" + up + "\tROT: " + rotation);
    }

    public static float[] getLookAtMatrix() {
        return new float[] {
                right.getX(),   up.getX(), forward.getX(),   0,
                right.getY(),   up.getY(), forward.getY(),   0,
                right.getZ(),   up.getZ(), forward.getZ(),   0,
                -Vector.dot(right, position),   -Vector.dot(up, position), -Vector.dot(forward, position), 1
        };
    }
}
