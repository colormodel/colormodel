package com.mappingcolor.model;

import com.jogamp.common.nio.Buffers;
import com.mappingcolor.R;
import com.mappingcolor.model.shape.Circle;
import com.mappingcolor.model.shape.Icosahedron;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import java.awt.Color;

public class ColorPoint {
    public static final MyColor WHITE_OUTLINE = MyColor.fromColor(Color.WHITE);

    private static float alpha = R.DEFAULT_COLOR_POINT_ALPHA / 100f;
    private static float wireframeAlpha = alpha;

    private Vector position;
    private MyColor color;
    private boolean transparent;
    private boolean mutated = false;

    public ColorPoint(Vector position, MyColor color) {
        this(position, color, !color.existsInRGB());
    }

    public ColorPoint(Vector position, MyColor color, boolean transparent) {
        this.position = position;
        this.color = color;
        this.transparent = transparent;
    }

    public ColorPoint(ColorPoint colorPoint) {
        this.position = colorPoint.position;
        this.color = colorPoint.color;
        this.transparent = colorPoint.transparent;
    }

    public Vector getPosition() { return position; }

    public MyColor getColor()
    {
        return color;
    }

    public static void setRadius(float radius) {
        Circle.setRadius(radius);
        Icosahedron.setRadius(radius);
    }

    public static void setAlpha(float alpha) {
        ColorPoint.alpha = alpha;
    }

    public static float getAlpha() {
        return alpha;
    }

    public static void setWireframeAlpha(float wireframeAlpha)
    {
        ColorPoint.wireframeAlpha = wireframeAlpha;
    }

    public static float getWireframeAlpha() {
        return wireframeAlpha;
    }

    public void setOpaque() { transparent = false; mutated = true; }

    public void setTransparent() { transparent = true; mutated = true; }

    public boolean isTransparent() {
        return transparent;
    }

    public void setMutated(boolean mutated) {
        this.mutated = mutated;
    }

    public void draw(GL2 gl)
    {
        if(transparent) {
            if (ColorModel.wireframeMode && wireframeAlpha != 0)
                drawWireFrame(gl);

            if (alpha == 0)
                return;
            if(ColorModel.drawMode == ColorModel.DrawMode.CMYK)
                gl.glColor4f(color.getC(), color.getY(), color.getM(), alpha);
            else
                gl.glColor4f(color.getRedf(), color.getGreenf(), color.getBluef(), alpha);
        }
        else {
            if(ColorModel.drawMode == ColorModel.DrawMode.CMYK)
                gl.glColor3f(color.getC(), color.getY(), color.getM());
            else
                gl.glColor3f(color.getRedf(), color.getGreenf(), color.getBluef());
        }

        gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
        gl.glCullFace(GL2.GL_BACK);

        gl.glPushMatrix();

        // move circle into position
        gl.glTranslatef(position.getX(), position.getY(), position.getZ());
        //gl.glMultMatrixf(getBillboardMatrix(), 0);

        // rotate the circle to face the camera
        gl.glRotatef(-Camera.rotation.getX(), 1, 0, 0);
        gl.glRotatef(-Camera.rotation.getY(), 0, 1, 0);

        gl.glBegin(GL2.GL_POLYGON);

        for(Vector vertex : Circle.getVertices())
            gl.glVertex3f(vertex.getX(), vertex.getY(), vertex.getZ());

        gl.glEnd();
        gl.glPopMatrix();
    }

    public void drawWireFrame(GL2 gl) {
        gl.glColor4f(WHITE_OUTLINE.getRedf(), WHITE_OUTLINE.getGreenf(), WHITE_OUTLINE.getBluef(), wireframeAlpha);
        gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE);
        gl.glCullFace(GL2.GL_BACK);
        gl.glPushMatrix();

        // move circle into position
        gl.glTranslatef(position.getX(), position.getY(), position.getZ());

        // rotate the circle to face the camera
        gl.glRotatef(-Camera.rotation.getX(), 1, 0, 0);
        gl.glRotatef(-Camera.rotation.getY(), 0, 1, 0);
        //gl.glRotatef(Camera.rotation.getZ(), 0, 0, 1);

        gl.glBegin(GL2.GL_POLYGON);

        for(Vector vertex : Circle.getVertices())
            gl.glVertex3f(vertex.getX(), vertex.getY(), vertex.getZ());

        gl.glEnd();
        gl.glPopMatrix();
    }

    public void drawVBO(GL2 gl, int i) {
        if(mutated) {
            VBOHelper.updateSingleColorPointVertexColorVBO(gl, this, i);
            mutated = false;
        }

        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL2.GL_COLOR_ARRAY);
        gl.glCullFace(GL2.GL_BACK);

        if(transparent) {
            if (ColorModel.wireframeMode && wireframeAlpha != 0)
                drawWireFrameVBO(gl, i);

            if (alpha == 0) // this should work to speed up performance
                return;
        }

        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBOHelper.getBinderId(VBOHelper.VBO_SEGMENT.COLOR_POINT, i));
        gl.glVertexPointer(VBOHelper.POINTS_PER_VERTEX, GL2.GL_FLOAT, 7 * Buffers.SIZEOF_FLOAT, 0);
        gl.glColorPointer(VBOHelper.POINTS_PER_COLOR, GL2.GL_FLOAT, 7 * Buffers.SIZEOF_FLOAT, VBOHelper.POINTS_PER_VERTEX * Buffers.SIZEOF_FLOAT);

        gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
        gl.glDrawArrays(GL2.GL_TRIANGLES, 0, VBOHelper.shapeVertices.size());

        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL2.GL_COLOR_ARRAY);
    }

    public void drawPigmentVBO(GL2 gl, int i) {
        if(mutated) {
            VBOHelper.updateSingleColorPointVertexColorVBO(gl, this, i);
            mutated = false;
        }

        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL2.GL_COLOR_ARRAY);
        gl.glCullFace(GL2.GL_BACK);

        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBOHelper.getBinderId(VBOHelper.VBO_SEGMENT.PIGMENT, i));
        gl.glVertexPointer(VBOHelper.POINTS_PER_VERTEX, GL2.GL_FLOAT, 7 * Buffers.SIZEOF_FLOAT, 0);
        gl.glColorPointer(VBOHelper.POINTS_PER_COLOR, GL2.GL_FLOAT, 7 * Buffers.SIZEOF_FLOAT, VBOHelper.POINTS_PER_VERTEX * Buffers.SIZEOF_FLOAT);

        gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
        gl.glDrawArrays(GL2.GL_TRIANGLES, 0, VBOHelper.shapeVertices.size());

        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL2.GL_COLOR_ARRAY);
    }

    private void drawWireFrameVBO(GL2 gl, int i) {
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBOHelper.getBinderId(VBOHelper.VBO_SEGMENT.WIREFRAME, i));
        gl.glVertexPointer(VBOHelper.POINTS_PER_VERTEX, GL2.GL_FLOAT, 7 * Buffers.SIZEOF_FLOAT, 0);
        gl.glColorPointer(VBOHelper.POINTS_PER_COLOR, GL2.GL_FLOAT, 7 * Buffers.SIZEOF_FLOAT, VBOHelper.POINTS_PER_VERTEX * Buffers.SIZEOF_FLOAT);
        gl.glDrawArrays(GL2.GL_LINES, 0, VBOHelper.shapeVertices.size());
    }

    private float[] getBillboardMatrix() {
        return new float[] {
                Camera.right.getX(),   Camera.up.getX(), -Camera.forward.getX(),   0,
                Camera.right.getY(),   Camera.up.getY(), -Camera.forward.getY(),   0,
                Camera.right.getZ(),   Camera.up.getZ(), -Camera.forward.getZ(),   0,
                -Vector.dot(Camera.right, position),   -Vector.dot(Camera.up, position), -Vector.dot(Camera.forward, position), 1
        };
    }
}