package com.mappingcolor.model;

import com.jogamp.opengl.math.FloatUtil;

public class Vector
{
    public static final Vector ZERO_VECTOR = new Vector(0,0,0);
    public static final Vector X_AXIS = new Vector(1,0,0);
    public static final Vector Y_AXIS = new Vector(0,1,0);
    public static final Vector Z_AXIS = new Vector(0,0,1);

    private static final int X = 0;
    private static final int Y = 1;
    private static final int Z = 2;

    private float[] V = new float[3];

    public Vector() {
        V[X] = 0;
        V[Y] = 0;
        V[Z] = 0;
    }
    public Vector(float...XYZ)
    {
        V[X] = (XYZ.length > 0) ? XYZ[0] : 0;
        V[Y] = (XYZ.length > 1) ? XYZ[1] : 0;
        V[Z] = (XYZ.length > 2) ? XYZ[2] : 0;
    }

    public Vector(Vector other)
    {
        set(other);
    }

    public Vector add(Vector other)
    {
        V[X] += other.getX();
        V[Y] += other.getY();
        V[Z] += other.getZ();

        return this;
    }

    public void addX(float x)
    {
        V[X] += x;
    }

    public void addY(float y)
    {
        V[Y] += y;
    }

    public void addZ(float z)
    {
        V[Z] += z;
    }

    public Vector cross(Vector other)
    {
        float x = V[Y] * other.getZ() - V[Z] * other.getY();
        float y = other.getX() * V[Z] - other.getZ() * V[X];
        float z = V[X] * other.getY() - V[Y] * other.getX();

        return new Vector(x, y, z);
    }

    public float dot(Vector other) {
        return (V[X]*other.getX() + V[Y]*other.getY() + V[Z]*other.getZ());
    }

    public float getX()
    {
        return V[X];
    }

    public float getY()
    {
        return V[Y];
    }

    public float getZ()
    {
        return V[Z];
    }

    public float[] getArray()
    {
        return V;
    }

    public float magnitude()
    {
        return FloatUtil.sqrt(V[X]*V[X] + V[Y]*V[Y] + V[Z]*V[Z]);
    }

    public Vector normalize()
    {
        float magnitude = magnitude();
        V[X] /= magnitude;
        V[Y] /= magnitude;
        V[Z] /= magnitude;

        return this;
    }

    public Vector scale(float scalar)
    {
        V[X] *= scalar;
        V[Y] *= scalar;
        V[Z] *= scalar;

        return this;
    }

    public void set(Vector other)
    {
        V[X] = other.getX();
        V[Y] = other.getY();
        V[Z] = other.getZ();
    }

    public void set(float...array){
        V[X] = array[0];
        V[Y] = array[1];
        V[Z] = array[2];
    }

    public void set(float x, float y, float z) {
        V[X] = x;
        V[Y] = y;
        V[Z] = z;
    }

    public void setX(float x)
    {
        V[X] = x;
    }

    public void setY(float y)
    {
        V[Y] = y;
    }

    public void setZ(float z)
    {
        V[Z] = z;
    }

    public Vector subtract(Vector other)
    {
        V[X] -= other.getX();
        V[Y] -= other.getY();
        V[Z] -= other.getZ();

        return this;
    }

    public void subtractX(float x)
    {
        V[X] -= x;
    }

    public void subtractY(float y)
    {
        V[Y] -= y;
    }

    public void subtractZ(float z)
    {
        V[Z] -= z;
    }

    public String toString()
    {
        return "(" + V[X] + ", " + V[Y] + ", " + V[Z] + ")";
    }

    public static Vector add(Vector...vectors) {
        Vector sum = new Vector();
        for(Vector v : vectors)
            sum.add(v);

        return sum;
    }

    public static Vector cross(Vector v1, Vector v2)
    {
        float x = v1.getY() * v2.getZ() - v1.getZ() * v2.getY();
        float y = v2.getX() * v1.getZ() - v2.getZ() * v1.getX();
        float z = v1.getX() * v2.getY() - v1.getY() * v2.getX();

        return new Vector(x, y, z);
    }

    public static float dot(Vector v1, Vector v2) {
        return (v1.getX()*v2.getX() + v1.getY()*v2.getY() + v1.getZ()*v2.getZ());
    }

    // does not modify the vector input, unlike the non-static method
    public static Vector normalize(Vector vector) {
        return new Vector(vector).normalize();
    }

    public static Vector scale(Vector vector, float scalar) {
        return new Vector(vector).scale(scalar);
    }

    public static Vector subtract(Vector...vectors) {
        Vector difference = new Vector(vectors[0]);
        for(int i = 1; i < vectors.length; i++)
            difference.subtract(vectors[i]);

        return difference;
    }
}
