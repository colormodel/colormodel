package com.mappingcolor.model;

import com.jogamp.common.nio.Buffers;
import com.mappingcolor.controller.Mouse;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import java.nio.IntBuffer;

public class ColorPointSelector {
    private static final int BUFFER_SIZE = 512;

    private ColorModel colorModel;

    public ColorPointSelector(ColorModel colorModel) {
        this.colorModel = colorModel;
    }

    public void pick(GLU glu, GL2 gl) {
        if(Mouse.isButton1Down()) {
            int[] selectBuf = new int[BUFFER_SIZE];
            IntBuffer selectBuffer = Buffers.newDirectIntBuffer(BUFFER_SIZE);
            int hits;
            int viewport[] = new int[4];
            // int x, y;

            gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);

            gl.glSelectBuffer(BUFFER_SIZE, selectBuffer);
            gl.glRenderMode(GL2.GL_SELECT);

            gl.glInitNames();
            gl.glPushName(-1);

            gl.glMatrixMode(GL2.GL_PROJECTION);
            gl.glPushMatrix();
            gl.glLoadIdentity();
    /* create 5x5 pixel picking region near cursor location */
            glu.gluPickMatrix((double) Mouse.getClickPointX(),
                    (double) (viewport[3] - Mouse.getClickPointY()), //
                    5.0, 5.0, viewport, 0);
            gl.glOrtho(0.0, 8.0, 0.0, 8.0, -0.5, 2.5);
            //drawRects(gl, GL2.GL_SELECT);
            gl.glPopMatrix();
            gl.glFlush();

            hits = gl.glRenderMode(GL2.GL_RENDER);
            selectBuffer.get(selectBuf);
            //processHits(hits, selectBuf);
        }
    }
}
