package com.mappingcolor.model;

import com.mappingcolor.R;

import javax.media.opengl.GL2;

public class ColorTracker {
    public static final int LEFT = 0;
    public static final int RIGHT = 1;

    private static final float AXIS_LENGTH = Camera.Z_FAR;
    private static final short pattern = (short)0xCCCC;

    private MyColor color = R.backgroundColor;
    private Vector position = Vector.ZERO_VECTOR;
    private boolean colorSelected = false;

    public void setColor(MyColor color) {
        this.color = color;
        position = color.getPosition();
        position.subtractZ(ColorModel.MAX_VALUE_HALF); // center color model at (0, 0, 0) coordinates
    }

    public MyColor getColor() {
        return color;
    }

    public void colorSelected(boolean isTracking) {
        this.colorSelected = isTracking;
    }

    public boolean isColorSelected() {
        return colorSelected;
    }

    public void draw(GL2 gl) {
        if(colorSelected) {
            gl.glPushMatrix();
            gl.glPushAttrib(GL2.GL_ENABLE_BIT);

            gl.glLineStipple(1, pattern);  //# [1]
            gl.glEnable(GL2.GL_LINE_STIPPLE);

            // x
            gl.glColor3f(color.getRedf(), color.getGreenf(), color.getBluef());
            gl.glBegin(GL2.GL_LINES);
            gl.glVertex3f(position.getX() - AXIS_LENGTH, position.getY(), position.getZ());
            gl.glVertex3f(position.getX() + AXIS_LENGTH, position.getY(), position.getZ());
            gl.glEnd();

            // y
            gl.glColor3f(color.getRedf(), color.getGreenf(), color.getBluef());
            gl.glBegin(GL2.GL_LINES);
            gl.glVertex3f(position.getX(), position.getY() - AXIS_LENGTH, position.getZ());
            gl.glVertex3f(position.getX(), position.getY() + AXIS_LENGTH, position.getZ());
            gl.glEnd();

            // z
            gl.glColor3f(color.getRedf(), color.getGreenf(), color.getBluef());
            gl.glBegin(GL2.GL_LINES);
            gl.glVertex3f(position.getX(), position.getY(), position.getZ() - AXIS_LENGTH);
            gl.glVertex3f(position.getX(), position.getY(), position.getZ() + AXIS_LENGTH);
            gl.glEnd();

            gl.glPopAttrib();
            gl.glPopMatrix();
        }
    }
}
