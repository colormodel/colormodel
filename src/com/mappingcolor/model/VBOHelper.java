package com.mappingcolor.model;

import com.jogamp.common.nio.Buffers;
import com.mappingcolor.gui.MyView;
import com.mappingcolor.model.shape.Icosahedron;

import javax.media.opengl.GL2;
import java.nio.FloatBuffer;
import java.util.List;
import java.util.Map;

public class VBOHelper {
    public static enum VBO_SEGMENT { COLOR_POINT, WIREFRAME, PIGMENT }

    public static final int POINTS_PER_VERTEX = 3;
    public static final int POINTS_PER_COLOR = 4;

    private static final int INITIAL_PIGMENT_SIZE = 200000;
    private static final int PIGMENT_STEP_SIZE = 4000;

    private static int[] VBO;
    public static final List<Vector> shapeVertices = Icosahedron.getVertices();

    private static int loadedVBOs = 0;

    private static int colorPointSegmentStart;
    private static int wireframeSegmentStart;
    private static int pigmentSegmentStart;

    private static int colorPointSize;
    public static int pigmentSize;

    public static int getBinderId(VBO_SEGMENT vboSegment, int id) {
        if(VBO == null)
            return 0;

        switch (vboSegment) {
            case COLOR_POINT:
                return VBO[colorPointSegmentStart + id];
            case WIREFRAME:
                return VBO[wireframeSegmentStart + id];
            case PIGMENT:
                return VBO[pigmentSegmentStart + id];
            default:
                return 0;
        }
    }

    public static void createVBOs(GL2 gl, Map<ColorPoint, Integer> colorPoints, Map<ColorPoint, Integer> pigments) {
        colorPointSize = colorPoints.size();
        int wireframeSize = colorPointSize;
        pigmentSize = (INITIAL_PIGMENT_SIZE > pigments.size()) ? INITIAL_PIGMENT_SIZE : pigments.size();

        colorPointSegmentStart = 0;
        wireframeSegmentStart = colorPointSize;
        pigmentSegmentStart = wireframeSegmentStart + wireframeSize;

        MyView.loadingBar.start(pigmentSegmentStart + ((INITIAL_PIGMENT_SIZE > pigments.size()) ? pigments.size() : INITIAL_PIGMENT_SIZE), "Color Model");

        VBO = new int[pigmentSegmentStart + getPigmentStepSize()]; //one segment for color points, one segment for wireframes

        gl.glGenBuffers(VBO.length, VBO, 0);

        loadedVBOs = 0;

        updateColorPointVBOs(gl, colorPoints);

        if(pigments.size() > 0)
            updatePigmentVBOs(gl, pigments, false);
    }

    private static int getPigmentStepSize() {
        int temp = INITIAL_PIGMENT_SIZE;

        while(temp < pigmentSize)
            temp += PIGMENT_STEP_SIZE;

        return temp;
    }

    private static void updateColorPointVBOs(GL2 gl, Map<ColorPoint, Integer> colorPoints) {
        synchronized (VBOHelper.class) {
            for (Map.Entry<ColorPoint, Integer> entry : colorPoints.entrySet()) {
                ColorPoint colorPoint = entry.getKey();
                MyColor color = colorPoint.getColor();
                FloatBuffer colorPointVertices = Buffers.newDirectFloatBuffer(shapeVertices.size() * POINTS_PER_VERTEX + shapeVertices.size() * POINTS_PER_COLOR);
                FloatBuffer wireframeVertices = Buffers.newDirectFloatBuffer(shapeVertices.size() * POINTS_PER_VERTEX + shapeVertices.size() * POINTS_PER_COLOR);

                gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBO[colorPointSegmentStart + entry.getValue()]);
                gl.glBufferData(GL2.GL_ARRAY_BUFFER, colorPointVertices.capacity() * Buffers.SIZEOF_FLOAT, null, GL2.GL_DYNAMIC_DRAW);

                for (Vector vertex : shapeVertices) {
                    colorPointVertices.put(vertex.getX() * Icosahedron.getRadius() + colorPoint.getPosition().getX());
                    colorPointVertices.put(vertex.getY() * Icosahedron.getRadius() + colorPoint.getPosition().getY());
                    colorPointVertices.put(vertex.getZ() * Icosahedron.getRadius() + colorPoint.getPosition().getZ());
                    if (ColorModel.drawMode == ColorModel.DrawMode.CMYK) {
                        colorPointVertices.put(color.getC());
                        colorPointVertices.put(color.getY());
                        colorPointVertices.put(color.getM());
                    } else {
                        colorPointVertices.put(color.getRedf());
                        colorPointVertices.put(color.getGreenf());
                        colorPointVertices.put(color.getBluef());
                    }
                    colorPointVertices.put(((colorPoint.isTransparent()) ? ColorPoint.getAlpha() : 1));

                    wireframeVertices.put(vertex.getX() * Icosahedron.getRadius() + colorPoint.getPosition().getX());
                    wireframeVertices.put(vertex.getY() * Icosahedron.getRadius() + colorPoint.getPosition().getY());
                    wireframeVertices.put(vertex.getZ() * Icosahedron.getRadius() + colorPoint.getPosition().getZ());
                    wireframeVertices.put(ColorPoint.WHITE_OUTLINE.getRedf());
                    wireframeVertices.put(ColorPoint.WHITE_OUTLINE.getGreenf());
                    wireframeVertices.put(ColorPoint.WHITE_OUTLINE.getBluef());
                    wireframeVertices.put(ColorPoint.getWireframeAlpha());
                }
                colorPointVertices.rewind();
                wireframeVertices.rewind();

                gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, 0, colorPointVertices.capacity() * Buffers.SIZEOF_FLOAT, colorPointVertices);

                gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBO[wireframeSegmentStart + entry.getValue()]);
                gl.glBufferData(GL2.GL_ARRAY_BUFFER, wireframeVertices.capacity() * Buffers.SIZEOF_FLOAT, null, GL2.GL_DYNAMIC_DRAW);
                gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, 0, wireframeVertices.capacity() * Buffers.SIZEOF_FLOAT, wireframeVertices);

                loadedVBOs = loadedVBOs + 2;
                MyView.loadingBar.updateProgress(loadedVBOs);
            }
        }
    }

    public static void updatePigmentVBOs(GL2 gl, Map<ColorPoint, Integer> pigments, boolean showLoadingBar) {
        if(showLoadingBar) {
            MyView.loadingBar.start(pigments.size(), "Color Model");
            loadedVBOs = 0;
        }

        synchronized (VBOHelper.class) {
            for (Map.Entry<ColorPoint, Integer> entry : pigments.entrySet()) {
                ColorPoint colorPoint = entry.getKey();
                MyColor color = colorPoint.getColor();
                FloatBuffer vertices = Buffers.newDirectFloatBuffer(shapeVertices.size() * POINTS_PER_VERTEX + shapeVertices.size() * POINTS_PER_COLOR);

                gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBO[pigmentSegmentStart + entry.getValue()]);
                gl.glBufferData(GL2.GL_ARRAY_BUFFER, vertices.capacity() * Buffers.SIZEOF_FLOAT, null, GL2.GL_DYNAMIC_DRAW);

                for (Vector vertex : shapeVertices) {
                    vertices.put(vertex.getX() * Icosahedron.getRadius() + colorPoint.getPosition().getX());
                    vertices.put(vertex.getY() * Icosahedron.getRadius() + colorPoint.getPosition().getY());
                    vertices.put(vertex.getZ() * Icosahedron.getRadius() + colorPoint.getPosition().getZ());
                    if (ColorModel.drawMode == ColorModel.DrawMode.CMYK) {
                        vertices.put(color.getC());
                        vertices.put(color.getY());
                        vertices.put(color.getM());
                    } else {
                        vertices.put(color.getRedf());
                        vertices.put(color.getGreenf());
                        vertices.put(color.getBluef());
                    }
                    vertices.put(((colorPoint.isTransparent()) ? ColorPoint.getAlpha() : 1));
                }
                vertices.rewind();

                gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, 0, vertices.capacity() * Buffers.SIZEOF_FLOAT, vertices);

                loadedVBOs++;
                MyView.loadingBar.updateProgress(loadedVBOs);
            }
        }
    }

    public static void updateColorPointVertexColorVBOs(GL2 gl, Map<ColorPoint, Integer> colorPoints) {
        MyView.loadingBar.start(colorPointSize, "Color Points");
        loadedVBOs = 0;

        synchronized (VBOHelper.class) {
            for (Map.Entry<ColorPoint, Integer> entry : colorPoints.entrySet()) {
                ColorPoint colorPoint = entry.getKey();
                MyColor color = colorPoint.getColor();
                FloatBuffer vertices = Buffers.newDirectFloatBuffer(shapeVertices.size() * POINTS_PER_VERTEX + shapeVertices.size() * POINTS_PER_COLOR);

                gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBO[colorPointSegmentStart + entry.getValue()]);
                gl.glBufferData(GL2.GL_ARRAY_BUFFER, vertices.capacity() * Buffers.SIZEOF_FLOAT, null, GL2.GL_DYNAMIC_DRAW);

                for (Vector vertex : shapeVertices) {
                    vertices.put(vertex.getX() * Icosahedron.getRadius() + colorPoint.getPosition().getX());
                    vertices.put(vertex.getY() * Icosahedron.getRadius() + colorPoint.getPosition().getY());
                    vertices.put(vertex.getZ() * Icosahedron.getRadius() + colorPoint.getPosition().getZ());
                    if (ColorModel.drawMode == ColorModel.DrawMode.CMYK) {
                        vertices.put(color.getC());
                        vertices.put(color.getY());
                        vertices.put(color.getM());
                    } else {
                        vertices.put(color.getRedf());
                        vertices.put(color.getGreenf());
                        vertices.put(color.getBluef());
                    }
                    vertices.put(((colorPoint.isTransparent()) ? ColorPoint.getAlpha() : 1));
                }
                vertices.rewind();

                gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, 0, vertices.capacity() * Buffers.SIZEOF_FLOAT, vertices);

                loadedVBOs++;
                MyView.loadingBar.updateProgress(loadedVBOs);
            }
        }
    }

    public static void updateSingleColorPointVertexColorVBO(GL2 gl, ColorPoint colorPoint, int i) {
        synchronized (VBOHelper.class) {
            MyColor color = colorPoint.getColor();
            FloatBuffer vertices = Buffers.newDirectFloatBuffer(shapeVertices.size() * POINTS_PER_VERTEX + shapeVertices.size() * POINTS_PER_COLOR);

            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBO[colorPointSegmentStart + i]);
            gl.glBufferData(GL2.GL_ARRAY_BUFFER, vertices.capacity() * Buffers.SIZEOF_FLOAT, null, GL2.GL_DYNAMIC_DRAW);

            for (Vector vertex : shapeVertices) {
                vertices.put(vertex.getX() * Icosahedron.getRadius() + colorPoint.getPosition().getX());
                vertices.put(vertex.getY() * Icosahedron.getRadius() + colorPoint.getPosition().getY());
                vertices.put(vertex.getZ() * Icosahedron.getRadius() + colorPoint.getPosition().getZ());
                vertices.put(color.getRedf());
                vertices.put(color.getGreenf());
                vertices.put(color.getBluef());
                vertices.put(((colorPoint.isTransparent()) ? ColorPoint.getAlpha() : 1));
            }
            vertices.rewind();

            gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, 0, vertices.capacity() * Buffers.SIZEOF_FLOAT, vertices);
        }
    }

    public static void updateWireframeVertexColorVBOs(GL2 gl, Map<ColorPoint, Integer> colorPoints) {
        MyView.loadingBar.start(colorPointSize, "Wireframes");
        loadedVBOs = 0;

        synchronized (VBOHelper.class) {
            for (Map.Entry<ColorPoint, Integer> entry : colorPoints.entrySet()) {
                ColorPoint colorPoint = entry.getKey();
                FloatBuffer vertices = Buffers.newDirectFloatBuffer(shapeVertices.size() * POINTS_PER_VERTEX + shapeVertices.size() * POINTS_PER_COLOR);

                gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBO[wireframeSegmentStart + entry.getValue()]);
                gl.glBufferData(GL2.GL_ARRAY_BUFFER, vertices.capacity() * Buffers.SIZEOF_FLOAT, null, GL2.GL_DYNAMIC_DRAW);

                for (Vector vertex : shapeVertices) {
                    vertices.put(vertex.getX() * (Icosahedron.getRadius() + .1f) + colorPoint.getPosition().getX());
                    vertices.put(vertex.getY() * (Icosahedron.getRadius() + .1f) + colorPoint.getPosition().getY());
                    vertices.put(vertex.getZ() * (Icosahedron.getRadius() + .1f) + colorPoint.getPosition().getZ());
                    vertices.put(ColorPoint.WHITE_OUTLINE.getRedf());
                    vertices.put(ColorPoint.WHITE_OUTLINE.getGreenf());
                    vertices.put(ColorPoint.WHITE_OUTLINE.getBluef());
                    vertices.put(ColorPoint.getWireframeAlpha());
                }
                vertices.rewind();

                gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, 0, vertices.capacity() * Buffers.SIZEOF_FLOAT, vertices);

                loadedVBOs++;
                MyView.loadingBar.updateProgress(loadedVBOs);
            }
        }
    }
}
