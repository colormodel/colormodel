package com.mappingcolor;

import com.mappingcolor.gui.MyView;
import com.mappingcolor.io.file.FileData;
import com.mappingcolor.io.image.ImageData;
import com.mappingcolor.io.video.VideoData;
import com.mappingcolor.io.video.VideoDataHumble;
import com.mappingcolor.model.MyColor;

import java.awt.*;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicInteger;

// R stands for resources; holds flags, model settings, and image buffer
public final class R {
    public static final String applicationName = "3D Color Chooser";

    public static boolean selectColorFlag = false;
    public static boolean deselectColorFlag = false;
    public static boolean resetCameraFlag = false;
    public static boolean resetModelFlag = false; // reverts back to the starting color model from the load-image-intersection
    public static boolean displayAxes = true;
    public static boolean clearPlotsFlag = false;
    public static boolean stepSizeChangedFlag = false; // refers to the value, chroma, and hue spacing sliders

    public static AtomicInteger currentFrame = new AtomicInteger();

    public static boolean enabledVBO = false;
    public static boolean supportVBO = false;
    public static boolean updateVBOs = false;
    public static boolean createVBOs = false;
    public static boolean updatePigmentVBOs = false;
    public static boolean updateColorPointVertexColorVBOs = false;
    public static boolean updateWireframeVertexColorVBOs = false;

    public static final int DEFAULT_COLOR_POINT_ALPHA = 15;
    public static final int DEFAULT_WIREFRAME_ALPHA = 15;
    public static final int DEFAULT_SPHERE_RADIUS = 15;
    public static final int DEFAULT_STEP_SIZE = 10;
    public static final int DEFAULT_COUNT_THRESHOLD = 5;
    public static final int DEFAULT_FRAME_SKIP = 30;

    public static final int WINDOW_WIDTH = 800; // formerly 1024
    public static final int WINDOW_HEIGHT = 600; // formerly 768

    public static final int SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() - 1;
    public static final int SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() - 1;

    public static final String[] imageFileFormats = new String[] { "png", "jpeg", "jpg", "gif", "bmp" };
    public static final String[] videoFileFormats = new String[] { "avi", "mov", "m4v", "mp4" };
    public static final String[] dataFileFormats = new String[] { "txt", "csv", "xlsx" };

    public static final String tempDir = System.getProperty("java.io.tmpdir") + "colorModel";

    // make private
    public static boolean dataChanged = false;
    public static boolean dataLoaded = false;
    public static boolean dataUnloaded = false;

    public static ImageData imageData = null;
    //public static VideoDataHumble videoData = null;
    public static VideoData videoData = null;
    public static FileData fileData = null;

    public static MyColor.ModelType pigmentColorModelType = MyColor.ModelType.VCH;

    public static MyColor backgroundColor = MyColor.fromRGB(0.4f, 0.4f, 0.4f);
    public static MyColor selectedColorOne = backgroundColor;
    public static MyColor selectedColorTwo = backgroundColor;

    public static boolean transparencyModeChangedFlag = false;

    public static int countThreshold = DEFAULT_COUNT_THRESHOLD;
    public static int frameSkip = DEFAULT_FRAME_SKIP;

    public static boolean debugData = false;

    public static final int GUI_CHANGE_COMPLETE_TIME = 300;
    public static Timer guiChangeComplete = new Timer();

    public static final float processor_cores = Runtime.getRuntime().availableProcessors();

    public static void clearColorFocusWidgetOne(MyView view)
    {
        view.colorFocusWidget.colorButton.left.setBackground(R.backgroundColor.getColor());
        view.colorFocusWidget.leftColorLabel.setVisible(false);
        view.colorFocusWidget.leftLabelPanel.clearValueLabels();
    }

    public static void clearColorFocusWidgetTwo(MyView view)
    {
        view.colorFocusWidget.colorButton.right.setBackground(R.backgroundColor.getColor());
        view.colorFocusWidget.rightColorLabel.setVisible(false);
        view.colorFocusWidget.rightLabelPanel.clearValueLabels();
    }

    public static void updateColorFocusWidgetOne(MyView view, MyColor selectedColor)
    {
        view.colorFocusWidget.colorButton.left.setBackground(selectedColor.getColor());
        view.colorFocusWidget.leftLabelPanel.updateValues(selectedColor);
        view.colorFocusWidget.leftColorLabel.setVisible(selectedColor.hasName());
        if(selectedColor.hasName())
            view.colorFocusWidget.leftColorLabel.setText(selectedColor.getName());
    }

    public static void updateColorFocusWidgetTwo(MyView view, MyColor selectedColor)
    {
        view.colorFocusWidget.colorButton.right.setBackground(selectedColor.getColor());
        view.colorFocusWidget.rightLabelPanel.updateValues(selectedColor);
        view.colorFocusWidget.rightColorLabel.setVisible(selectedColor.hasName());
        if(selectedColor.hasName())
            view.colorFocusWidget.rightColorLabel.setText(selectedColor.getName());
    }

    public static void repaintImagePanel(MyView view)
    {
        view.imageProcessingPopup.repaint();
    }
}
