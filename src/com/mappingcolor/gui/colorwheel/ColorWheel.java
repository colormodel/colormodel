package com.mappingcolor.gui.colorwheel;

import com.mappingcolor.model.ColorModel;
import com.mappingcolor.model.MyColor;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.Point2D;

public class ColorWheel extends JPanel {

    public static final int VALUE = 65;
    public static final float ALPHA = 0.5f;

    public boolean useMasking = true;

    private Color[][] colors;
    private Color[][] fadedColors;
    private PolarCoords[][] polarMap;
    private int radius;
    private int radiusSquared;
    private Point2D origin;

    private ColorPointer pointer1;
    private ColorPointer pointer2;

    private PolarCoords vertex1Ch;
    private PolarCoords vertex2Ch;
    private Point2D vertex1;
    private Point2D vertex2;

    protected JSpinner p1ChromaSpinner;
    protected JSpinner p2ChromaSpinner;

    protected JSpinner p1HueSpinner;
    protected JSpinner p2HueSpinner;

    public JSpinner maxValueSpinner;
    public JSpinner minValueSpinner;

    protected JPanel controlPanel = new JPanel();

    public ColorWheel() {
        setDoubleBuffered(true);
        setOpaque(false);
        setPreferredSize(new Dimension(201, 201));
        setMinimumSize(new Dimension(201, 201));
        setSize(201, 201);

        updateRadius();

        pointer1 = new ColorPointer(this, ColorPointer.POINTER_1);
        pointer2 = new ColorPointer(this, ColorPointer.POINTER_2);
        pointer1.setToolTipText("Pointer 1");
        pointer2.setToolTipText("Pointer 2");

        pointer1.setLocation((int) (getRadius() / 3.0), (int) (getRadius() / 3.0));
        pointer2.setLocation((int) (getRadius() * (6.0 / 4.0)), (int) (getRadius() * (3.0 / 4.0)));

        setLayout(null);
        add(pointer1);
        add(pointer2);

        pointer1.updateChromaHue();
        pointer2.updateChromaHue();
        updatePoints(false);

        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    doSpinnerAndLabelStuff();

                    addComponentListener(new ComponentAdapter() {
                        public void componentResized(ComponentEvent e) {
                            ColorWheel.this.updateRadius();
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        Point2D ycoords = fromPolar(chromaToR(120), 90);
        Point2D xcoords = fromPolar(chromaToR(120), 180);

        Point2D ypix = fromCoords(ycoords);
        Point2D xpix = fromCoords(xcoords);
    }

    public void doSpinnerAndLabelStuff() {

        JLabel max = new JLabel("Max:", JLabel.RIGHT);
        max.setPreferredSize(new Dimension(27, 20));
        JLabel valueLabel = new JLabel("V:");
        maxValueSpinner = new JSpinner(new SpinnerNumberModel(MyColor.MAX_VALUE, 0, MyColor.MAX_VALUE, 1));
        JLabel chromaLabel = new JLabel("C:");
        p1ChromaSpinner = new JSpinner(new SpinnerNumberModel((int) Math.round(pointer1.getChromaHue().r), 0, MyColor.MAX_CHROMA, 1));
        JLabel hueLabel = new JLabel("h:");
        p1HueSpinner = new JSpinner(new SpinnerNumberModel((int) Math.round(pointer1.getChromaHue().theta), -10, MyColor.MAX_HUE + 10, 1));

        JPanel spinnerPanel = new JPanel(new GridLayout(2,3));
        spinnerPanel.add(addLabel(valueLabel, maxValueSpinner));
        spinnerPanel.add(addLabel(chromaLabel, p1ChromaSpinner));
        spinnerPanel.add(addLabel(hueLabel, p1HueSpinner));

        JLabel min = new JLabel("Min:", JLabel.RIGHT);
        min.setPreferredSize(new Dimension(23, 20));
        valueLabel = new JLabel("V:");
        minValueSpinner = new JSpinner(new SpinnerNumberModel(0, 0, MyColor.MAX_VALUE, 1));
        chromaLabel = new JLabel("C:");
        p2ChromaSpinner = new JSpinner(new SpinnerNumberModel((int) Math.round(pointer2.getChromaHue().r), 0, MyColor.MAX_CHROMA, 1));
        hueLabel = new JLabel("h:");
        p2HueSpinner = new JSpinner(new SpinnerNumberModel((int) Math.round(pointer2.getChromaHue().theta), -10, MyColor.MAX_HUE + 10, 1));

        spinnerPanel.add(addLabel(valueLabel, minValueSpinner));
        spinnerPanel.add(addLabel(chromaLabel, p2ChromaSpinner));
        spinnerPanel.add(addLabel(hueLabel, p2HueSpinner));

        JPanel labelPanel = new JPanel(new GridLayout(2,1));
        labelPanel.add(max);
        labelPanel.add(min);

        controlPanel = new JPanel(new BorderLayout());
        //controlPanel.setLayout(new GridLayout(2,1));
        controlPanel.add(labelPanel, BorderLayout.WEST);
        controlPanel.add(spinnerPanel, BorderLayout.CENTER);

        p1ChromaSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (!e.getSource().equals(p1ChromaSpinner)) return;

                int chroma = checkChromaLimit(p1ChromaSpinner);
                pointer1.setChroma(chroma);
                pointer1.updatePosition();
            }
        });

        p2ChromaSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (!e.getSource().equals(p2ChromaSpinner)) return;

                int chroma = checkChromaLimit(p2ChromaSpinner);
                pointer2.setChroma(chroma);
                pointer2.updatePosition();
            }
        });

        p1HueSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (!e.getSource().equals(p1HueSpinner)) return;

                int hue = checkHueLimits(p1HueSpinner);
                pointer1.setHue(hue);
                pointer1.updatePosition();
            }
        });

        p2HueSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (!e.getSource().equals(p2HueSpinner)) return;

                int hue = checkHueLimits(p2HueSpinner);
                pointer2.setHue(hue);
                pointer2.updatePosition();
            }
        });
    }

    private JComponent addLabel(JLabel label, JComponent c)
    {
        JPanel panel = new JPanel();
        panel.setBorder(null);
        panel.add(label);
        panel.add(c);
        return panel;
    }

    private int checkChromaLimit(JSpinner spinner) {
        int chroma = (Integer) spinner.getValue();
        if (chroma > 175)
            spinner.getModel().setValue(175);
        return Math.min(chroma, 175);
    }

    private int checkHueLimits(JSpinner spinner) {
        int spinnerHue = (Integer) spinner.getValue();
        int hue = spinnerHue;

        if (hue > 360)
            while (hue > 360)
                hue -= 360;

        if (hue < 0)
            while (hue < 0)
                hue += 360;

        if (spinnerHue != hue)
            spinner.getModel().setValue(hue);

        return hue;
    }

    public double rToChroma(double r) {
        return r / this.radius * MyColor.MAX_CHROMA;
    }

    public double chromaToR(double chroma) {
        return chroma / MyColor.MAX_CHROMA * this.radius;
    }

    private boolean selectionContains(int x, int y) {
        PolarCoords ch = this.polarMap[x][y];

        double maxChroma = Math.round(Math.max(pointer1.getRHue().r, pointer2.getRHue().r));
        double minChroma = Math.round(Math.min(pointer1.getRHue().r, pointer2.getRHue().r));
        if (ch.r > maxChroma || ch.r < minChroma) return false;

        double p1theta = Math.round(pointer1.getChromaHue().theta);
        double p2theta = Math.round(pointer2.getChromaHue().theta);
        return p2theta > p1theta ? (ch.theta >= p2theta || ch.theta <= p1theta) : (ch.theta <= p1theta && ch.theta >= p2theta);
    }

    @Override
    public void paint(Graphics g) {
        if (!(g instanceof Graphics2D)) System.out.println("WTF Y U NO GRAPHICS2D!? (ColorWheel)");
        Graphics2D g2d = (Graphics2D) g;

        drawColors(g);

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // AntiAliasing ON

        g.drawOval(0, 0, this.radius * 2, this.radius * 2); // Draw wheel border

        g.drawOval(31, 31, (this.radius - 31) * 2, (this.radius - 31) * 2); // Draw RGB Limits

        if (!useMasking) drawSelectionBorder(g);

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF); // AntiAliasing OFF

        //drawVertices(g);

        if (!this.isEnabled())
        {
            g.setColor(new Color(.5f, .5f, .5f, .7f));
            g.fillOval(0, 0, this.radius * 2, this.radius * 2);
        }

        super.paint(g);
    }

    private void drawColors(Graphics g)
    {
        Color oldColor = g.getColor();
        Color color = null;
        for (int x = 0; x < this.radius * 2 + 1; x++)
        {
            for (int y = 0; y < this.radius * 2 + 1; y++)
            {
                if (this.colors[x][y] != null)
                {
                    color = null;
                    if (useMasking)
                        color = (selectionContains(x, y) ? this.colors : this.fadedColors)[x][y];
                    else
                        color = this.colors[x][y];

                    if (color != null)
                    {
                        g.setColor(color);
                        g.drawLine(x, y, x, y);
                    }
                }
            }
        }
        g.setColor(oldColor);
    }

    private void drawVertices(Graphics g)
    {
        g.fillRect((int) vertex1.getX() - 1, (int) vertex1.getY() - 1, 3, 3);
        g.fillRect((int) vertex2.getX() - 1, (int) vertex2.getY() - 1, 3, 3);
    }

    private void drawSelectionBorder(Graphics g)
    {
        g.drawLine(pointer1.getCenterX(), pointer1.getCenterY(), (int) vertex1.getX(), (int) vertex1.getY());
        g.drawLine(pointer2.getCenterX(), pointer2.getCenterY(), (int) vertex2.getX(), (int) vertex2.getY());

        // from pointer 1 to vertex 2
        double r = pointer1.getRHue().r;
        Point2D lastPoint = new Point2D.Double(pointer1.getCenterX(), pointer1.getCenterY());
        double maxTheta = pointer1.getChromaHue().theta;
        double minTheta = vertex2Ch.theta;
        if (minTheta > maxTheta) minTheta -= 360;

        for (double i = maxTheta - 5; i >= minTheta; i -= 5)
        {
            Point2D point = fromCoords(fromPolar(r, i));
            g.drawLine((int)lastPoint.getX(), (int)lastPoint.getY(),(int)point.getX(), (int)point.getY());
            lastPoint = point;
        }
        g.drawLine((int)lastPoint.getX(), (int)lastPoint.getY(),(int)vertex2.getX(), (int)vertex2.getY());

        // from vertex 1 to pointer 2
        r = pointer2.getRHue().r;
        lastPoint = vertex1;
        maxTheta = vertex1Ch.theta;
        minTheta = pointer2.getChromaHue().theta;
        if (minTheta > maxTheta) minTheta -= 360;

        for (double i = maxTheta - 5; i >= minTheta; i -= 5)
        {
            Point2D point = fromCoords(fromPolar(r, i));
            g.drawLine((int)lastPoint.getX(), (int)lastPoint.getY(),(int)point.getX(), (int)point.getY());
            lastPoint = point;
        }
        g.drawLine((int)lastPoint.getX(), (int)lastPoint.getY(), pointer2.getCenterX(), pointer2.getCenterY());

    }

    public void updatePoints(boolean dragged)
    {
        PolarCoords pointer1Ch = pointer1.getChromaHue();
        PolarCoords pointer2Ch = pointer2.getChromaHue();

        vertex1Ch = new PolarCoords(pointer2.getRHue().r, pointer1Ch.theta);
        Point2D v1coords = fromPolar(vertex1Ch.r, vertex1Ch.theta);
        vertex1 = fromCoords(v1coords);

        vertex2Ch = new PolarCoords(pointer1.getRHue().r, pointer2Ch.theta);
        Point2D v2coords = fromPolar(vertex2Ch.r, vertex2Ch.theta);
        vertex2 = fromCoords(v2coords);

        if (dragged)
        {
            int p1Chroma = Math.min((int) Math.round(pointer1Ch.r), 175);
            if (p1ChromaSpinner != null && p1Chroma != ((Integer)p1ChromaSpinner.getValue()))
                p1ChromaSpinner.getModel().setValue(p1Chroma);

            int p2Chroma = Math.min((int) Math.round(pointer2Ch.r), 175);
            if (p2ChromaSpinner != null && p2Chroma != ((Integer)p2ChromaSpinner.getValue()))
                p2ChromaSpinner.getModel().setValue(p2Chroma);

            int p1Hue = (int) Math.round(pointer1Ch.theta);
            if (p1HueSpinner != null && p1Hue != ((Integer)p1HueSpinner.getValue()))
                p1HueSpinner.getModel().setValue(p1Hue);
            int p2Hue = (int) Math.round(pointer2Ch.theta);
            if (p2HueSpinner != null && p2Hue != ((Integer)p2HueSpinner.getValue()))
                p2HueSpinner.getModel().setValue(p2Hue);
        }

        ColorModel.chromaLowerBound = getMinChroma();
        ColorModel.chromaUpperBound = getMaxChroma();
        ColorModel.hueLowerBound = getMinHue();
        ColorModel.hueUpperBound = getMaxHue();
    }

    public int getRadius()
    {
        return this.radius;
    }

    public int getRadiusSquared()
    {
        return this.radiusSquared;
    }

    public void updateRadius()
    {
        this.radius = findRadius();
        this.radiusSquared = this.radius * this.radius;
        this.origin = new Point2D.Float(this.radius, this.radius);
        updateWheelColors();
    }

    public void updateWheelColors()
    {
        int size = this.radius * 2 + 1;
        this.colors = new Color[size][size];
        this.fadedColors = new Color[size][size];
        this.polarMap = new PolarCoords[size][size];
        //g.fillOval(0, 0, this.radius * 2, this.radius * 2);
        Color color = null;
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                if (Point2D.distanceSq(x, y, this.radius, this.radius) < this.radiusSquared)
                {
                    this.polarMap[x][y] = rectToPolar(toCoords(x, y, this.radius, this.radius));
                    this.polarMap[x][y].r = Math.round(this.polarMap[x][y].r);
                    this.polarMap[x][y].theta = Math.round(this.polarMap[x][y].theta);
                    this.colors[x][y] = color = getPixelColor(x, y);
                    this.fadedColors[x][y] = new Color(color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f, ALPHA);
                }
                else
                {
                    this.colors[x][y] = null;
                    this.fadedColors[x][y] = null;
                }
            }
        }
    }

    private int findRadius()
    {
        return Math.min(this.getWidth(), this.getHeight()) / 2;
    }

    public Color getPixelColor(int pX, int pY)
    {
       Point2D coords = toCoords(pX, pY, this.radius, this.radius);
       PolarCoords polar = rectToPolar(coords);
       //polar.theta -= 90;
       //if (coords.getX() < 0) polar.theta += 180;
//       else if (coords.getX() < 0 && coords.getY() < 0) polar.theta += 180;
//       else if (coords.getX() >= 0 && coords.getY() < 0) polar.theta += 270;
       return MyColor.fromVCH(VALUE, (float) rToChroma(polar.r), (float) polar.theta).getColor();
    }

    public static Point2D toCoords(double pX, double pY, double cX, double cY)
    {
        double x = pX - cX;
        double y = cY - pY;
        return new Point2D.Double(x, y);
    }

    public Point2D fromCoords(Point2D coords)
    {
        return fromCoords(coords, this.radius);
    }

    public static Point2D fromCoords(Point2D coords, int radius)
    {
        int[] pixels = new int[2];
        pixels[0] = (int) Math.round(coords.getX() + radius);
        pixels[1] = (int) Math.round(radius - coords.getY());
        return new Point2D.Double(pixels[0], pixels[1]);
    }

    public static Point2D fromPolar(double r, double theta)
    {
        double x = r * Math.cos(Math.toRadians(theta));
        double y = r * Math.sin(Math.toRadians(theta));
        return new Point2D.Double(x, y);
    }

    public static PolarCoords rectToPolar(Point2D coords)
    {
        return rectToPolar(coords.getX(), coords.getY());
    }

    public static PolarCoords rectToPolar(double x, double y)
    {
        double r = Math.sqrt(x * x + y * y);
        double theta = Math.toDegrees(Math.atan(y / x));
        if (x == 0 && y == 0) theta = 0;

        // http://hyperphysics.phy-astr.gsu.edu/hbase/ttrig.html#c3
        if (x < 0) theta += 180;
        if (x >= 0 && y < 0) theta += 360;

        return new PolarCoords(r, theta);
    }

    public static class PolarCoords
    {
        public double r;
        public double theta;

        public PolarCoords(double r, double theta)
        {
            this.r = r;
            this.theta = theta;
        }
    }

    public int getMinChroma()
    {
        return (int) Math.round(Math.min(this.pointer1.getChromaHue().r, this.pointer2.getChromaHue().r));
    }
    public int getMaxChroma()
    {
        return (int) Math.round(Math.max(this.pointer1.getChromaHue().r, this.pointer2.getChromaHue().r));
    }

    public int getMinHue()
    {
        return (int) Math.round(this.pointer2.getChromaHue().theta);
    }

    public int getMaxHue()
    {
        return (int) Math.round(this.pointer1.getChromaHue().theta);
    }
}
