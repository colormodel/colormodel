package com.mappingcolor.gui.colorwheel;

import java.awt.*;
import java.awt.geom.Point2D;

public class ColorPointer extends DraggableComponent
{
    public static int CROSS = 0;
    public static int POINTER_1 = 1;
    public static int POINTER_2 = 2;

    private static int RADIUS = 5;

    private ColorWheel parentWheel;
    private ColorWheel.PolarCoords chromaHue;
    private ColorWheel.PolarCoords rHue;

    public boolean chromaLocked = false;
    public boolean hueLocked = false;

    private int pointerStyle;

    public ColorPointer(ColorWheel wheel)
    {
        this(wheel, CROSS);
    }

    public ColorPointer(ColorWheel wheel, int pointerStyle)
    {
        super();
        this.parentWheel = wheel;
        this.pointerStyle = pointerStyle;

        setBounds(wheel.getRadius() - RADIUS, wheel.getRadius() - RADIUS, RADIUS * 2, RADIUS * 2);
        updateChromaHue();
    }

    public int getCenterX()
    {
        return this.getX() + RADIUS;
    }

    public int getCenterY()
    {
        return this.getY() + RADIUS;
    }

    @Override
    public void paint(Graphics g)
    {
        if (pointerStyle == CROSS) drawCross(g);
        else if (pointerStyle == POINTER_1) drawPointer1(g);
        else if (pointerStyle == POINTER_2) drawPointer2(g);
        super.paint(g);
    }

    private void drawCross(Graphics g)
    {
        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        g.drawLine(0, cY, RADIUS * 2, cY);
        g.drawLine(cX, 0, cX, RADIUS * 2);
    }

    private void drawPointer2(Graphics g)
    {
        if (!(g instanceof Graphics2D)) System.out.println("WTF Y U NO GRAPHICS2D!? (ColorPointer)");
        Graphics2D g2d = (Graphics2D) g;

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // AntiAliasing ON

        g.setColor(Color.WHITE);
        g.fillOval(1, 1, getWidth() - 2, getHeight() - 2);

        g.setColor(Color.BLACK);
        g.drawOval(1, 1, getWidth() - 2, getHeight() - 2);
        g.fillOval(3, 3, getWidth() - 5, getHeight() - 5);

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF); // AntiAliasing OFF

    }

    private void drawPointer1(Graphics g)
    {

        if (!(g instanceof Graphics2D)) System.out.println("WTF Y U NO GRAPHICS2D!? (ColorPointer)");
        Graphics2D g2d = (Graphics2D) g;

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // AntiAliasing ON

        g.setColor(Color.WHITE);
        g.fillOval(1, 1, getWidth() - 2, getHeight() - 2);
        //g.setColor(Color.WHITE);
        g.setColor(Color.BLACK);
        g.drawOval(1, 1, getWidth() - 2, getHeight() - 2);
        g.drawOval(3, 3, getWidth() - 6, getHeight() - 6);

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF); // AntiAliasing OFF
    }

    @Override
    public boolean canMove(int x, int y) {
        return isEnabled() && Point.distanceSq(x + 5, y + 5, parentWheel.getRadius(), parentWheel.getRadius()) <= parentWheel.getRadiusSquared();
    }

    public ColorWheel.PolarCoords getChromaHue()
    {
        return this.chromaHue;
    }
    public ColorWheel.PolarCoords getRHue() { return this.rHue; }

    public void setChroma(double chroma)
    {
        this.chromaHue.r = chroma;
        this.rHue.r = parentWheel.chromaToR(chroma);
    }

    public void setHue(double hue)
    {
        this.chromaHue.theta = hue;
        this.rHue.theta = hue;
    }

    public void updatePosition()
    {
        int radius = parentWheel.getRadius();
        Point2D point = ColorWheel.fromCoords(ColorWheel.fromPolar(this.rHue.r, this.chromaHue.theta), radius);
        setLocation((int) Math.round(point.getX() - 5), (int) Math.round(point.getY() - 5));
        componentMoved(false);
        updateToolTip();
    }

    public void updateChromaHue()
    {
        int radius = parentWheel.getRadius();
        ColorWheel.PolarCoords polar = ColorWheel.rectToPolar(ColorWheel.toCoords(getCenterX(), getCenterY(), radius, radius));
        this.chromaHue = new ColorWheel.PolarCoords(parentWheel.rToChroma(polar.r), polar.theta);
        this.rHue = new ColorWheel.PolarCoords(polar.r, polar.theta);
        updateToolTip();
    }

    public void updateToolTip()
    {
        setToolTipText(String.format("(r=%s, θ=%s)", Math.round(this.chromaHue.r), Math.round(this.chromaHue.theta)));
    }

    @Override
    protected void componentMoved(boolean dragged)
    {
        if (dragged) updateChromaHue();
        parentWheel.updatePoints(dragged);
        parentWheel.repaint();
    }

    @Override
    public boolean isEnabled()
    {
        return super.isEnabled() && parentWheel.isEnabled();
    }
}
