package com.mappingcolor.gui.colorwheel;

import com.mappingcolor.Main;
import com.mappingcolor.gui.SwingUtils;
import com.mappingcolor.gui.side.RangeSlider;
import com.mappingcolor.model.ColorModel;
import com.mappingcolor.model.MyColor;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ColorWheelPanel extends JPanel
{
    public ColorWheel colorWheel;
    public JCheckBox enabledBox;
    public RangeSlider rangeSliderValue;

    public ColorWheelPanel()
    {
        this.setBorder(new TitledBorder("VCh Range Selection"));
        this.colorWheel = new ColorWheel();

        this.rangeSliderValue = new RangeSlider();
        this.rangeSliderValue.setValue(0);
        this.rangeSliderValue.setUpperValue(100);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel cwPanel = new JPanel();
        cwPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        cwPanel.add(rangeSliderValue);
        cwPanel.add(colorWheel);

        this.enabledBox = new JCheckBox("Enabled", true);
        this.enabledBox.setAlignmentX(JCheckBox.CENTER_ALIGNMENT);
        this.enabledBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean enabled = enabledBox.isSelected();
                colorWheel.setEnabled(enabled);
                //Main.view.slidePanel.rangeSliderValue.setEnabled(enabled);
                colorWheel.minValueSpinner.setEnabled(enabled);
                colorWheel.maxValueSpinner.setEnabled(enabled);
                colorWheel.p1ChromaSpinner.setEnabled(enabled);
                colorWheel.p1HueSpinner.setEnabled(enabled);
                colorWheel.p2ChromaSpinner.setEnabled(enabled);
                colorWheel.p2HueSpinner.setEnabled(enabled);
                rangeSliderValue.setEnabled(enabled);

                if (enabled)
                {
                    ColorModel.valueLowerBound = Main.view.slidePanel.rangeSliderValue.getValue();
                    ColorModel.valueUpperBound = Main.view.slidePanel.rangeSliderValue.getUpperValue();
                    ColorModel.chromaLowerBound = colorWheel.getMinChroma();
                    ColorModel.chromaUpperBound = colorWheel.getMaxChroma();
                    ColorModel.hueLowerBound = colorWheel.getMinHue();
                    ColorModel.hueUpperBound = colorWheel.getMaxHue();
                }
                else
                {
                    ColorModel.valueLowerBound = 0;
                    ColorModel.valueUpperBound = MyColor.MAX_VALUE;
                    ColorModel.chromaLowerBound = 0;
                    ColorModel.chromaUpperBound = MyColor.MAX_CHROMA;
                    ColorModel.hueLowerBound = 0;
                    ColorModel.hueUpperBound = MyColor.MAX_HUE;
                }
            }
        });


        rangeSliderValue.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ColorModel.valueLowerBound = rangeSliderValue.getValue();
                ColorModel.valueUpperBound = rangeSliderValue.getUpperValue();
                if (((Integer)colorWheel.minValueSpinner.getValue()).intValue() != rangeSliderValue.getValue())
                    colorWheel.minValueSpinner.setValue(rangeSliderValue.getValue());
                if (((Integer)colorWheel.maxValueSpinner.getValue()).intValue() != rangeSliderValue.getUpperValue())
                    colorWheel.maxValueSpinner.setValue(rangeSliderValue.getUpperValue());
                updateUI();
            }

        });


        colorWheel.maxValueSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (!e.getSource().equals(colorWheel.maxValueSpinner)) return;

                int value = ((Integer)colorWheel.maxValueSpinner.getValue()).intValue();
                rangeSliderValue.setUpperValue(value);
                updateUI();
            }
        });
        colorWheel.minValueSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (!e.getSource().equals(colorWheel.minValueSpinner)) return;

                int value = ((Integer)colorWheel.minValueSpinner.getValue()).intValue();
                rangeSliderValue.setValue(value);
                updateUI();
            }
        });


        panel.add(enabledBox);
        panel.add(cwPanel);
        panel.add(SwingUtils.nest(colorWheel.controlPanel));
        add(panel);
        this.enabledBox.doClick();
    }
}
