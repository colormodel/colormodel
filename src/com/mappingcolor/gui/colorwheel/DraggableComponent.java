package com.mappingcolor.gui.colorwheel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public abstract class DraggableComponent extends JComponent
{
    private volatile int screenX = 0;
    private volatile int screenY = 0;
    private volatile int myX = 0;
    private volatile int myY = 0;

    public DraggableComponent() {
        setOpaque(false);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                screenX = e.getXOnScreen();
                screenY = e.getYOnScreen();

                myX = getX();
                myY = getY();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

        });
        addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent e) {
                int deltaX = e.getXOnScreen() - screenX;
                int deltaY = e.getYOnScreen() - screenY;

                if (canMove(myX + deltaX, myY + deltaY)) {
                    setLocation(myX + deltaX, myY + deltaY);
                    componentMoved(true);
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }

        });
    }

    public abstract boolean canMove(int x, int y);

    protected void componentMoved(boolean dragged)
    {

    }
}