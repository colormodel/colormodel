package com.mappingcolor.gui;

public class OsChecker {

    private OsChecker() {}

    public static String getOsName() {
        return System.getProperty("os.name", "unknown");
    }

    public static String platform() {
        String osname = System.getProperty("os.name", "generic").toLowerCase();
        if (osname.startsWith("windows")) {
            return "win32";
        } else if (osname.startsWith("linux")) {
            return "linux";
        } else if (osname.startsWith("sunos")) {
            return "solaris";
        } else if (osname.startsWith("mac") || osname.startsWith("darwin")) {
            return "mac";
        } else return "generic";
    }

    public static boolean isWindows() {
        return (getOsName().toLowerCase().contains("windows"));
    }

    public static boolean isLinux() {
        return getOsName().toLowerCase().contains("linux");
    }

    public static boolean isUnix() {
        final String os = getOsName().toLowerCase();

        // XXX: this obviously needs some more work to be "true" in general (see bottom of file)
        return (os.contains("sunos")) || (os.contains("linux")) || isMac() && (System.getProperty("os.version", "").startsWith("10."));

    }

    public static boolean isMac() {
        final String os = getOsName().toLowerCase();
        return os.startsWith("mac") || os.startsWith("darwin");
    }

    public static boolean isSolaris() {
        final String os = getOsName().toLowerCase();
        return os.contains("sunos");
    }
}