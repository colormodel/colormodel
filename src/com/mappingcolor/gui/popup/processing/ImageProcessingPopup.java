package com.mappingcolor.gui.popup.processing;

import com.mappingcolor.R;
import com.mappingcolor.io.image.ImageData;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageProcessingPopup extends ProcessingPopup
{
    public static final String PROCESSING_POPUP_TYPE = "Image";
    public JPanel imagePanel;

    public ImageProcessingPopup()
    {
        super(PROCESSING_POPUP_TYPE);
    }

    public void initialize() {
        super.initialize(buildLoadedDataPanel());
    }

    public JPanel buildLoadedDataPanel() {
        imagePanel = new JPanel() {
            @Override
            public void paint(Graphics g)
            {
                if (R.imageData != null) drawImage(this, R.imageData.getFrame(), g);
            }
        };
        imagePanel.setDoubleBuffered(true);
        imagePanel.setMinimumSize(new Dimension(235, 235));
        imagePanel.setPreferredSize(new Dimension(235, 235));

        JPanel loadedImagePanel = new JPanel(new BorderLayout());
        loadedImagePanel.setBorder(BorderFactory.createTitledBorder(loadedImagePanel.getBorder(), "Loaded Image"));
        loadedImagePanel.add(imagePanel, BorderLayout.CENTER);

        return loadedImagePanel;
    }

    public boolean loadHelper(File file) {
        R.videoData = null;
        R.fileData = null;
        R.imageData = new ImageData(file);

        return true;
    }

    @Override
    protected int getCountThreshold() {
        return R.DEFAULT_COUNT_THRESHOLD;
    }

    @Override
    protected FileNameExtensionFilter getLoadFileFilter()
    {
        return new FileNameExtensionFilter("Images", R.imageFileFormats);
    }
}
