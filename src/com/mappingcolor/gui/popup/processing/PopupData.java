package com.mappingcolor.gui.popup.processing;

import javax.swing.*;
import java.io.File;

public interface PopupData
{
    void initialize(JPanel dataPanel);

    JPanel buildLoadedDataPanel();

    boolean loadHelper(File file);
}
