package com.mappingcolor.gui.popup.processing;

import com.mappingcolor.R;
import com.mappingcolor.io.file.FileData;
import com.mappingcolor.model.MyColor;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class DataProcessingPopup extends ProcessingPopup
{
    public static final String PROCESSING_POPUP_TYPE = "Data";

    public JRadioButton labButton;
    public JRadioButton vchButton;

    public DataProcessingPopup()
    {
        super(PROCESSING_POPUP_TYPE);
    }

    public void initialize() {
        super.initialize(buildLoadedDataPanel());
    }

    @Override
    protected JPanel buildFilterPanel() {
        vchButton = new JRadioButton("VCh", true);
        labButton = new JRadioButton("LAB");

        labButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (labButton.isSelected())
                {
                    R.pigmentColorModelType = MyColor.ModelType.LAB;
                }
            }
        });

        vchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (vchButton.isSelected())
                {
                    R.pigmentColorModelType = MyColor.ModelType.VCH;
                }
            }
        });

        ButtonGroup group = new ButtonGroup();
        group.add(vchButton);
        group.add(labButton);

        JPanel inputTypePanel = new JPanel();
        inputTypePanel.setBorder(BorderFactory.createTitledBorder(inputTypePanel.getBorder(), "Input Type"));
        inputTypePanel.add(vchButton);
        inputTypePanel.add(labButton);

        return inputTypePanel;
    }

    @Override
    protected int getCountThreshold() {
        return 0;
    }

    @Override
    protected FileNameExtensionFilter getLoadFileFilter()
    {
        return new FileNameExtensionFilter("Data", R.dataFileFormats);
    }

    public JPanel buildLoadedDataPanel() {
        return new JPanel();
    }

    public boolean loadHelper(File file) {
        if (file != null) {
            R.fileData = new FileData(file);
            R.imageData = null;
            R.videoData = null;
            R.dataChanged = true;
            return true;
        }
        return false;
    }
}
