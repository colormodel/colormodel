package com.mappingcolor.gui.popup.processing;

import com.mappingcolor.R;
import com.mappingcolor.io.ColorMapData;
import com.mappingcolor.io.file.ExportData;
import com.mappingcolor.model.ColorModel;
import com.mappingcolor.model.MyColor;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class OutputPopup extends JDialog
{
    public JRadioButton vchButton;
    public JRadioButton labButton;
    public JRadioButton rgbButton;

    public JRadioButton excelButton;
    public JRadioButton textButton;
    public JRadioButton csvButton;

    public JRadioButton allData;
    public JRadioButton visibleData;

    public JButton okayButton;
    public JButton cancelButton;

    public JFileChooser saveLocationChooser = null;

    public JSpinner beginFrame;
    public JSpinner endFrame;
    private boolean isVideo;


    public OutputPopup(Window parent, boolean isVideo)
    {
        super(parent);
        setTitle("Output");
        setModal(true);

        this.isVideo = isVideo;

        add(buildMainPanel());
        pack();
        setMinimumSize(getSize());

        setLocationRelativeTo(null);
        setVisible(true);
    }

    public JPanel buildMainPanel()
    {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
        panel.add(buildColorPanel());
        panel.add(Box.createVerticalStrut(10));
        panel.add(buildFilePanel());
        panel.add(Box.createVerticalStrut(10));
        panel.add(buildDataSelectionPanel());
        panel.add(Box.createVerticalStrut(10));
        if(isVideo) {
            panel.add(buildFrameSelectionPanel());
            panel.add(Box.createVerticalStrut(10));
        }
        panel.add(buildButtonPanel());

        return panel;
    }

    private JPanel buildColorPanel()
    {
        vchButton = new JRadioButton("VCh", true);
        labButton = new JRadioButton("LAB", false);
        rgbButton = new JRadioButton("RGB", false);

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(vchButton);
        buttonGroup.add(labButton);
        buttonGroup.add(rgbButton);

        JPanel colorPanel = new JPanel();
        colorPanel.setBorder(BorderFactory.createTitledBorder(colorPanel.getBorder(), "Color Output Type"));
        colorPanel.add(vchButton);
        colorPanel.add(labButton);
        colorPanel.add(rgbButton);
        return colorPanel;
    }

    private JPanel buildFilePanel()
    {
        excelButton = new JRadioButton("Excel", true);
        textButton = new JRadioButton("Text", false);
        csvButton = new JRadioButton("CSV", false);

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(excelButton);
        buttonGroup.add(textButton);
        buttonGroup.add(csvButton);

        JPanel filePanel = new JPanel();
        filePanel.setBorder(BorderFactory.createTitledBorder(filePanel.getBorder(), "File Output Type"));
        filePanel.add(excelButton);
        filePanel.add(textButton);
        filePanel.add(csvButton);
        return filePanel;
    }

    private JPanel buildDataSelectionPanel() {
        allData = new JRadioButton("All", true);
        visibleData = new JRadioButton("Visible", false);

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(allData);
        buttonGroup.add(visibleData);

        JPanel dataPanel = new JPanel();
        dataPanel.setBorder(BorderFactory.createTitledBorder(dataPanel.getBorder(), "Data Selection"));
        dataPanel.add(allData);
        dataPanel.add(visibleData);
        return dataPanel;
    }

    private JPanel buildFrameSelectionPanel() {
        int totalFrames = R.videoData.getVideoStreamData().getTotalFrames();
        beginFrame = new JSpinner(new SpinnerNumberModel(1, 1, totalFrames, 1));
        endFrame = new JSpinner(new SpinnerNumberModel(totalFrames, 1, totalFrames, 1));

        beginFrame.setPreferredSize(new Dimension(43, 20));
        endFrame.setPreferredSize(new Dimension(43, 20));

        JPanel row = new JPanel(new GridLayout(0, 2, 0, 0));
        JPanel p = new JPanel();
        p.add(new JLabel("Begin:"));
        p.add(beginFrame);
        row.add(p);

        p = new JPanel();
        p.add(new JLabel("End:"));
        p.add(endFrame);
        row.add(p);

        JPanel framePanel = new JPanel(new BorderLayout());
        framePanel.setBorder(BorderFactory.createTitledBorder(framePanel.getBorder(), "Frame Selection"));

        framePanel.add(row);

        return framePanel;
    }

    private JPanel buildButtonPanel()
    {
        okayButton = new JButton("Okay");
        okayButton.setAlignmentX(JButton.CENTER_ALIGNMENT);
        cancelButton = new JButton("Cancel");
        cancelButton.setAlignmentX(JButton.CENTER_ALIGNMENT);

        okayButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (saveLocationChooser == null) {
                    LookAndFeel laf = UIManager.getLookAndFeel();
                    String lafClassName = UIManager.getSystemLookAndFeelClassName();
                    try {
                        UIManager.setLookAndFeel(lafClassName);
                        saveLocationChooser = new JFileChooser();
                        UIManager.setLookAndFeel(laf);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                if (saveLocationChooser == null) saveLocationChooser = new JFileChooser();
                saveLocationChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Data Files", R.dataFileFormats);
                saveLocationChooser.setFileFilter(filter);
                int returnVal = saveLocationChooser.showSaveDialog(OutputPopup.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    String fileName = saveLocationChooser.getSelectedFile().getPath();

                    ColorMapData data = getColorMapData();

                    String extension;
                    int begin = 1;
                    int end = 1;
                    if(isVideo) {
                        begin = (Integer) beginFrame.getValue();
                        end = (Integer) endFrame.getValue();
                    }

                    if(excelButton.isSelected()) {
                        extension = fileName.contains(ExportData.EXCEL_EXTENSION) ? "" : ExportData.EXCEL_EXTENSION;
                        ExportData.writeToFile(fileName + extension, getColorOutputType(), R.countThreshold, data, visibleData.isSelected(), begin, end);
                    }
                    else if (csvButton.isSelected()) {
                        extension = fileName.contains(ExportData.CSV_EXTENSION) ? "" : ExportData.CSV_EXTENSION;
                        ExportData.writeToFile(fileName + extension, getColorOutputType(), R.countThreshold, data, visibleData.isSelected(), begin, end);
                    }
                    else {
                        extension = fileName.contains(ExportData.TEXT_EXTENSION) ? "" : ExportData.TEXT_EXTENSION;
                        ExportData.writeToFile(fileName + extension, getColorOutputType(), R.countThreshold, data, visibleData.isSelected(), begin, end);
                    }
                }
                OutputPopup.this.dispose();
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OutputPopup.this.dispose();
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.setAlignmentX(JPanel.CENTER_ALIGNMENT);
        buttonPanel.add(okayButton);
        buttonPanel.add(cancelButton);
        return buttonPanel;
    }

    public MyColor.ModelType getColorOutputType()
    {
        if (vchButton.isSelected()) return MyColor.ModelType.VCH;
        if (labButton.isSelected()) return MyColor.ModelType.LAB;
        else return MyColor.ModelType.RGB;
    }

    private ColorMapData getColorMapData()
    {
        ColorMapData colorMapData;

        if(R.imageData != null)
            colorMapData = R.imageData;
        else if(R.videoData != null)
            colorMapData = R.videoData;
        else
            colorMapData = R.fileData;

        return colorMapData;
    }
}
