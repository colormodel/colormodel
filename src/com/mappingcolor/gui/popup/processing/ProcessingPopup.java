package com.mappingcolor.gui.popup.processing;

import com.mappingcolor.Main;
import com.mappingcolor.R;
import com.mappingcolor.gui.WindowActivationListener;
import com.mappingcolor.model.ColorModel;
import com.mappingcolor.gui.menu.MenuBar;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.Field;

public abstract class ProcessingPopup extends JDialog implements PopupData
{

    public JRadioButton nearestNeighborButton;
    public JRadioButton exactLocationButton;
    public JSpinner occurrenceSpinner;
    public JButton loadButton;
    public JButton rescanButton;
    public JButton clearButton;
    public JButton outputButton;
    public String type;

    public ProcessingPopup(String type)
    {
        this.type = type;
        setAutoRequestFocus(false);
        setTitle(type + " Processing");
        setModal(false);

        addWindowListener(new WindowActivationListener(this));
    }

    public void initialize(JPanel dataPanel) {
        addWindowListener(new WindowAdapter() {

            JMenuItem loadMenuItem = null;
            JCheckBoxMenuItem processingMenuItem = null;

            public void windowClosing(WindowEvent windowevent) {
                if (loadMenuItem == null) {
                    try {
                        Field loadMenuItemField = MenuBar.class.getDeclaredField("load" + type + "MenuItem");
                        loadMenuItem = (JMenuItem) loadMenuItemField.get(Main.view.menuBar);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (processingMenuItem == null) {
                    try {
                        Field processingMenuItemField = MenuBar.class.getDeclaredField(type.toLowerCase() + "ProcessingMenuItem");
                        processingMenuItem = (JCheckBoxMenuItem) processingMenuItemField.get(Main.view.menuBar);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (loadMenuItem != null) loadMenuItem.setEnabled(true);
                if (processingMenuItem != null) processingMenuItem.setSelected(false);
            }
        });

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        panel.add(buildMappingMethodPanel());
        panel.add(Box.createVerticalStrut(10));
        panel.add(buildFilterPanel());
        panel.add(Box.createVerticalStrut(10));
        panel.add(dataPanel);
        panel.add(buildLowerLoadedPanel(), BorderLayout.SOUTH);
        panel.add(Box.createVerticalStrut(10));
        panel.add(buildOutputButtonPanel());

        add(panel);

        pack();
        setMinimumSize(getSize());

        setVisible(false);
    }

    protected JComponent buildOutputButtonPanel() {
        outputButton = new JButton("Output to File");
        outputButton.setAlignmentX(JButton.CENTER);
        outputButton.setEnabled(false);
        outputButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(type.equals(VideoProcessingPopup.PROCESSING_POPUP_TYPE))
                    new OutputPopup(ProcessingPopup.this, true);
                else
                    new OutputPopup(ProcessingPopup.this, false);
            }
        });

        return nest(outputButton);
    }

    protected JPanel buildFilterPanel() {
        occurrenceSpinner = new JSpinner(new SpinnerNumberModel(R.DEFAULT_COUNT_THRESHOLD, 1, Integer.MAX_VALUE, 1));

        occurrenceSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                R.countThreshold = (Integer) occurrenceSpinner.getValue();

                if(R.imageData != null || R.videoData != null || R.fileData != null)
                    rescanButton.setEnabled(true);
            }
        });

        JPanel occurrencePanel = new JPanel();
        occurrencePanel.setBorder(BorderFactory.createTitledBorder(occurrencePanel.getBorder(), "Filter"));
        occurrencePanel.add(new JLabel("Occurrences: ", JLabel.RIGHT));
        occurrencePanel.add(occurrenceSpinner);

        return occurrencePanel;
    }

    protected abstract int getCountThreshold();

    protected JPanel buildLowerLoadedPanel() {
        loadButton = new JButton("Load " + type);
        rescanButton = new JButton("Re-Scan " + type);
        clearButton = new JButton("Clear " + type);
        loadButton.setHorizontalAlignment(JButton.CENTER);
        rescanButton.setHorizontalAlignment(JButton.CENTER);
        clearButton.setHorizontalAlignment(JButton.CENTER);
        rescanButton.setEnabled(false);
        clearButton.setEnabled(false);

        if(type.equals(VideoProcessingPopup.PROCESSING_POPUP_TYPE))
            rescanButton.setVisible(false);

        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (exactLocationButton.isSelected())
                    ColorModel.mappingMethod = ColorModel.MappingMethod.EXACT_LOCATION;
                else if (nearestNeighborButton.isSelected())
                    ColorModel.mappingMethod = ColorModel.MappingMethod.NEAREST_NEIGHBOR;

                load(Main.view);

                R.countThreshold = getCountThreshold();
            }
        });

        rescanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (R.imageData != null) {
                    R.dataChanged = true;
                    R.clearPlotsFlag = true;
                    rescanButton.setEnabled(false);

                    if (exactLocationButton.isSelected())
                        ColorModel.mappingMethod = ColorModel.MappingMethod.EXACT_LOCATION;
                    else if (nearestNeighborButton.isSelected())
                        ColorModel.mappingMethod = ColorModel.MappingMethod.NEAREST_NEIGHBOR;
                }
            }
        });

        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                outputButton.setEnabled(false);
                rescanButton.setEnabled(false);
                clearButton.setEnabled(false);
                R.dataChanged = true;
                R.dataUnloaded = true;
                R.imageData = null;
                R.fileData = null;
                R.videoData = null;

                if(type.equals(VideoProcessingPopup.PROCESSING_POPUP_TYPE))   {
                    VideoProcessingPopup videoProcessingPopup = (VideoProcessingPopup) ProcessingPopup.this;
                    videoProcessingPopup.frameSkipSpinner.setEnabled(false);
                    videoProcessingPopup.videoPlayer.unload();
                }

                ProcessingPopup.this.repaint();
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(loadButton);
        buttonPanel.add(clearButton);

        JPanel lowerPanel = new JPanel(new GridLayout(2,1,0,0));
        lowerPanel.add(buttonPanel);
        lowerPanel.add(nest(rescanButton));

        return lowerPanel;
    }

    protected JPanel buildMappingMethodPanel() {
        nearestNeighborButton = new JRadioButton("Nearest Neighbor", true);
        exactLocationButton = new JRadioButton("Exact Location");

        ButtonGroup group = new ButtonGroup();
        group.add(nearestNeighborButton);
        group.add(exactLocationButton);

        nearestNeighborButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (nearestNeighborButton.isSelected()) {
                    if (!Main.view.dataProcessingPopup.nearestNeighborButton.isSelected())
                        Main.view.dataProcessingPopup.nearestNeighborButton.setSelected(true);
                    if (!Main.view.imageProcessingPopup.nearestNeighborButton.isSelected())
                        Main.view.imageProcessingPopup.nearestNeighborButton.setSelected(true);
                    if (!Main.view.videoProcessingPopup.nearestNeighborButton.isSelected())
                        Main.view.videoProcessingPopup.nearestNeighborButton.setSelected(true);
                }

                if(R.imageData != null) {
                    if (!rescanButton.isEnabled())
                        rescanButton.setEnabled(true);
                }
                else if (R.videoData != null)
                    R.videoData.mappingChanged = true;
            }
        });

        exactLocationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (exactLocationButton.isSelected()) {
                    if (!Main.view.dataProcessingPopup.exactLocationButton.isSelected())
                        Main.view.dataProcessingPopup.exactLocationButton.setSelected(true);
                    if (!Main.view.imageProcessingPopup.exactLocationButton.isSelected())
                        Main.view.imageProcessingPopup.exactLocationButton.setSelected(true);
                    if (!Main.view.videoProcessingPopup.exactLocationButton.isSelected())
                        Main.view.videoProcessingPopup.exactLocationButton.setSelected(true);
                }

                if(R.imageData != null) {
                    if (!rescanButton.isEnabled())
                        rescanButton.setEnabled(true);
                }
                else if (R.videoData != null)
                    R.videoData.mappingChanged = true;
            }
        });

        JPanel mappingMethodPanel = new JPanel();
        mappingMethodPanel.setBorder(BorderFactory.createTitledBorder(mappingMethodPanel.getBorder(), "Mapping Method"));
        mappingMethodPanel.add(nearestNeighborButton);
        mappingMethodPanel.add(exactLocationButton);
        return mappingMethodPanel;
    }

    protected static JComponent nest(JComponent component)
    {
        JPanel p = new JPanel(new FlowLayout());
        p.setBorder(null);
        p.add(component);
        return p;
    }

    public boolean load(JFrame dialogParent) {
        JFileChooser chooser = null;

        LookAndFeel laf = UIManager.getLookAndFeel();
        String lafClassName = UIManager.getSystemLookAndFeelClassName();
        //if (lafClassName.contains("Windows"))
        {
            try {
                UIManager.setLookAndFeel(lafClassName);
                chooser = new JFileChooser();
                UIManager.setLookAndFeel(laf);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (chooser == null) chooser = new JFileChooser();

        FileNameExtensionFilter filter = getLoadFileFilter();
        assert  filter != null;
        chooser.setFileFilter(filter);

        int returnVal = chooser.showOpenDialog(dialogParent);
        if (returnVal == JFileChooser.APPROVE_OPTION && loadHelper(chooser.getSelectedFile()))
        {
            R.dataChanged = true;
            R.dataLoaded = true;
            Main.view.settingsPopup.enableWireframe(true);
            outputButton.setEnabled(true);
            clearButton.setEnabled(true);
            return true;
        }
        return false;
    }

    protected abstract FileNameExtensionFilter getLoadFileFilter();

    protected static void drawImage(JComponent canvas, Image image, Graphics g) {
        int imgWidth = image.getWidth(null);
        int imgHeight = image.getHeight(null);

        double imgAspect = (double) imgHeight / imgWidth;

        int canvasWidth = canvas.getWidth();
        int canvasHeight = canvas.getHeight();

        double canvasAspect = (double) canvasHeight / canvasWidth;

        int x1 = 0; // top left X position
        int y1 = 0; // top left Y position
        int x2;	// bottom right X position
        int y2;	// bottom right Y position

        if (imgWidth < canvasWidth && imgHeight < canvasHeight) {
            // the image is smaller than the canvas
            x1 = (canvasWidth - imgWidth)  / 2;
            y1 = (canvasHeight - imgHeight) / 2;
            x2 = imgWidth + x1;
            y2 = imgHeight + y1;

        } else {
            if (canvasAspect > imgAspect) {
                y1 = canvasHeight;
                // keep image aspect ratio
                canvasHeight = (int) (canvasWidth * imgAspect);
                y1 = (y1 - canvasHeight) / 2;
            } else {
                x1 = canvasWidth;
                // keep image aspect ratio
                canvasWidth = (int) (canvasHeight / imgAspect);
                x1 = (x1 - canvasWidth) / 2;
            }
            x2 = canvasWidth + x1;
            y2 = canvasHeight + y1;
        }

        g.drawImage(image, x1, y1, x2, y2, 0, 0, imgWidth, imgHeight, null);
    }
}
