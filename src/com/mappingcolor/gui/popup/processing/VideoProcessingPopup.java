package com.mappingcolor.gui.popup.processing;

import com.mappingcolor.R;
import com.mappingcolor.gui.player.video.VideoPlayer;
import com.mappingcolor.io.video.VideoData;
import com.mappingcolor.io.video.VideoDataHumble;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;

public class VideoProcessingPopup extends ProcessingPopup
{
    public static final String PROCESSING_POPUP_TYPE = "Video";

    public VideoPlayer videoPlayer;
    public JSpinner frameSkipSpinner;

    public VideoProcessingPopup()
    {
        super(PROCESSING_POPUP_TYPE);
    }

    public void initialize() {
        super.initialize(buildLoadedDataPanel());
    }

    @Override
    protected JPanel buildFilterPanel() {
        occurrenceSpinner = new JSpinner(new SpinnerNumberModel(R.DEFAULT_COUNT_THRESHOLD, 1, Integer.MAX_VALUE, 1));
        frameSkipSpinner = new JSpinner(new SpinnerNumberModel(R.DEFAULT_FRAME_SKIP, 1, Integer.MAX_VALUE, 1));
        frameSkipSpinner.setEnabled(false);
        occurrenceSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                R.countThreshold = (Integer) occurrenceSpinner.getValue();

                if(R.videoData != null)
                    rescanButton.setEnabled(true);
            }
        });

        frameSkipSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                R.frameSkip = (Integer) frameSkipSpinner.getValue();

                if(R.videoData != null)
                    rescanButton.setEnabled(true);
            }
        });

        JPanel labelPanel = new JPanel(new GridLayout(2,1,0,2));
        labelPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));
        labelPanel.add(new JLabel("Occurrences: ", JLabel.RIGHT));
        labelPanel.add(new JLabel("Frame Skip: ", JLabel.RIGHT));

        JPanel spinnerPanel = new JPanel(new GridLayout(2,1,0,2));
        spinnerPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 5));
        spinnerPanel.add(occurrenceSpinner);
        spinnerPanel.add(frameSkipSpinner);

        JPanel filterPanel = new JPanel(new BorderLayout());
        filterPanel.setBorder(BorderFactory.createTitledBorder(filterPanel.getBorder(), "Filter"));
        filterPanel.add(labelPanel, BorderLayout.WEST);
        filterPanel.add(spinnerPanel, BorderLayout.CENTER);

        return filterPanel;
    }

    @Override
    protected FileNameExtensionFilter getLoadFileFilter()
    {
        return new FileNameExtensionFilter("Videos", R.videoFileFormats);
    }

    public JPanel buildLoadedDataPanel() {
        videoPlayer = new VideoPlayer();

        JPanel loadedVideoPanel = new JPanel(new BorderLayout());
        loadedVideoPanel.setBorder(BorderFactory.createTitledBorder(loadedVideoPanel.getBorder(), "Loaded Video"));
        loadedVideoPanel.add(videoPlayer, BorderLayout.NORTH);

        return loadedVideoPanel;
    }

    public boolean loadHelper(File file) {
        //R.videoData = new VideoDataHumble(file);
        R.videoData = new VideoData(file);
        R.imageData = null;
        R.fileData = null;
        videoPlayer.load();
        frameSkipSpinner.setValue((int) Math.round(R.videoData.getVideoStreamData().getFrameRate()));
        frameSkipSpinner.setEnabled(true);
        R.videoData.mappingChanged = true;

        return true;
    }

    @Override
    protected int getCountThreshold() {
        return R.DEFAULT_COUNT_THRESHOLD;
    }
}
