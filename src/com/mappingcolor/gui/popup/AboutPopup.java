package com.mappingcolor.gui.popup;

import com.mappingcolor.gui.MyView;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class AboutPopup extends JDialog
{
    public JButton aboutClose;
    public JLabel aboutLabel;
    public JEditorPane textArea;

    public AboutPopup(JFrame frame)
    {
        super(frame);
        setUndecorated(true);
        setResizable(false);

        this.aboutClose = new JButton();
        this.aboutLabel = new JLabel();
        this.textArea = new JEditorPane();

        textArea.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
                {
                    try
                    {
                        Desktop.getDesktop().browse(e.getURL().toURI());
                    }
                    catch (Exception ex)
                    {
                       System.out.println("Unexpected exception opening link " + e.getURL());
                       ex.printStackTrace();
                    }
                }
            }
        });
    }

    public void setIcons()
    {
        aboutLabel.setIcon(new ImageIcon(this.getClass().getClassLoader().getResource("resources/images/vcu_about_clean.png")));
        aboutClose.setIcon(new ImageIcon(this.getClass().getClassLoader().getResource("resources/images/close_icon.png")));
        aboutClose.setPressedIcon(new ImageIcon(this.getClass().getClassLoader().getResource("resources/images/close_icon_pressed.png")));
    }

    public void initialize(MyView view)
    {
        setLayout(null);
        setSize(aboutLabel.getIcon().getIconWidth(), aboutLabel.getIcon().getIconHeight());
        this.textArea.setOpaque(false);
        this.textArea.setLocation(0, 10);
        this.textArea.setSize(getWidth(), 265);
        this.textArea.setEditable(false);

        String html = "";
        try {
            InputStreamReader isr = new InputStreamReader(getClass().getResourceAsStream("/resources/files/about.html"));
            BufferedReader in = new BufferedReader(isr);
            String input;
            while ((input = in.readLine()) != null) html += input;
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.textArea.setContentType("text/html");
        this.textArea.setText(html);

        aboutLabel.setLocation(0, 0);
        aboutLabel.setSize(aboutLabel.getIcon().getIconWidth(), aboutLabel.getIcon().getIconHeight());

        aboutClose.setLocation(436, 10);
        aboutClose.setSize(16, 16);
        aboutClose.setContentAreaFilled(false);
        aboutClose.setBorderPainted(false);

        JPanel frontPanel = new JPanel(null);
        frontPanel.setOpaque(false);
        frontPanel.setLocation(0, 0);
        frontPanel.setSize(getSize());
        frontPanel.add(aboutClose);
        frontPanel.add(textArea);

        JLayeredPane pane = new JLayeredPane();
        pane.add(aboutLabel);
        pane.add(frontPanel);
        pane.moveToFront(frontPanel);

        setContentPane(pane);

        final MyView finalView = view;
        aboutClose.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
                setModal(false);
                finalView.requestFocus();
               // finalView.gljPanel.requestFocusInWindow();
            }
        });

        setVisible(false);
    }

    public void display(Component parent)
    {
        setLocationRelativeTo(parent);
        setVisible(true);
        setModal(true);
        requestFocus();
    }
}