package com.mappingcolor.gui.popup;

import com.mappingcolor.R;
import com.mappingcolor.gui.MyView;
import com.mappingcolor.gui.WindowActivationListener;
import com.mappingcolor.model.ColorModel;
import com.mappingcolor.model.ColorPoint;
import com.mappingcolor.model.MyColor;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;

public class SettingsPopup extends JDialog
{
    public JSlider backgroundColorSlider;
    public JSlider sphereRadiusSlider;
    public JSlider colorAlphaSlider;
    public JSlider wireframeAlphaSlider;
    public JCheckBox wireframeEnabledCheckbox;

    class UpdateWireFrameAlphaTask extends TimerTask {
        @Override
        public void run() {
            R.updateVBOs = true;
            R.updateWireframeVertexColorVBOs = true;
            R.guiChangeComplete.cancel();
        }
    }

    class UpdateColorPointAlphaTask extends TimerTask {
        @Override
        public void run() {
            R.updateVBOs = true;
            R.updateColorPointVertexColorVBOs = true;
            R.guiChangeComplete.cancel();
        }
    }

    class UpdateRadiusTask extends TimerTask {
        @Override
        public void run() {
            R.updateVBOs = true;
            R.updateColorPointVertexColorVBOs = true;
            R.updateWireframeVertexColorVBOs = true;
            R.guiChangeComplete.cancel();
        }
    }

    public SettingsPopup(MyView view)
    {
        //super(view);
        setAutoRequestFocus(false);
        setTitle("Settings");
        setModal(false);

        addWindowListener(new WindowActivationListener(this));
    }

    public void initialize(final MyView view)
    {
        JPanel innerCenterPanel = new JPanel();
        innerCenterPanel.setLayout(new BoxLayout(innerCenterPanel, BoxLayout.Y_AXIS));
        innerCenterPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        innerCenterPanel.add(buildColorAlphaSlider());
        innerCenterPanel.add(Box.createVerticalStrut(10));
        innerCenterPanel.add(buildWireframeAlphaSliderPanel());
        innerCenterPanel.add(Box.createVerticalStrut(10));
        innerCenterPanel.add(buildSphereRadiusSlider());
        innerCenterPanel.add(Box.createVerticalStrut(10));
        innerCenterPanel.add(buildBackgroundColorSlider());
        add(innerCenterPanel);

        addListeners(view);

        pack();
        setMinimumSize(getSize());

        setVisible(false);
    }

    public void reset() {
        sphereRadiusSlider.setValue(R.DEFAULT_SPHERE_RADIUS);
        colorAlphaSlider.setValue(R.DEFAULT_COLOR_POINT_ALPHA);
        wireframeAlphaSlider.setValue(R.DEFAULT_COLOR_POINT_ALPHA);

        if(R.imageData == null && R.videoData == null && R.fileData == null)
            enableWireframe(false);
    }

    public void enableWireframe(boolean enable) {
        wireframeAlphaSlider.setEnabled(enable);
        wireframeEnabledCheckbox.setEnabled(enable);
    }

    private JSlider buildColorAlphaSlider()
    {
        colorAlphaSlider = new JSlider(1, 100, R.DEFAULT_COLOR_POINT_ALPHA);
        colorAlphaSlider.setMinimum(0);
        colorAlphaSlider.setMaximum(100);
        colorAlphaSlider.setValue(15);
        colorAlphaSlider.setMajorTickSpacing(25);
        colorAlphaSlider.setMinorTickSpacing(5);
        colorAlphaSlider.setSnapToTicks(false);
        colorAlphaSlider.setPaintTicks(true);
        colorAlphaSlider.setPaintLabels(true);
        colorAlphaSlider.setBorder(BorderFactory.createTitledBorder(colorAlphaSlider.getBorder(), "Alpha - Color Sphere"));

        return colorAlphaSlider;
    }

    private JPanel buildWireframeAlphaSliderPanel()
    {
        wireframeAlphaSlider = new JSlider(0, 100, R.DEFAULT_WIREFRAME_ALPHA);
        wireframeAlphaSlider.setMajorTickSpacing(25);
        wireframeAlphaSlider.setMinorTickSpacing(5);
        wireframeAlphaSlider.setSnapToTicks(false);
        wireframeAlphaSlider.setPaintTicks(true);
        wireframeAlphaSlider.setPaintLabels(true);
        wireframeAlphaSlider.setEnabled(false);

        wireframeEnabledCheckbox = new JCheckBox("Enable Wireframe");
        wireframeEnabledCheckbox.setHorizontalAlignment(JCheckBox.LEFT);
        wireframeEnabledCheckbox.setEnabled(false);

        JPanel wireframeAlphaPanel = new JPanel(new BorderLayout());
        wireframeAlphaPanel.setBorder(BorderFactory.createTitledBorder(wireframeAlphaPanel.getBorder(), "Alpha - Wireframe Sphere"));
        wireframeAlphaPanel.add(wireframeEnabledCheckbox, BorderLayout.NORTH);
        wireframeAlphaPanel.add(wireframeAlphaSlider, BorderLayout.SOUTH);

        return wireframeAlphaPanel;
    }

    private JSlider buildSphereRadiusSlider()
    {
        sphereRadiusSlider = new JSlider(5, 25, R.DEFAULT_SPHERE_RADIUS);
        sphereRadiusSlider.setMajorTickSpacing(5);
        sphereRadiusSlider.setMinorTickSpacing(1);
        sphereRadiusSlider.setSnapToTicks(true);
        sphereRadiusSlider.setPaintTicks(true);
        sphereRadiusSlider.setPaintLabels(true);
        sphereRadiusSlider.setBorder(BorderFactory.createTitledBorder(sphereRadiusSlider.getBorder(), "Sphere Radius"));

        return sphereRadiusSlider;
    }

    private JSlider buildBackgroundColorSlider()
    {
        int initialBackgroundColor = (int)(R.backgroundColor.getRedf() * 10);
        backgroundColorSlider = new JSlider(0, 10, initialBackgroundColor);
        backgroundColorSlider.setMinorTickSpacing(1);
        backgroundColorSlider.setMajorTickSpacing(5);
        backgroundColorSlider.setSnapToTicks(true);
        backgroundColorSlider.setPaintTicks(true);
        backgroundColorSlider.setPaintLabels(true);
        backgroundColorSlider.setBorder(BorderFactory.createTitledBorder(backgroundColorSlider.getBorder(), "Background Color"));

        return backgroundColorSlider;
    }

    private void addListeners(final MyView view)
    {
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowevent) {
                view.menuBar.settingsMenuItem.setSelected(false);
            }
        });

        backgroundColorSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                float x = backgroundColorSlider.getValue() / 10f;
                R.backgroundColor = MyColor.fromRGB(x, x, x);
            }
        });

        sphereRadiusSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ColorPoint.setRadius(sphereRadiusSlider.getValue() / 10f);
                R.guiChangeComplete.cancel();
                R.guiChangeComplete = new Timer();
                R.guiChangeComplete.schedule(new UpdateRadiusTask(), R.GUI_CHANGE_COMPLETE_TIME);
            }
        });

        colorAlphaSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ColorPoint.setAlpha(colorAlphaSlider.getValue() / 100f);
                R.guiChangeComplete.cancel();
                R.guiChangeComplete = new Timer();
                R.guiChangeComplete.schedule(new UpdateColorPointAlphaTask(), R.GUI_CHANGE_COMPLETE_TIME);
            }
        });

        wireframeAlphaSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ColorPoint.setWireframeAlpha(wireframeAlphaSlider.getValue() / 100f);
                R.guiChangeComplete.cancel();
                R.guiChangeComplete = new Timer();
                R.guiChangeComplete.schedule(new UpdateWireFrameAlphaTask(), R.GUI_CHANGE_COMPLETE_TIME);
            }
        });

        wireframeEnabledCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ColorModel.wireframeMode = wireframeEnabledCheckbox.isSelected();
            }
        });
    }
}
