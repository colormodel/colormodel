package com.mappingcolor.gui.popup;

import com.mappingcolor.gui.MyView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.List;

public class KeyBindingsPopup extends JDialog
{
    public static final List<String> keyBindings = Arrays.asList(
            "LEFT CLICK:", "Select color one",
            "RIGHT CLICK:", "Select color two",
            "", "",
            "CTRL + F:", "Clear left selected color",
            "CTRL + G:", "Clear right selected color",
            "CTRL + H:", "Clear both selected colors",
            "", "",
            "CTRL + R:", "Reset color model parameters",
            "CTRL + C:", "Reset camera position and rotation",
            "", "",
            "CTRL + W", "Color Wheel",
            "", "",
            "ALT + LEFT CLICK:", "Rotate camera",
            "ALT + RIGHT CLICK:", "Translate camera",
            "ALT + CTRL + LEFT CLICK:", "Translate camera",
            "", "",
            "UP:", "Rotate camera up",
            "DOWN:", "Rotate camera down",
            "LEFT:", "Rotate camera left",
            "RIGHT:", "Rotate camera right"
    );

    public KeyBindingsPopup(JFrame parent)
    {
        super(parent);
        setTitle("Key Bindings");
        setModal(false);
    }

    public void initialize(final MyView view)
    {
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowevent) {
                view.menuBar.keyBindingsMenuItem.setSelected(false);
            }
        });

        JPanel left = new JPanel(new GridLayout(0, 1));
        left.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        add(left, "Center");

        JPanel right = new JPanel(new GridLayout(0, 1));
        right.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        add(right, "East");

        boolean goLeft = true;
        for (String keyBind : keyBindings)
        {
            if (goLeft)
                left.add(new JLabel(keyBind, JLabel.RIGHT));
            else
                right.add(new JLabel(keyBind));
            goLeft = !goLeft;
        }

        pack();
        setMinimumSize(getSize());
        setVisible(false);
    }
}
