package com.mappingcolor.gui;

import com.apple.eawt.Application;
import com.jogamp.newt.awt.NewtCanvasAWT;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.util.FPSAnimator;
import com.mappingcolor.Main;
import com.mappingcolor.controller.Keyboard;
import com.mappingcolor.controller.Mouse;
import com.mappingcolor.gui.popup.*;
import com.mappingcolor.gui.popup.processing.DataProcessingPopup;
import com.mappingcolor.gui.popup.processing.ImageProcessingPopup;
import com.mappingcolor.gui.side.ColorFocusWidget;
import com.mappingcolor.gui.side.LoadingBar;
import com.mappingcolor.gui.side.SlidePanel;
import com.mappingcolor.gui.popup.processing.VideoProcessingPopup;
import com.mappingcolor.model.Renderer;
import com.mappingcolor.model.ColorModel;
import com.mappingcolor.gui.menu.MenuBar;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.swing.*;
import java.awt.*;

@SuppressWarnings("deprecation")
public class MyView extends JFrame
{
    public static final int FRAMES_PER_SECOND = 60; // 1000ms / 20 intervals
    public ColorModel colorModel;
    public Mouse mouse;
    public Keyboard keyboard;
    private Renderer renderer;

    // public constants
    public static final String VALUE_LABEL = "V: ";
    public static final String CHROMA_LABEL = "C: ";
    public static final String HUE_LABEL = "h: ";
    public static final String RED_LABEL = "R: ";
    public static final String GREEN_LABEL = "G: ";
    public static final String BLUE_LABEL = "B: ";
    public static final String L_LABEL = "L: ";
    public static final String A_LABEL = "A: ";
    public static final String B_LABEL = "B: ";

    // fonts
    public static Font font = new Font("calibri", Font.BOLD, 18);

    public ColorFocusWidget colorFocusWidget;

    public GLCapabilities glCapabilities = new GLCapabilities(GLProfile.getDefault());
    public GLWindow glWindow = GLWindow.create(glCapabilities);
    public NewtCanvasAWT newtCanvasAWT = new NewtCanvasAWT(glWindow);

    public SlidePanel slidePanel = new SlidePanel();

    // Menu bar stuff
    public MenuBar menuBar;

    public SettingsPopup settingsPopup = new SettingsPopup(this);
    public AboutPopup aboutPopup;
    public KeyBindingsPopup keyBindingsPopup = new KeyBindingsPopup(this);

    public ImageProcessingPopup imageProcessingPopup = new ImageProcessingPopup();
    public DataProcessingPopup dataProcessingPopup = new DataProcessingPopup();
    public VideoProcessingPopup videoProcessingPopup = new VideoProcessingPopup();

    public static LoadingBar loadingBar;

    public MyView(String title)
    {
        super(title);
        this.colorModel = new ColorModel();
        loadingBar = new LoadingBar();

        setAutoRequestFocus(false);

        this.renderer = new Renderer(this, colorModel);
        this.mouse = new Mouse();
        this.keyboard = new Keyboard();
        this.aboutPopup = new AboutPopup(this);
        this.colorFocusWidget = new ColorFocusWidget(renderer.getColorTrackers());

        setIcons(); // this should be in build()
        initialize();
        addEventListeners(); // this should be in build()
        pack();
    }

    public void setIcons() {
        Image image = new ImageIcon(this.getClass().getClassLoader().getResource("resources/images/logo.png")).getImage();
        this.setIconImage(image);
        if(OsChecker.isMac())
            Application.getApplication().setDockIconImage(image);

        settingsPopup.setIconImage(image);
        imageProcessingPopup.setIconImage(image);
        videoProcessingPopup.setIconImage(image);
        dataProcessingPopup.setIconImage(image);
        aboutPopup.setIcons();
    }

    public void addEventListeners()
    {
        glWindow.addGLEventListener(renderer);
        glWindow.addMouseListener(mouse);
        glWindow.addKeyListener(keyboard);
    }

    public void initialize()
    {
        glCapabilities.setDoubleBuffered(true);

        aboutPopup.initialize(this);
        keyBindingsPopup.initialize(this);
        colorFocusWidget.initialize();
        settingsPopup.initialize(this);

        slidePanel.initialize(this);
        imageProcessingPopup.initialize();
        dataProcessingPopup.initialize();
        videoProcessingPopup.initialize();

        newtCanvasAWT.setMinimumSize(Main.MIN_GL_WINDOW_SIZE);
        newtCanvasAWT.setPreferredSize(Main.MIN_GL_WINDOW_SIZE);

        new FPSAnimator(glWindow, MyView.FRAMES_PER_SECOND).start();

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, slidePanel, newtCanvasAWT);
        setContentPane(splitPane);

        menuBar = new MenuBar(this);
        setJMenuBar(menuBar);
    }

    public void focusAllWindows()
    {
        if (this.settingsPopup.isVisible()) this.settingsPopup.setVisible(true);
        if (this.imageProcessingPopup.isVisible()) this.imageProcessingPopup.setVisible(true);
        if (this.videoProcessingPopup.isVisible()) this.videoProcessingPopup.setVisible(true);
        if (this.dataProcessingPopup.isVisible()) this.dataProcessingPopup.setVisible(true);
        if (this.isVisible()) this.setVisible(true);
    }
}
