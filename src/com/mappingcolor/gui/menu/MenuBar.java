package com.mappingcolor.gui.menu;

import com.mappingcolor.R;
import com.mappingcolor.gui.MyView;
import com.mappingcolor.model.Camera;
import com.mappingcolor.model.ColorModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MenuBar extends JMenuBar
{
    public JMenu fileMenu = new JMenu("File");
    public JMenuItem loadImageMenuItem = new JMenuItem("Load Image");
    public JMenuItem loadVideoMenuItem = new JMenuItem("Load Video");
    public JMenuItem loadDataMenuItem = new JMenuItem("Load Data");
    public JMenuItem exitMenuItem = new JMenuItem("Exit");

    public JMenu viewMenu = new JMenu("View");
    public JMenuItem zoomInMenuItem = new StayOpenMenuItem("Zoom In");
    public JMenuItem zoomOutMenuItem = new StayOpenMenuItem("Zoom Out");
    public JMenuItem resetCameraMenuItem = new JMenuItem("Reset Camera");
    public JMenuItem resetModelMenuItem = new JMenuItem("Reset Model");
    public JCheckBoxMenuItem displayAxesMenuItem = new StayOpenCheckBoxMenuItem("Display Axes");

    public JMenu modelDrawModeMenu = new JMenu("Model Draw Mode");
    public ButtonGroup modelDrawModeGroup = new ButtonGroup();
    public JRadioButtonMenuItem rgbMenuItem = new StayOpenRadioButtonMenuItem("RGB");
    public JRadioButtonMenuItem cmykMenuItem = new StayOpenRadioButtonMenuItem("CMYK");
    public JRadioButtonMenuItem allMenuItem = new StayOpenRadioButtonMenuItem("All");

    public JMenu setTransparencyMenu = new JMenu("Set Transparency");
    public ButtonGroup setTransparencyGroup = new ButtonGroup();
    public JRadioButtonMenuItem noneMenuItem = new StayOpenRadioButtonMenuItem("None");
    public JRadioButtonMenuItem nonRgbMenuItem = new StayOpenRadioButtonMenuItem("Non-RGB");
    public JRadioButtonMenuItem transparencyAllMenuItem = new StayOpenRadioButtonMenuItem("All");

    public JMenu toolsMenu = new JMenu("Tools");
    public JCheckBoxMenuItem settingsMenuItem = new JCheckBoxMenuItem("Settings");
    public JCheckBoxMenuItem imageProcessingMenuItem = new JCheckBoxMenuItem("Image Processing");
    public JCheckBoxMenuItem dataProcessingMenuItem = new JCheckBoxMenuItem("Data Processing");
    public JCheckBoxMenuItem videoProcessingMenuItem = new JCheckBoxMenuItem("Video Processing");

    public JMenu helpMenu = new JMenu("Help");
    public JCheckBoxMenuItem keyBindingsMenuItem = new JCheckBoxMenuItem("Key Bindings");
    public JMenuItem aboutMenuItem = new JMenuItem("About");

    public MenuBar(final MyView view)
    {
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);

        initComponents();
        layoutComponents();
        addListeners(view);
    }

    private void initComponents()
    {
        displayAxesMenuItem.setSelected(R.displayAxes);
        modelDrawModeGroup.add(rgbMenuItem);
        modelDrawModeGroup.add(cmykMenuItem);
        modelDrawModeGroup.add(allMenuItem);
        rgbMenuItem.setSelected(true);

        displayAxesMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.ALT_MASK));
        rgbMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.ALT_MASK));
        cmykMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.ALT_MASK));
        allMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, InputEvent.ALT_MASK));

        setTransparencyGroup.add(noneMenuItem);
        setTransparencyGroup.add(nonRgbMenuItem);
        setTransparencyGroup.add(transparencyAllMenuItem);

        if (ColorModel.transparencyMode == ColorModel.TransparencyMode.ALL)
            transparencyAllMenuItem.setSelected(true);
        else if (ColorModel.transparencyMode == ColorModel.TransparencyMode.NON_RGB)
            nonRgbMenuItem.setSelected(true);
        else if (ColorModel.transparencyMode == ColorModel.TransparencyMode.NONE)
            noneMenuItem.setSelected(true);
    }

    private void layoutComponents()
    {
        add(fileMenu);
        fileMenu.add(loadImageMenuItem);
        fileMenu.add(loadVideoMenuItem);
        fileMenu.add(loadDataMenuItem);
        fileMenu.add(exitMenuItem);

        add(viewMenu);
        viewMenu.add(zoomInMenuItem);
        viewMenu.add(zoomOutMenuItem);
        viewMenu.addSeparator();
        viewMenu.add(resetCameraMenuItem);
        viewMenu.add(resetModelMenuItem);
        viewMenu.addSeparator();
        viewMenu.add(displayAxesMenuItem);

        viewMenu.add(modelDrawModeMenu);
        modelDrawModeMenu.add(rgbMenuItem);
        modelDrawModeMenu.add(cmykMenuItem);
        modelDrawModeMenu.add(allMenuItem);

        viewMenu.add(setTransparencyMenu);
        setTransparencyMenu.add(noneMenuItem);
        setTransparencyMenu.add(nonRgbMenuItem);
        setTransparencyMenu.add(transparencyAllMenuItem);

        add(toolsMenu);
        toolsMenu.add(settingsMenuItem);
        toolsMenu.add(imageProcessingMenuItem);
        toolsMenu.add(videoProcessingMenuItem);
        toolsMenu.add(dataProcessingMenuItem);

        add(helpMenu);
        helpMenu.add(keyBindingsMenuItem);
        helpMenu.add(aboutMenuItem);
    }

    private void addListeners(final MyView view)
    {
        loadImageMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (view.imageProcessingPopup.load(view)) {
                    imageProcessingMenuItem.setSelected(true);
                    loadImageMenuItem.setEnabled(false);
                    view.imageProcessingPopup.setVisible(true);
                    Point p = view.getLocationOnScreen();
                    p.x -= view.imageProcessingPopup.getWidth();
                    if (p.x < 0) p.x = 0;
                    view.imageProcessingPopup.setLocation(p);
                }
            }
        });

        loadVideoMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (view.videoProcessingPopup.load(view)) {
                    videoProcessingMenuItem.setSelected(true);
                    loadVideoMenuItem.setEnabled(false);
                    view.videoProcessingPopup.setVisible(true);
                    Point p = view.getLocationOnScreen();
                    p.x -= view.videoProcessingPopup.getWidth();
                    if (p.x < 0) p.x = 0;
                    view.videoProcessingPopup.setLocation(p);
                }
            }
        });

        loadDataMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (view.dataProcessingPopup.load(view)) {
                    dataProcessingMenuItem.setSelected(true);
                    loadDataMenuItem.setEnabled(false);
                    view.dataProcessingPopup.setVisible(true);
                    Point p = view.getLocationOnScreen();
                    p.x -= view.dataProcessingPopup.getWidth();
                    if (p.x < 0) p.x = 0;
                    view.dataProcessingPopup.setLocation(p);
                }
            }
        });

        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        aboutMenuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                view.aboutPopup.display(view);
                view.newtCanvasAWT.requestFocusInWindow();
            }

        });

        keyBindingsMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.keyBindingsPopup.setVisible(keyBindingsMenuItem.isSelected());
                if (keyBindingsMenuItem.isSelected())
                    view.keyBindingsPopup.setLocationRelativeTo(view);
            }
        });

        settingsMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.settingsPopup.setVisible(settingsMenuItem.isSelected());
                if (settingsMenuItem.isSelected())
                {
                    Point p = view.getLocationOnScreen();
                    p.x -= view.settingsPopup.getWidth();
                    if (p.x < 0) p.x = 0;
                    view.settingsPopup.setLocation(p);
                }
            }
        });

        imageProcessingMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                view.imageProcessingPopup.setVisible(imageProcessingMenuItem.isSelected());
                if (imageProcessingMenuItem.isSelected())
                {
                    Point p = view.getLocationOnScreen();
                    p.x -= view.imageProcessingPopup.getWidth();
                    if (p.x < 0) p.x = 0;
                    view.imageProcessingPopup.setLocation(p);
                }
            }
        });

        videoProcessingMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.videoProcessingPopup.setVisible(videoProcessingMenuItem.isSelected());
                if (videoProcessingMenuItem.isSelected())
                {
                    Point p = view.getLocationOnScreen();
                    p.x -= view.videoProcessingPopup.getWidth();
                    if (p.x < 0) p.x = 0;
                    view.videoProcessingPopup.setLocation(p);
                }
            }
        });

        dataProcessingMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                view.dataProcessingPopup.setVisible(dataProcessingMenuItem.isSelected());
                if (dataProcessingMenuItem.isSelected())
                {
                    Point p = view.getLocationOnScreen();
                    p.x -= view.dataProcessingPopup.getWidth();
                    if (p.x < 0) p.x = 0;
                    view.dataProcessingPopup.setLocation(p);
                }
            }
        });

        resetCameraMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Camera.reset();
                view.newtCanvasAWT.requestFocusInWindow();
            }

        });

        resetModelMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                R.resetModelFlag = true;
                view.newtCanvasAWT.requestFocusInWindow();
            }

        });

        zoomInMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Camera.position.addZ(Camera.ZOOM_DELTA);
                view.newtCanvasAWT.requestFocusInWindow();
            }
        });

        zoomOutMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Camera.position.subtractZ(Camera.ZOOM_DELTA);
                view.newtCanvasAWT.requestFocusInWindow();
            }
        });

        displayAxesMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                R.displayAxes = displayAxesMenuItem.isSelected();
            }
        });

        rgbMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rgbMenuItem.isSelected())
                    ColorModel.drawMode = ColorModel.DrawMode.RGB;

                if(R.enabledVBO) {
                    R.updateVBOs = true;
                    R.updateColorPointVertexColorVBOs = true;

                    if (ColorModel.mappingMethod == ColorModel.MappingMethod.EXACT_LOCATION)
                        R.updatePigmentVBOs = true;
                }
            }
        });

        cmykMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (cmykMenuItem.isSelected())
                    ColorModel.drawMode = ColorModel.DrawMode.CMYK;

                if(R.enabledVBO) {
                    R.updateVBOs = true;
                    R.updateColorPointVertexColorVBOs = true;

                    if (ColorModel.mappingMethod == ColorModel.MappingMethod.EXACT_LOCATION)
                        R.updatePigmentVBOs = true;
                }
            }
        });

        allMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (allMenuItem.isSelected())
                    ColorModel.drawMode = ColorModel.DrawMode.ALL;

                if(R.enabledVBO) {
                    R.updateVBOs = true;
                    R.updateColorPointVertexColorVBOs = true;

                    if (ColorModel.mappingMethod == ColorModel.MappingMethod.EXACT_LOCATION)
                        R.updatePigmentVBOs = true;
                }
            }
        });

        noneMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (noneMenuItem.isSelected()) {
                    ColorModel.transparencyMode = ColorModel.TransparencyMode.NONE;
                    R.transparencyModeChangedFlag = true;
                }
            }
        });

        nonRgbMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (nonRgbMenuItem.isSelected()) {
                    ColorModel.transparencyMode = ColorModel.TransparencyMode.NON_RGB;
                    R.transparencyModeChangedFlag = true;
                }
            }
        });

        transparencyAllMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (transparencyAllMenuItem.isSelected()) {
                    ColorModel.transparencyMode = ColorModel.TransparencyMode.ALL;
                    R.transparencyModeChangedFlag = true;
                }
            }
        });
    }
}
