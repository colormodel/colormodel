package com.mappingcolor.gui.side;

import com.mappingcolor.R;
import com.mappingcolor.gui.MyView;
import com.mappingcolor.model.*;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;

public class ColorValues extends JPanel
{
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.0");

    private double[] lab = new double[3];
    private double[] vch = new double[3];
    private int[] rgb = new int[3];

    private JLabel firstLabel;
    private JLabel secondLabel;
    private JLabel thirdLabel;

    private JLabel firstValue;
    private JLabel secondValue;
    private JLabel thirdValue;

    private MyColor.ModelType mode;

    private ColorTracker colorTracker;

    public ColorValues(ColorTracker colorTracker)
    {
        this.colorTracker = colorTracker;
        this.firstLabel = new JLabel();
        this.firstLabel.setPreferredSize(new Dimension(18, 16));
        this.secondLabel = new JLabel();
        this.secondLabel.setPreferredSize(new Dimension(18, 16));
        this.thirdLabel = new JLabel();
        this.thirdLabel.setPreferredSize(new Dimension(18, 16));

        this.firstValue = new JLabel();
        this.firstValue.setHorizontalTextPosition(JLabel.RIGHT);
        this.firstValue.setPreferredSize(new Dimension(35, 16));

        this.secondValue = new JLabel();
        this.secondValue.setHorizontalTextPosition(JLabel.RIGHT);
        this.secondValue.setPreferredSize(new Dimension(35, 16));

        this.thirdValue = new JLabel();
        this.thirdValue.setHorizontalTextPosition(JLabel.RIGHT);
        this.thirdValue.setPreferredSize(new Dimension(35, 16));

        setLayout(new GridLayout(3, 1, 0, 0));
        add(merge(this.firstLabel, this.firstValue));
        add(merge(this.secondLabel, this.secondValue));
        add(merge(this.thirdLabel, this.thirdValue));

        this.mode = MyColor.ModelType.VCH;
        updateUnitLabels();
        clearValueLabels();
    }

    public void setMode(MyColor.ModelType mode)
    {
        this.mode = mode;
        updateUnitLabels();
        if (!this.firstValue.getText().equals("")) updateValueLabels();
    }

    public void clearValueLabels()
    {
        this.firstValue.setText("---");
        this.secondValue.setText("---");
        this.thirdValue.setText("---");
    }

    public void updateValues(MyColor color)
    {
        updateVCH(color);
        updateLAB(color);
        updateRGB(color);
        updateValueLabels();
    }

    protected void updateVCH(MyColor color)
    {
        this.vch[0] = color.getValue();
        this.vch[1] = color.getChroma();
        this.vch[2] = color.getHue();
    }

    protected void updateLAB(MyColor color)
    {
        this.lab[0] = color.getL();
        this.lab[1] = color.getA();
        this.lab[2] = color.getB();
    }

    protected void updateRGB(MyColor color)
    {
        this.rgb[0] = color.getRed();
        this.rgb[1] = color.getBlue();
        this.rgb[2] = color.getGreen();
    }

    protected void updateValueLabels()
    {
        if(colorTracker.getColor() != R.backgroundColor) {
            if (this.mode == MyColor.ModelType.VCH) {
                this.firstValue.setText(DECIMAL_FORMAT.format(this.vch[0]));
                this.secondValue.setText(DECIMAL_FORMAT.format(this.vch[1]));
                this.thirdValue.setText(DECIMAL_FORMAT.format(this.vch[2]));
            } else if (this.mode == MyColor.ModelType.LAB) {
                this.firstValue.setText(DECIMAL_FORMAT.format(this.lab[0]));
                this.secondValue.setText(DECIMAL_FORMAT.format(this.lab[1]));
                this.thirdValue.setText(DECIMAL_FORMAT.format(this.lab[2]));
            } else //if (this.mode == MyColor.Format.RGB)
            {
                this.firstValue.setText(String.valueOf(this.rgb[0]));
                this.secondValue.setText(String.valueOf(this.rgb[1]));
                this.thirdValue.setText(String.valueOf(this.rgb[2]));
            }
        }
    }

    protected void updateUnitLabels()
    {
        if (this.mode == MyColor.ModelType.VCH)
        {
            this.firstLabel.setText(MyView.VALUE_LABEL);
            this.secondLabel.setText(MyView.CHROMA_LABEL);
            this.thirdLabel.setText(MyView.HUE_LABEL);
        }
        else if (this.mode == MyColor.ModelType.LAB)
        {
            this.firstLabel.setText(MyView.L_LABEL);
            this.secondLabel.setText(MyView.A_LABEL);
            this.thirdLabel.setText(MyView.B_LABEL);
        }
        else // if (this.mode == MyColor.Format.RGB)
        {
            this.firstLabel.setText(MyView.RED_LABEL);
            this.secondLabel.setText(MyView.BLUE_LABEL);
            this.thirdLabel.setText(MyView.GREEN_LABEL);
        }
    }

    private static JPanel merge(JLabel label, JLabel component)
    {
        JPanel panel = new JPanel();
        panel.add(label);
        panel.add(component);
        return panel;
    }
}
