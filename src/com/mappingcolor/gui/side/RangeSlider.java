package com.mappingcolor.gui.side;

import javax.swing.*;

public class RangeSlider extends JSlider {
    private boolean compliment = false;

//boolean isOne = false;
    /**
     * Constructs a com.mappingcolor.gui.side.RangeSlider with default minimum and maximum values of 0
     * and 100.
     */
    public RangeSlider() {
        initSlider();
    }

    public boolean getCompliment() {
        return compliment;
    }

    public void setCompliment(boolean compliment) {
        this.compliment = compliment;
    }

    /**
     * Constructs a com.mappingcolor.gui.side.RangeSlider with the specified default minimum and maximum
     * values.
     */
    public RangeSlider(int min, int max) {
        super(min, max);
        initSlider();
    }

    /**
     * Initializes the slider by setting default properties.
     */
    private void initSlider() {
        setOrientation(VERTICAL);
        setFocusable(false);
        //setBackground(MyView.WIDGET_COLOR);
    }

    /**
     * Overrides the superclass method to install the UI delegate to draw two
     * thumbs.
     */
    @Override
    public void updateUI() {
        setUI(new RangeSliderUI(this));
        // Update UI for slider labels.  This must be called after updating the
        // UI of the slider.  Refer to JSlider.updateUI().
        updateLabelUIs();
    }

    /**
     * Returns the lower value in the range.
     */
    @Override
    public int getValue() {
        return super.getValue();
    }

    /**
     * Sets the lower value in the range.
     */
    @Override
    public void setValue(int value) {
//    	if (isOne)
//    	{
//    		super.setValue(value);
//    		return;
//    	}else
    	{
            int oldValue = getValue();
            if (oldValue == value) {
                return;
            }

            // Compute new value and extent to maintain upper value.
            int oldExtent = getExtent();
            int newValue = Math.min(Math.max(getMinimum(), value), oldValue + oldExtent);
            int newExtent = oldExtent + oldValue - newValue;

            // Set new value and extent, and fire a single change event.
            getModel().setRangeProperties(newValue, newExtent, getMinimum(),
                getMaximum(), getValueIsAdjusting());
    	}
    }

    /**
     * Returns the upper value in the range.
     */
    public int getUpperValue() {
        return getValue() + getExtent();
    }

    /**
     * Sets the upper value in the range.
     */
    public void setUpperValue(int value) {
        if (getValue() > value) setValue(value);
    	//if(isOne)
//    	{
//    		setExtent(0);
//
//    	}else
    	{
        // Compute new extent.
        int lowerValue = getValue();
        int newExtent = Math.min(Math.max(0, value - lowerValue), getMaximum() - lowerValue);
        
        // Set extent to set upper value.
        setExtent(newExtent);
    	}
    }
}