package com.mappingcolor.gui.side;

import com.mappingcolor.model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ColorFocusWidget extends JPanel
{
    public JLabel leftColorLabel;
    public JLabel rightColorLabel;

    public ColorValues leftLabelPanel;
    public ColorValues rightLabelPanel;

    public ColorBox colorButton = new ColorBox();

    public JRadioButton vchButton;
    public JRadioButton labButton;
    public JRadioButton rgbButton;

    private ColorTracker[] colorTrackers;

    public ColorFocusWidget(ColorTracker[] colorTrackers)
    {
        this.colorTrackers = colorTrackers;

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5,5,5,5),
                BorderFactory.createTitledBorder(getBorder(), "Color")));
    }

    public void initialize()
    {
        leftColorLabel = new JLabel();
        leftColorLabel.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        leftColorLabel.setHorizontalAlignment(JLabel.LEFT);
        leftColorLabel.setHorizontalTextPosition(JLabel.LEFT);
        leftColorLabel.setVisible(false);

        rightColorLabel = new JLabel();
        rightColorLabel.setAlignmentX(JLabel.RIGHT_ALIGNMENT);
        rightColorLabel.setHorizontalAlignment(JLabel.RIGHT);
        rightColorLabel.setHorizontalTextPosition(JLabel.RIGHT);
        rightColorLabel.setVisible(false);

        vchButton = new JRadioButton("VCh", true);
        labButton = new JRadioButton("LAB", false);
        rgbButton = new JRadioButton("RGB", false);

        vchButton.setAlignmentX(JRadioButton.CENTER_ALIGNMENT);
        labButton.setAlignmentX(JRadioButton.CENTER_ALIGNMENT);
        rgbButton.setAlignmentX(JRadioButton.CENTER_ALIGNMENT);

        leftLabelPanel = new ColorValues(colorTrackers[0]);
        rightLabelPanel = new ColorValues(colorTrackers[1]);

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(vchButton);
        buttonGroup.add(labButton);
        buttonGroup.add(rgbButton);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(colorButton);

        JPanel radioPanel = new JPanel(new GridLayout(1, 3, 0, 0));
        radioPanel.add(vchButton);
        radioPanel.add(labButton);
        radioPanel.add(rgbButton);

        JPanel lowerPanel = new JPanel(new BorderLayout());
        lowerPanel.add(buttonPanel, BorderLayout.CENTER);
        lowerPanel.add(leftLabelPanel, BorderLayout.WEST);
        lowerPanel.add(rightLabelPanel, BorderLayout.EAST);
        lowerPanel.add(nest(radioPanel), BorderLayout.SOUTH);

        JPanel upperPanel = new JPanel();
        upperPanel.setLayout(new BoxLayout(upperPanel, BoxLayout.X_AXIS));
        upperPanel.add(leftColorLabel);
        upperPanel.add(Box.createHorizontalGlue());
        upperPanel.add(rightColorLabel);

        add(upperPanel, BorderLayout.CENTER);
        add(lowerPanel, BorderLayout.SOUTH);

        vchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (vchButton.isSelected())
                {
                    leftLabelPanel.setMode(MyColor.ModelType.VCH);
                    rightLabelPanel.setMode(MyColor.ModelType.VCH);
                }
            }
        });

        labButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (labButton.isSelected())
                {
                    leftLabelPanel.setMode(MyColor.ModelType.LAB);
                    rightLabelPanel.setMode(MyColor.ModelType.LAB);
                }
            }
        });

        rgbButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rgbButton.isSelected())
                {
                    leftLabelPanel.setMode(MyColor.ModelType.RGB);
                    rightLabelPanel.setMode(MyColor.ModelType.RGB);
                }
            }
        });
    }

    private JComponent nest(JComponent component)
    {
        JPanel panel = new JPanel();
        panel.add(component);
        return panel;
    }
}