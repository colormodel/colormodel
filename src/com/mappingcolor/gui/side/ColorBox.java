package com.mappingcolor.gui.side;

import com.mappingcolor.R;

import javax.swing.*;
import java.awt.*;

public class ColorBox extends JPanel
{
//    public Color color;

    public ColorBox left;
    public ColorBox right;
    public ColorBox()
    {
//        this.color = Color.BLACK;
        setLayout(null);
        setSize(64,64);
        setMinimumSize(new Dimension(64,64));
        setPreferredSize(new Dimension(64, 64));
        setBackground(R.backgroundColor.getColor());

        left = new ColorBox(64,32);
        right = new ColorBox(64,32);

        left.setLocation(0,0);
        right.setLocation(32,0);

        add(left);
        add(right);
    }

    public ColorBox(int height, int width)
    {
        setSize(width, height);
        setPreferredSize(new Dimension(width, height));
        setBackground(R.backgroundColor.getColor());
    }

//    @Override
//    public void paint(Graphics g)
//    {
//        super.paint(g);
//        g.setColor(color);
//        g.clearRect(0, 0, getWidth(), getHeight());
//        g.fillRect(0, 0, getWidth(), getHeight());
//
//    }
}
