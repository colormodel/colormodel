package com.mappingcolor.gui.side;

import com.mappingcolor.Main;
import com.mappingcolor.R;
import com.mappingcolor.gui.MyView;
import com.mappingcolor.gui.colorwheel.ColorWheelPanel;
import com.mappingcolor.model.ColorModel;
import com.mappingcolor.model.MyColor;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SlidePanel extends JPanel
{
    public JCheckBox singleV, singleC, singleH;

    public JLabel minValueLabel = new JLabel("Min Value: 0");
    public JLabel minChromaLabel = new JLabel("Min Chroma: 0");
    public JLabel minHueLabel = new JLabel("Min Hue: 0");

    public JLabel maxValueLabel = new JLabel("Max Value: 100");
    public JLabel maxChromaLabel = new JLabel("Max Chroma: 100");
    public JLabel maxHueLabel = new JLabel("Max Hue: 360");

    public RangeSlider rangeSliderValue = new RangeSlider();
    public RangeSlider rangeSliderChroma = new RangeSlider();
    public RangeSlider rangeSliderHue = new RangeSlider();

    public JSpinner valueSpinner;
    public JSpinner chromaSpinner;
    public JSpinner hueSpinner;

    public JButton changeStepSizeButton = new JButton("Set Step Size");

    public TitledBorder valueBorder;
    public TitledBorder chromaBorder;
    public TitledBorder hueBorder;
    public static String valueFormat = "Value - Range [%s, %s]";
    public static String chromaFormat = "Chroma - Range [%s, %s]";
    public static String hueFormat = "Hue - Range [%s, %s]";

    public ColorWheelPanel colorWheelPanel;

    public SlidePanel()
    {
        setLayout(new BorderLayout());
        setMinimumSize(Main.MIN_SLIDE_PANEL_SIZE);
    }

    public void initialize(final MyView view)
    {
        rangeSliderValue.setOrientation(JSlider.VERTICAL);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        //panel.add(buildValueSliderPanel(view));
        //panel.add(buildChromaSliderPanel(view));
        //panel.add(buildHueSliderPanel(view));
        panel.add(colorWheelPanel = new ColorWheelPanel());
        panel.add(buildSpinnerPanel());
        add(panel, BorderLayout.NORTH);

        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(view.colorFocusWidget);
        panel.add(MyView.loadingBar);
        add(panel, BorderLayout.SOUTH);

        changeStepSizeButton.setEnabled(false);

        addListeners(view);
    }

    public void reset() {
        rangeSliderValue.setValue(0);
        rangeSliderValue.setUpperValue(MyColor.MAX_VALUE);
        rangeSliderChroma.setValue(0);
        rangeSliderChroma.setUpperValue(MyColor.MAX_CHROMA);
        rangeSliderHue.setValue(0);
        rangeSliderHue.setUpperValue(MyColor.MAX_HUE);
        valueSpinner.setValue(R.DEFAULT_STEP_SIZE);
        chromaSpinner.setValue(R.DEFAULT_STEP_SIZE);
        hueSpinner.setValue(R.DEFAULT_STEP_SIZE);
    }

    private void addListeners(final MyView view)
    {

        rangeSliderValue.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ColorModel.valueLowerBound = rangeSliderValue.getValue();
                ColorModel.valueUpperBound = rangeSliderValue.getUpperValue();
                ColorModel.valueCompliment = rangeSliderValue.getCompliment();
                if(rangeSliderValue.getCompliment()) {
                    minValueLabel.setText("Min Value: " + rangeSliderValue.getUpperValue());
                    maxValueLabel.setText("Max Value: " + rangeSliderValue.getValue());
                    valueBorder.setTitle(String.format(valueFormat, rangeSliderValue.getUpperValue(), rangeSliderValue.getValue()));
                }
                else {
                    minValueLabel.setText("Min Value: " + rangeSliderValue.getValue());
                    maxValueLabel.setText("Max Value: " + rangeSliderValue.getUpperValue());
                    valueBorder.setTitle(String.format(valueFormat, rangeSliderValue.getValue(), rangeSliderValue.getUpperValue()));
                }
                updateUI();
            }
        });

        rangeSliderChroma.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
<<<<<<< HEAD
                ColorModel.chromaLowerBound = rangeSliderChroma.getValue();
                ColorModel.chromaUpperBound = rangeSliderChroma.getUpperValue();
                ColorModel.chromaCompliment = rangeSliderChroma.getCompliment();
                if(rangeSliderChroma.getCompliment()) {
                    minChromaLabel.setText("Min Chroma: " + rangeSliderChroma.getUpperValue());
                    maxChromaLabel.setText("Max Chroma: " + rangeSliderChroma.getValue());
                    chromaBorder.setTitle(String.format(chromaFormat, rangeSliderChroma.getUpperValue(), rangeSliderChroma.getValue()));
                }
                else {
                    minChromaLabel.setText("Min Chroma: " + rangeSliderChroma.getValue());
                    maxChromaLabel.setText("Max Chroma: " + rangeSliderChroma.getUpperValue());
                    chromaBorder.setTitle(String.format(chromaFormat, rangeSliderChroma.getValue(), rangeSliderChroma.getUpperValue()));
                }
=======
                //ColorModel.chromaLowerBound = rangeSliderChroma.getValue();
                //ColorModel.chromaUpperBound = rangeSliderChroma.getUpperValue();
                minChromaLabel.setText("Min Chroma: " + rangeSliderChroma.getValue());
                maxChromaLabel.setText("Max Chroma: " + rangeSliderChroma.getUpperValue());
                chromaBorder.setTitle(String.format(chromaFormat, rangeSliderChroma.getValue(), rangeSliderChroma.getUpperValue()));
>>>>>>> origin/colorwheel
                updateUI();
            }
        });

        rangeSliderHue.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                //ColorModel.hueLowerBound = rangeSliderHue.getValue();
                //ColorModel.hueUpperBound = rangeSliderHue.getUpperValue();
                minHueLabel.setText("Min Hue: " + rangeSliderHue.getValue());
                maxHueLabel.setText("Max Hue: " + rangeSliderHue.getUpperValue());
                hueBorder.setTitle(String.format(hueFormat, rangeSliderHue.getValue(), rangeSliderHue.getUpperValue()));
                updateUI();
            }
        });

        valueSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ColorModel.valueStepSize = (Integer) valueSpinner.getValue();
                changeStepSizeButton.setEnabled(true);
            }
        });

        chromaSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ColorModel.chromaStepSize = (Integer) chromaSpinner.getValue();
                changeStepSizeButton.setEnabled(true);
            }
        });

        hueSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ColorModel.hueStepSize = (Integer) hueSpinner.getValue();
                changeStepSizeButton.setEnabled(true);
            }
        });

        changeStepSizeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                R.stepSizeChangedFlag = true;
                changeStepSizeButton.setEnabled(false);
            }
        });
    }

    private JPanel buildSpinnerPanel()
    {
        valueSpinner = new JSpinner(new SpinnerNumberModel(10, 2, 25, 1));
        chromaSpinner = new JSpinner(new SpinnerNumberModel(10, 2, 25, 1));
        hueSpinner = new JSpinner(new SpinnerNumberModel(10, 2, 45, 1));

        valueSpinner.setPreferredSize(new Dimension(43, 20));
        chromaSpinner.setPreferredSize(new Dimension(43, 20));
        hueSpinner.setPreferredSize(new Dimension(43, 20));

<<<<<<< HEAD
        JPanel middle = new JPanel(new GridLayout(0, 3, 0, 0));
=======
//        JPanel top = new JPanel(new GridLayout(0, 3, 0, 0));
//        top.add(new JLabel(" Value: ", JLabel.CENTER));
//        top.add(new JLabel(" Chroma: ", JLabel.CENTER));
//        top.add(new JLabel(" hue: ", JLabel.CENTER));

        JPanel middle = new JPanel(new GridLayout(0, 3, 0, 0));
//        middle.setBorder(BorderFactory.createEmptyBorder(2, 0, 5, 0));
>>>>>>> origin/colorwheel
        JPanel p = new JPanel();
        p.add(new JLabel("V:"));
        p.add(valueSpinner);
        middle.add(p);

        p = new JPanel();
        p.add(new JLabel("C:"));
        p.add(chromaSpinner);
        middle.add(p);
<<<<<<< HEAD

        p = new JPanel();
        p.add(new JLabel("h:"));
        p.add(hueSpinner);
        middle.add(p);
=======

        p = new JPanel();
        p.add(new JLabel("h:"));
        p.add(hueSpinner);
        middle.add(p);

//        middle.add(valueSpinner);
//        middle.add(chromaSpinner);
//        middle.add(hueSpinner);
>>>>>>> origin/colorwheel

        JPanel spinnerPanel = new JPanel(new BorderLayout());
        spinnerPanel.setBorder(BorderFactory.createTitledBorder(spinnerPanel.getBorder(), "Step Size"));

<<<<<<< HEAD
=======
        //spinnerPanel.add(top, BorderLayout.NORTH);
>>>>>>> origin/colorwheel
        spinnerPanel.add(middle);
        spinnerPanel.add(changeStepSizeButton, BorderLayout.SOUTH);

        return spinnerPanel;
    }

    private JPanel buildValueSliderPanel(MyView view)
    {
        rangeSliderValue.setMinorTickSpacing(view.colorModel.getValueStep());
        rangeSliderValue.setMajorTickSpacing(MyColor.MAX_VALUE / 2);

        rangeSliderValue.setMinimum(0);
        rangeSliderValue.setMaximum(MyColor.MAX_VALUE);
        rangeSliderValue.setValue(0);
        rangeSliderValue.setUpperValue(MyColor.MAX_VALUE);
        rangeSliderValue.setPaintLabels(true);
        rangeSliderValue.setPaintTicks(true);
        rangeSliderValue.setSnapToTicks(false);

        maxValueLabel.setFont(MyView.font);
        minValueLabel.setFont(MyView.font);
        maxValueLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        minValueLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        minValueLabel.setText("Min Value: " + rangeSliderValue.getValue());
        maxValueLabel.setText("Max Value: " + rangeSliderValue.getUpperValue());

        JPanel valuePanel = new JPanel();
        valuePanel.setLayout(new BoxLayout(valuePanel, BoxLayout.PAGE_AXIS));
        valueBorder = BorderFactory.createTitledBorder(valuePanel.getBorder(), String.format(valueFormat, 0, MyColor.MAX_VALUE));
        valuePanel.setBorder(valueBorder);
        //valuePanel.add(maxValueLabel);
        valuePanel.add(rangeSliderValue);
        // valuePanel.add(minValueLabel);

        return valuePanel;
    }

    private JPanel buildChromaSliderPanel(MyView view)
    {
        rangeSliderChroma.setMinorTickSpacing(view.colorModel.getChromaStep());
        rangeSliderChroma.setMajorTickSpacing(40);

        rangeSliderChroma.setMinimum(0);
        rangeSliderChroma.setMaximum(MyColor.MAX_CHROMA);
        rangeSliderChroma.setValue(0);
        rangeSliderChroma.setUpperValue(MyColor.MAX_CHROMA);
        rangeSliderChroma.setPaintLabels(true);
        rangeSliderChroma.setPaintTicks(true);
        rangeSliderChroma.setSnapToTicks(false);

        maxChromaLabel.setFont(MyView.font);
        minChromaLabel.setFont(MyView.font);

        maxChromaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        minChromaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        minChromaLabel.setText("Min Chroma: " + rangeSliderChroma.getValue());
        maxChromaLabel.setText("Max Chroma: " + rangeSliderChroma.getUpperValue());

        JPanel chromaPanel = new JPanel();
        chromaPanel.setLayout(new BoxLayout(chromaPanel, BoxLayout.PAGE_AXIS));
        chromaBorder = BorderFactory.createTitledBorder(chromaPanel.getBorder(), String.format(chromaFormat, 0, MyColor.MAX_CHROMA));
        chromaPanel.setBorder(chromaBorder);
        chromaPanel.add(rangeSliderChroma);

        return chromaPanel;
    }

    private JPanel buildHueSliderPanel(MyView view)
    {
        rangeSliderHue.setMinorTickSpacing(15);
        rangeSliderHue.setMajorTickSpacing(view.colorModel.getHueStep() * 6);

        rangeSliderHue.setMinimum(0);
        rangeSliderHue.setMaximum(MyColor.MAX_HUE);
        rangeSliderHue.setValue(0);
        rangeSliderHue.setUpperValue(MyColor.MAX_HUE);
        rangeSliderHue.setPaintLabels(true);
        rangeSliderHue.setPaintTicks(true);
        rangeSliderHue.setSnapToTicks(false);

        maxHueLabel.setFont(MyView.font);
        minHueLabel.setFont(MyView.font);

        maxHueLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        minHueLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        minHueLabel.setText("Min Hue: " + rangeSliderHue.getValue());
        maxHueLabel.setText("Max Hue: " + rangeSliderHue.getUpperValue());

        JPanel huePanel = new JPanel();
        huePanel.setLayout(new BoxLayout(huePanel, BoxLayout.PAGE_AXIS));
        hueBorder = BorderFactory.createTitledBorder(huePanel.getBorder(), String.format(hueFormat, 0, MyColor.MAX_HUE));
        huePanel.setBorder(hueBorder);
        huePanel.add(rangeSliderHue);

        return huePanel;
    }

    public void setSingleShow(final MyView view)
    {
        String vText = "Single L View";
        String cText = "Single C View";
        String hText = "Single H View";

        singleV = new JCheckBox();
        singleV.setText(vText);
        singleV.setFont(MyView.font);
        singleV.setSelected(false);
        singleV.setOpaque(false);
        //add(singleV);

        singleC = new JCheckBox();
        singleC.setText(cText);
        singleC.setFont(MyView.font);
        singleC.setSelected(false);
        singleC.setOpaque(false);
        //add(singleC);

        singleH = new JCheckBox();
        singleH.setText(hText);
        singleH.setFont(MyView.font);
        singleH.setSelected(false);
        singleH.setOpaque(false);
        //add(singleH);

        singleV.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println(singleV.getText() + " is "
                        + singleV.isSelected());
                //view.menuBar.displayEntireModelMenuItem.setSelected(true);
                view.repaint();
                view.newtCanvasAWT.requestFocusInWindow();
            }
        });

        singleC.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println(singleC.getText() + " is "
                        + singleC.isSelected());
                //view.menuBar.displayEntireModelMenuItem.setSelected(true);
                view.repaint();
                view.newtCanvasAWT.requestFocusInWindow();
            }
        });

        singleH.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println(singleH.getText() + " is "
                        + singleH.isSelected());
                //view.menuBar.displayEntireModelMenuItem.setSelected(true);
                view.repaint();
                view.newtCanvasAWT.requestFocusInWindow();
            }
        });
    }
}
