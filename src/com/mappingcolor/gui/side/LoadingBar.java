package com.mappingcolor.gui.side;

import javax.swing.*;
import java.awt.*;

public class LoadingBar extends JPanel
{
    private static final String DEFAULT_LOADING_OBJECT = "Model";
    private static final String PROGRESS_STRING = "Loading %s... %s%%";

    private JProgressBar progressBar = new JProgressBar(0, 100);
    private String loadingObject = DEFAULT_LOADING_OBJECT;

    private static int progress = 0;

    public LoadingBar()
    {
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0, 7, 5, 7));

        progressBar.setStringPainted(true);
        progressBar.setBackground(Color.WHITE);
        updateProgress(100);
        add(progressBar);
    }

    public void start(int maximumProgress, String loadingObject)
    {
        this.loadingObject = loadingObject != null ? loadingObject : DEFAULT_LOADING_OBJECT;
        progressBar.setMaximum(maximumProgress);
        updateProgress(0);
    }

    public void updateProgress(int progress) {
        LoadingBar.progress = progress;
        repaintSafely();
    }

    public void repaintSafely() {
        if (SwingUtilities.isEventDispatchThread()) {
            progressBar.setValue(progress);
            updateProgressString();
            progressBar.repaint();
        }
        else {
            try {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        repaintSafely();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isLoading()
    {
        return progress < progressBar.getMaximum();
    }

    private void updateProgressString() {
        if(isLoading())
            progressBar.setString(String.format(PROGRESS_STRING, loadingObject, Math.round(progressBar.getPercentComplete() * 100)));
        else
            progressBar.setString("Color Model Loaded");
    }
}
