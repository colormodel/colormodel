package com.mappingcolor.gui.player.video;

import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.IMediaViewer;
import com.xuggle.mediatool.MediaListenerAdapter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.mediatool.event.IVideoPictureEvent;

import java.awt.*;
import java.awt.image.BufferedImage;

import javax.swing.*;

public class Playback extends JPanel {

    String whatToPlay;
    private boolean mantainRatio = true;
    private ImageComponent imageComponent;
    private int videoWidth = getWidth();
    private int videoHeight = getHeight();
    private BufferedImage currentImage = null;
    private boolean firstTime = false;
    private Playback curObj;

    public Playback() {
        super();
        setMinimumSize(new Dimension(235, 235));
        setPreferredSize(new Dimension(235, 235));
        this.setVisible(true);
        this.setBackground(Color.BLACK);
        this.setOpaque(true);
    }

    public void load(String fileName) {
        firstTime = true;
        whatToPlay = fileName;
        curObj = this;

        this.setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.LINE_AXIS));
        imageComponent = new ImageComponent();
        this.add(imageComponent);
    }

    public boolean isFullScreen() {
        return isFullScreen();
    }

    private void resize() {
        if (currentImage == null) {
            return;
        }

        if (mantainRatio) {
            double imgW = currentImage.getWidth(this);
            double imgH = currentImage.getHeight(this);

            double ratio1 = (double) getWidth() / imgW;
            double ratio2 = (double) getHeight() / imgH;

            double scaleFactor = Math.min(ratio1, ratio2);
            videoWidth = (int) (currentImage.getWidth(this) * scaleFactor);
            videoHeight = (int) (currentImage.getHeight(this) * scaleFactor);
        } else {
            videoWidth = getWidth();
            videoHeight = getHeight();
        }
    }

    public void setImage(final BufferedImage aImage) {
        currentImage = aImage;
        if (firstTime) {
            curObj.resize();
            firstTime = false;
        }
        imageComponent.setImage(aImage);
    }

    public void setMantainRatio(boolean mantainRatio) {
        this.mantainRatio = mantainRatio;
    }

    public class ImageComponent extends JComponent {

        private BufferedImage image;

        public void setImage(BufferedImage image) {
            this.image = image;
            repaint();
            //SwingUtilities.invokeLater(new ImageRunnable(image));
        }

        private class ImageRunnable implements Runnable {

            private final BufferedImage newImage;

            public ImageRunnable(BufferedImage newImage) {
                super();
                this.newImage = newImage;
            }

            public void run() {
                ImageComponent.this.image = newImage;
                repaint();
            }
        }

        public synchronized void paint(Graphics g) {
            if (image != null) {
                g.drawImage(image, (getWidth() - videoWidth) / 2, (getHeight() - videoHeight) / 2, videoWidth, videoHeight, this);
            }
        }
    }

    public void startPlayback() {
        new Thread(new Runnable() {

            public void run() {
                IMediaReader mediaReader = ToolFactory.makeReader(whatToPlay);

                // stipulate that we want BufferedImages created in BGR 24bit color space
                mediaReader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);
                mediaReader.addListener(new ImageSnapListener());
                mediaReader.addListener(ToolFactory.makeViewer(IMediaViewer.Mode.VIDEO_ONLY));
                // read out the contents of the media file and
                // dispatch events to the attached listener
                //while (mediaReader.readPacket() == null);

                while(mediaReader.readPacket() == null);
                mediaReader.close();
            }
        }).start();
    }

    private class ImageSnapListener extends MediaListenerAdapter {
        public void onVideoPicture(IVideoPictureEvent event) {
            setImage(event.getImage());
        }
    }
}