package com.mappingcolor.gui.player.video;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class PlayPauseButton extends JButton
{
    private static final String PLAY = "►";
    private static final String PAUSE = "❚❚";

    private boolean paused;

    public PlayPauseButton()
    {
        super(PLAY);
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK), new EmptyBorder(2,4,2,4)));
        this.paused = true;
    }

    public boolean isPaused()
    {
        return this.paused;
    }

    public void pause() {
        paused = true;
        setText(PLAY);
    }

    public void play() {
        paused = false;
        setText(PAUSE);
    }
}
