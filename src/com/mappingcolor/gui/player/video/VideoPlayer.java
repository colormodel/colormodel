package com.mappingcolor.gui.player.video;

import javax.swing.*;

import com.mappingcolor.R;

public class VideoPlayer extends JPanel {
    public VideoPanel videoPanel;
    public ControlsPanel controlsPanel;

    public VideoPlayer() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        videoPanel = new VideoPanel(this);
        this.add(videoPanel);

        controlsPanel = new ControlsPanel(this);
        this.add(controlsPanel);
    }

    public void load() {
        controlsPanel.setScrubSliderMax(R.videoData.getVideoStreamData().getTotalFrames());
        controlsPanel.setEnableControls(true);
    }

    public void unload() {
        controlsPanel.setEnableControls(false);
    }

    public ControlsPanel getControlsPanel() {
        return controlsPanel;
    }
}
