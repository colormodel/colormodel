package com.mappingcolor.gui.player.video;

import com.mappingcolor.Main;
import com.mappingcolor.R;
import com.mappingcolor.model.ColorModel;
import com.xuggle.xuggler.IContainer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class ControlsPanel extends JPanel {
    private static final String STEP_BACKWARD = "<";
    private static final String STEP_FORWARD = ">";

    public PlayPauseButton playPauseButton;
    public JButton stepBackButton;
    public JButton stepForwardButton;
    public JSlider scrubSlider;

    private VideoPlayer videoPlayer;
    private Thread playThread = new Thread();
    private int seekType;

    public ControlsPanel(VideoPlayer videoPlayer) {
        this.videoPlayer = videoPlayer;

        buildControlsPanel();
        setEnableControls(false);
    }

    public void setScrubSliderMax(int max) {
        scrubSlider.setValue(0);
        scrubSlider.setMaximum(max);

        //Turn on labels at major tick marks.
        scrubSlider.setMajorTickSpacing(Math.round(max/2));
        scrubSlider.setMinorTickSpacing(Math.round(max/10));
        scrubSlider.setPaintTicks(true);
        scrubSlider.setPaintLabels(true);
    }

    public void setEnableControls(boolean enable) {
        playPauseButton.setEnabled(enable);
        stepBackButton.setEnabled(enable);
        stepForwardButton.setEnabled(enable);
        scrubSlider.setEnabled(enable);

        if(!enable) {
            scrubSlider.setPaintLabels(false);
            scrubSlider.setPaintTicks(false);
            scrubSlider.setValue(0);
        }
    }

    private void buildControlsPanel() {
        playPauseButton = new PlayPauseButton();
        playPauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!playPauseButton.isPaused()) {
                    //seekType = Demuxer.SeekFlag.SEEK_FRAME.swigValue();
                    seekType = IContainer.SEEK_FLAG_FRAME;
                    scrubSlider.setEnabled(true);
                    stepBackButton.setEnabled(true);
                    stepForwardButton.setEnabled(true);
                    playPauseButton.pause();
                }
                else {
                    //seekType = Demuxer.SeekFlag.SEEK_ANY.swigValue();
                    seekType = IContainer.SEEK_FLAG_ANY;
                    scrubSlider.setEnabled(false);
                    stepBackButton.setEnabled(false);
                    stepForwardButton.setEnabled(false);
                    playThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                while(scrubSlider.getValue() < scrubSlider.getMaximum() && !playPauseButton.isPaused()) {
                                    R.currentFrame.set(scrubSlider.getValue() + 1);
                                    scrubSlider.setValue(R.currentFrame.get());
                                    Thread.sleep(R.videoData.getVideoStreamData().getFrameTime());
                                }

                                playThread.interrupt();
                                scrubSlider.setEnabled(true);
                                stepBackButton.setEnabled(true);
                                stepForwardButton.setEnabled(true);
                                playPauseButton.pause();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    playThread.start();
                    playPauseButton.play();
                }
            }
        });

        stepBackButton = new JButton(STEP_BACKWARD);
        stepBackButton.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK), new EmptyBorder(2, 4, 2, 4)));
        stepBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(scrubSlider.getValue() > 0) {
                    R.currentFrame.set(Math.max(scrubSlider.getValue() - R.frameSkip, 0));
                    scrubSlider.setValue(R.currentFrame.get());
                }
            }
        });

        stepForwardButton = new JButton(STEP_FORWARD);
        stepForwardButton.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK), new EmptyBorder(2, 4, 2, 4)));
        stepForwardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(scrubSlider.getValue() < scrubSlider.getMaximum()) {
                    R.currentFrame.set(Math.min(scrubSlider.getValue() + R.frameSkip, R.videoData.getVideoStreamData().getTotalFrames()));
                    scrubSlider.setValue(R.currentFrame.get());
                }
            }
        });

        scrubSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 0);
        scrubSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (Main.view.videoProcessingPopup.exactLocationButton.isSelected())
                    ColorModel.mappingMethod = ColorModel.MappingMethod.EXACT_LOCATION;
                else if (Main.view.videoProcessingPopup.nearestNeighborButton.isSelected())
                    ColorModel.mappingMethod = ColorModel.MappingMethod.NEAREST_NEIGHBOR;

                if (R.videoData != null) {
                    R.currentFrame.set(scrubSlider.getValue());
                    R.videoData.getFrameAtTimeCode(R.currentFrame.get(), seekType, true);

                    BufferedImage image = R.videoData.getFrame();
                    if(image != null)
                        videoPlayer.videoPanel.setImage(image);

                    R.dataChanged = true;
                }
            }
        });

        JPanel left = new JPanel();
        left.add(playPauseButton);
        left.add(stepBackButton);
        left.add(stepForwardButton);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(scrubSlider, BorderLayout.CENTER);
        panel.add(left, BorderLayout.WEST);
        add(panel);
    }
}
