package com.mappingcolor.gui.player.video;

import com.xuggle.mediatool.IMediaReader;

import javax.swing.*;
import java.awt.*;

public class VideoPanel extends JPanel {
    private IMediaReader reader;
    private Image image;
    //private final MediaListenerAdapter adapter;
    private VideoPlayer videoPlayer;

    public VideoPanel(VideoPlayer videoPlayer) {
        this.videoPlayer = videoPlayer;
        this.setBackground(Color.BLACK);
        this.setMinimumSize(new Dimension(235, 185));
        this.setPreferredSize(new Dimension(235, 185));

        /*this.adapter = new MediaListenerAdapter() {

            @Override
            public void onVideoPicture(IVideoPictureEvent event) {
                setImage(event.getImage());
                try {
                    Thread.sleep((int) reader.getContainer().getStream(event.getStreamIndex()).getFrameRate().getDouble());
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        };*/
    }

    public void setImage(final Image image) {
        this.image = image;

        //SwingUtilities.invokeLater(new Runnable() {
        //    public void run() {
                repaint();
        //    }
        //});
    }

    @Override
    public synchronized void paint(Graphics g) {
        super.paint(g);
        if (image != null) {
            drawImage(image, g);
        }
    }

    private void drawImage(Image image, Graphics g)
    {
        int imgWidth = image.getWidth(null);
        int imgHeight = image.getHeight(null);

        double imgAspect = (double) imgHeight / imgWidth;

        int canvasWidth = this.getWidth();
        int canvasHeight = this.getHeight();

        double canvasAspect = (double) canvasHeight / canvasWidth;

        int x1 = 0; // top left X position
        int y1 = 0; // top left Y position
        int x2 = 0;	// bottom right X position
        int y2 = 0;	// bottom right Y position

        if (imgWidth < canvasWidth && imgHeight < canvasHeight) {
            // the image is smaller than the canvas
            x1 = (canvasWidth - imgWidth)  / 2;
            y1 = (canvasHeight - imgHeight) / 2;
            x2 = imgWidth + x1;
            y2 = imgHeight + y1;

        } else {
            if (canvasAspect > imgAspect) {
                y1 = canvasHeight;
                // keep image aspect ratio
                canvasHeight = (int) (canvasWidth * imgAspect);
                y1 = (y1 - canvasHeight) / 2;
            } else {
                x1 = canvasWidth;
                // keep image aspect ratio
                canvasWidth = (int) (canvasHeight / imgAspect);
                x1 = (x1 - canvasWidth) / 2;
            }
            x2 = canvasWidth + x1;
            y2 = canvasHeight + y1;
        }

        g.drawImage(image, x1, y1, x2, y2, 0, 0, imgWidth, imgHeight, null);
    }
}
