package com.mappingcolor.gui;

import javax.swing.*;
import java.awt.*;

public class LoadingScreen extends JWindow {
    private static final String LOADING_TEXT = " Loading ";
    private static final int LOADING_UPDATE = 333;

    private Thread updateLoading;
    private int loadingCounter = 1;
    private String loadingPeriods = "";

    public void showSplash() {
        JPanel content = (JPanel)getContentPane();
        content.setBackground(Color.white);
        ImageIcon imageIcon = new ImageIcon(this.getClass().getClassLoader().getResource("resources/images/color_model_logo.png"));

        int width = imageIcon.getIconWidth();
        int height = imageIcon.getIconHeight();
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screen.width - width) / 2;
        int y = (screen.height - height) / 2;
        setBounds(x, y, width, height);

        JLabel label = new JLabel(imageIcon);
        content.add(label, BorderLayout.CENTER);

        final JLabel initializing = new JLabel(LOADING_TEXT, JLabel.CENTER);
        initializing.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        content.add(initializing, BorderLayout.SOUTH);

        content.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));

        setVisible(true);

        updateLoading = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    loadingPeriods = "";
                    for(int i = 0; i <= loadingCounter % 5; i++)
                        loadingPeriods += ".";

                    initializing.setText(loadingPeriods + LOADING_TEXT + loadingPeriods);
                    loadingCounter++;

                    try {
                        Thread.sleep(LOADING_UPDATE);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        updateLoading.start();
    }

    public void hideSplash() {
        try {
            setVisible(false);
            updateLoading.join();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}