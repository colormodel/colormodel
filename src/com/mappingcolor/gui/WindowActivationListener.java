package com.mappingcolor.gui;

import com.mappingcolor.Main;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowActivationListener extends WindowAdapter
{
    private static final int REFOCUS_INTERVAL = 2000;
    private static long nextRefocus = System.currentTimeMillis();

    private Window owner;
    public WindowActivationListener(Window owner)
    {
        this.owner = owner;
    }

    public void windowActivated(WindowEvent e)
    {
        //System.out.println("TRY REFOCUS");
        if (System.currentTimeMillis() < nextRefocus) return;
        //System.out.println("REFOCUS");
        nextRefocus = System.currentTimeMillis() + REFOCUS_INTERVAL;
        Main.view.focusAllWindows();
        if (this.owner.isVisible()) this.owner.setVisible(true);
    }
}
