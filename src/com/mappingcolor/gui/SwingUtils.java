package com.mappingcolor.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Shaun on 2/12/2015.
 */
public class SwingUtils
{
    private SwingUtils() {}

    public static JPanel nest(JComponent component)
    {
        JPanel panel = new JPanel(new FlowLayout());
        panel.setOpaque(false);
        panel.add(component);
        return panel;
    }
}
