package com.mappingcolor.io.video;

import com.mappingcolor.Main;
import com.mappingcolor.R;
import com.mappingcolor.io.ColorMapData;
import com.mappingcolor.model.ColorModel;
import com.mappingcolor.model.MyColor;
import com.xuggle.xuggler.*;
import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.video.IConverter;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

public class VideoData extends ColorMapData {
    private Map<Integer, Integer> uniqueColorsCounts;
    private Map<Integer, Integer> processed;
    private ArrayList<MyColor> colorsNext;
    private BufferedImage frameNext;

    private long duration;
    private long bitRate;

    private AudioStreamInfo audioStreamData;
    private VideoStreamInfo videoStreamData;

    private IStreamCoder videoCoder;
    private IVideoResampler resampler;
    private IContainer videoContainer;
    private IPacket packet = IPacket.make();

    private int videoLoadResult;
    private int currentFrame = 0;

    public boolean mappingChanged = true;

    //xuggle 5.5 was built from sources from the below forum threads -
    //allows for xuggle 5.5 and not 5.4 (5.4 has crash issues on ivy bridge CPUs) and is the LGPL version.
    //for win x86, win x64, linux x86, linux x64
    //https://groups.google.com/forum/#!topic/xuggler-users/Nfnl-a0_z6I
    //http://stackoverflow.com/questions/23162574/how-can-i-build-the-gpl-and-lgpl-version-of-xuggle-xuggler
    //https://docs.google.com/document/d/1mjlI2jAGKK3_W4KIBjpUfM3F_SJjUkZhQJuwtIBn_7I/pub
    //http://superuser.com/questions/339537/where-can-i-get-the-repositories-for-old-ubuntu-versions
    //
    // for mac x86, mac x64
    //TODO build xuggle 5.5 for mac x86/64
    public VideoData(File file) {
        super(file);

        colors = new ArrayList<MyColor>();

        videoContainer = IContainer.make();
        videoLoadResult = videoContainer.open(file.getAbsolutePath(), IContainer.Type.READ, null);

        if(videoLoadResult > 0) {
            //fails if video stream is not found
            getVideoInfo(videoContainer);

            uniqueColorsCounts = new ConcurrentHashMap<Integer, Integer>(videoStreamData.getTotalFrames());
            processed = new ConcurrentHashMap<Integer, Integer>(videoStreamData.getTotalFrames());

            resampler = resampleIfNeeded(videoCoder);
        }
    }

    public long getDuration() {
        return duration;
    }

    public long getBitRate() {
        return bitRate;
    }

    public ArrayList<MyColor> getColorsAt(int i) {
        if(i == currentFrame)
            return colors;

        ArrayList<MyColor> temp = loadTempFile(i);
        if(temp != null)
            return temp;

        getFrameAtTimeCode(i, IContainer.SEEK_FLAG_ANY, false);

        return colors;
    }

    public AudioStreamInfo getAudioStreamData() {
        return audioStreamData;
    }

    public VideoStreamInfo getVideoStreamData() {
        return videoStreamData;
    }

    public void getFrameAtTimeCode(final int frame, final int seekType, boolean processNextFrame) {
        if(ColorModel.mappingMethod == ColorModel.MappingMethod.NEAREST_NEIGHBOR && R.countThreshold != ColorModel.nearestCountThreshold
                || ColorModel.mappingMethod == ColorModel.MappingMethod.EXACT_LOCATION && R.countThreshold != ColorModel.exactCountThreshold)
            processed.clear();

        /*if(processNextFrame && colorsNext != null) {
            colors = new ArrayList<MyColor>(colorsNext);
            colorsNext = null;

            return this.frameNext;
        }*/

        videoContainer.seekKeyFrame(videoStreamData.getIndex(), -1, 0);
        videoContainer.seekKeyFrame(videoStreamData.getIndex(), videoContainer.getStartTime() + frame, seekType);

        if(videoContainer.readNextPacket(packet) >= 0) {
            if (packet.getStreamIndex() == videoStreamData.getIndex()) {
                IVideoPicture picture = IVideoPicture.make(videoCoder.getPixelType(), videoCoder.getWidth(), videoCoder.getHeight());

                int offset = 0;
                while (offset < packet.getSize()) {
                    int bytesDecoded = videoCoder.decodeVideo(picture, packet, offset);
                    if (bytesDecoded < 0)
                        throw new RuntimeException("got error decoding video in: " + fileName);

                    offset += bytesDecoded;

                    if (picture.isComplete()) {
                        IVideoPicture newPic = picture;

                        if (resampler != null) {
                            newPic = IVideoPicture.make(resampler.getOutputPixelFormat(), picture.getWidth(), picture.getHeight());

                            if (resampler.resample(newPic, picture) < 0)
                                throw new RuntimeException("could not resampler-sample video from: " + fileName);
                        }

                        if (newPic.getPixelType() != IPixelFormat.Type.BGR24)
                            throw new RuntimeException("could not decode video as BGR 24 bit data in: " + fileName);

                        IConverter iConverter = ConverterFactory.createConverter(ConverterFactory.XUGGLER_BGR_24, newPic);
                        final BufferedImage image = iConverter.toImage(newPic);

                        if(frame % R.frameSkip == 0 ||
                                Main.view.videoProcessingPopup.videoPlayer.getControlsPanel().playPauseButton.isPaused()) {

                            if(mappingChanged || processed.get(frame) == null) {
                                if(mappingChanged)
                                    processed.clear();

                                processed.put(frame, frame);

                                Map<Integer, MyColor> colorMap = createColorMapParallel(image);

                                uniqueColorsCounts.put(frame, colorMap.size());

                                if(processNextFrame)
                                    colors = new ArrayList<MyColor>(colorMap.values());
                                else
                                    colorsNext = new ArrayList<MyColor>(colorMap.values());

                                final String fileName = getTempFileName(frame);
                                if(fileName != null) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            writeTempFile(fileName, colors);
                                        }
                                    }).start();
                                }

                                mappingChanged = false;
                            }

                            /*final int nextFrame = frame + R.frameSkip;
                            if(processed.get(nextFrame) == null && processNextFrame) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        frameNext = getFrameAtTimeCode(nextFrame, seekType, false);
                                        //System.out.println("Run " + nextFrame);
                                    }
                                }).start();
                            }*/
                        }

                        //System.out.println("No Run " + frame);

                        currentFrame = frame;

                        this.frame = image;
                    }
                }
            }
        }
    }

    private IVideoResampler resampleIfNeeded(IStreamCoder videoCoder) {
        IVideoResampler resampler = null;
        if (videoCoder.getPixelType() != IPixelFormat.Type.BGR24) {
            // if this stream is not in BGR24, we'resampler going to need to
            // convert it.  The VideoResampler does that for us.
            resampler = IVideoResampler.make(
                    videoCoder.getWidth(), videoCoder.getHeight(), IPixelFormat.Type.BGR24,
                    videoCoder.getWidth(), videoCoder.getHeight(), videoCoder.getPixelType());

            if (resampler == null)
                throw new RuntimeException("could not create color space resampler for: " + fileName);
        }

        return resampler;
    }

    //assumes only one audio channel and one video channel
    public void getVideoInfo(IContainer video) {
        duration = video.getDuration();
        bitRate = video.getBitRate();

        for (int i = 0; i < video.getNumStreams(); i++) {
            IStream stream = video.getStream(i);
            IStreamCoder coder = stream.getStreamCoder();

            if(coder != null) {
                if (coder.getCodecType() == ICodec.Type.CODEC_TYPE_AUDIO) {
                    audioStreamData = new AudioStreamInfo(i, coder);
                } else if (coder.getCodecType() == ICodec.Type.CODEC_TYPE_VIDEO) {
                    videoStreamData = new VideoStreamInfo(i, coder, (int) stream.getNumFrames());
                    videoCoder = coder;
                }
            }
        }

        if (videoStreamData == null)
            throw new RuntimeException("could not find video stream in container: " + fileName);

        if (videoCoder.open(null, null) < 0)
            throw new RuntimeException("could not open video decoder for container: " + fileName);
    }

    public String getTempFileName(int frame) {
        String file = R.tempDir + File.separator + fileName + "_" + fileExtension + "_" + ColorModel.mappingMethod + "_"
                + R.countThreshold + "_" + frame + ".dat";

        if(!new File(file).exists())
            return file;

        return null;
    }

    private void writeTempFile(String fileName, ArrayList<MyColor> colors) {
        System.out.println(fileName);

        StringBuilder stringBuilder = new StringBuilder();

        for(MyColor color : colors)
            stringBuilder.append(color.getValue()).append(" ").append(color.getChroma())
                    .append(" ").append(color.getHue()).append("\n");

        byte[] buffer = stringBuilder.toString().getBytes();

        try {
            FileChannel rwChannel = new RandomAccessFile(fileName, "rw").getChannel();
            ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, buffer.length);

            wrBuf.put(buffer);

            rwChannel.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<MyColor> loadTempFile(int i) {
        String file = R.tempDir + File.separator + fileName + "_" + fileExtension + "_" + ColorModel.mappingMethod + "_"
                + R.countThreshold + "_" + frame + ".dat";

        if(!new File(file).exists())
            return null;

        ArrayList<MyColor> fileColors = new ArrayList<MyColor>();

        FileInputStream inputStream = null;
        Scanner sc = null;
        try {

            inputStream = new FileInputStream(file);
            sc = new Scanner(inputStream, "UTF-8");
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] values = line.split(" ");
                fileColors.add(MyColor.fromVCH(Integer.parseInt(values[0]), Integer.parseInt(values[1]), Integer.parseInt(values[2])));
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (sc != null) {
                    sc.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fileColors;
    }
}
