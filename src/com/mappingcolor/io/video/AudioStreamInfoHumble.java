package com.mappingcolor.io.video;

import io.humble.video.AudioFormat;
import io.humble.video.Decoder;

public class AudioStreamInfoHumble {
    private int index;
    private int sampleRate;
    private int channels;
    private AudioFormat.Type sampleFormat;

    public AudioStreamInfoHumble(int index, Decoder decoder) {
        this.index = index;
        sampleRate = decoder.getSampleRate();
        channels = decoder.getChannels();
        sampleFormat = decoder.getSampleFormat();
    }

    public int getIndex() {
        return index;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public int getChannels() {
        return channels;
    }

    public AudioFormat.Type getSampleFormat() {
        return sampleFormat;
    }

    @Override
    public String toString() {
        return "Audio{" +
                "sampleRate=" + sampleRate +
                ", channels=" + channels +
                ", sampleFormat=" + sampleFormat +
                '}';
    }
}
