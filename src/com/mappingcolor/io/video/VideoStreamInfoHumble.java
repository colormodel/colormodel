package com.mappingcolor.io.video;

import io.humble.video.Decoder;
import io.humble.video.PixelFormat;

public class VideoStreamInfoHumble {
    private int index;
    private int width;
    private int height;
    private PixelFormat.Type pixelType;
    private double frameRate;
    private int frameTime;
    private int totalFrames;
    private int pixelCount;

    public VideoStreamInfoHumble(int index, Decoder decoder, double frameRate, long duration) {
        this.index = index;
        width = decoder.getWidth();
        height = decoder.getHeight();
        pixelType = decoder.getPixelFormat();
        this.frameRate = frameRate;
        frameTime = (int) Math.round(1000 / frameRate);
        pixelCount = width * height;
        totalFrames = (int) Math.floor(duration / (frameTime * 1000));
    }

    public int getIndex() {
        return index;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public PixelFormat.Type getPixelType() {
        return pixelType;
    }

    public double getFrameRate() {
        return frameRate;
    }

    public int getFrameTime() {
        return frameTime;
    }

    public int getPixelCount() {
        return pixelCount;
    }

    public int getTotalFrames() {
        return totalFrames;
    }

    @Override
    public String toString() {
        return "VideoStreamInfo{" +
                "index=" + index +
                ", width=" + width +
                ", height=" + height +
                ", pixelType=" + pixelType +
                ", frameRate=" + frameRate +
                ", frameTime=" + frameTime +
                ", totalFrames=" + totalFrames +
                ", pixelCount=" + pixelCount +
                '}';
    }
}
