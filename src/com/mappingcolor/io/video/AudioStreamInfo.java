package com.mappingcolor.io.video;

import com.xuggle.xuggler.IAudioSamples;
import com.xuggle.xuggler.IStreamCoder;

public class AudioStreamInfo {
    private int index;
    private int sampleRate;
    private int channels;
    private IAudioSamples.Format sampleFormat;

    public AudioStreamInfo(int index, IStreamCoder coder) {
        this.index = index;
        sampleRate = coder.getSampleRate();
        channels = coder.getChannels();
        sampleFormat = coder.getSampleFormat();
    }

    public int getIndex() {
        return index;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public int getChannels() {
        return channels;
    }

    public IAudioSamples.Format getSampleFormat() {
        return sampleFormat;
    }

    @Override
    public String toString() {
        return "Audio{" +
                "sampleRate=" + sampleRate +
                ", channels=" + channels +
                ", sampleFormat=" + sampleFormat +
                '}';
    }
}
