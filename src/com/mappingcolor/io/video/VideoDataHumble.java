package com.mappingcolor.io.video;

import com.mappingcolor.Main;
import com.mappingcolor.R;
import com.mappingcolor.io.ColorMapData;
import com.mappingcolor.model.MyColor;
import io.humble.video.*;
import io.humble.video.awt.MediaPictureConverter;
import io.humble.video.awt.MediaPictureConverterFactory;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VideoDataHumble extends ColorMapData {
    private HashMap<Integer, Integer> uniqueColorsCounts;
    private HashMap<Integer, ArrayList<MyColor>> colorsList;

    private long duration;
    private long bitRate;
    private long startTime;

    private AudioStreamInfoHumble audioStreamData;
    private VideoStreamInfoHumble videoStreamData;

    private Decoder videoDecoder;
    private MediaPacket packet = MediaPacket.make();

    private Demuxer demuxer = Demuxer.make();

    protected BufferedImage frame;

    public boolean mappingChanged = true;

    public VideoDataHumble(File file) {
        super(file);

        try {
            demuxer.open(fileName, null, false, true, null, null);
            getVideoInfo();

            uniqueColorsCounts = new HashMap<Integer, Integer>(videoStreamData.getTotalFrames());
            colorsList = new HashMap<Integer, ArrayList<MyColor>>(videoStreamData.getTotalFrames());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<MyColor> getColors(int i) {
        return colorsList.get(i);
    }

    public long getDuration() {
        return duration;
    }

    public long getBitRate() {
        return bitRate;
    }

    public AudioStreamInfoHumble getAudioStreamData() {
        return audioStreamData;
    }

    public VideoStreamInfoHumble getVideoStreamData() {
        return videoStreamData;
    }

    synchronized public BufferedImage getTimecode(int frame, int seekType) {
        try {
            demuxer.seek(videoStreamData.getIndex(), -1, -1, -1, 0);
            demuxer.seek(videoStreamData.getIndex(), frame, frame, frame, seekType);

            if (demuxer.read(packet) >= 0) {
                if (packet.getStreamIndex() == videoStreamData.getIndex()) {
                    MediaPicture picture = MediaPicture.make(videoDecoder.getWidth(), videoDecoder.getHeight(), videoDecoder.getPixelFormat());
                    MediaPictureConverter converter = MediaPictureConverterFactory.createConverter(MediaPictureConverterFactory.HUMBLE_BGR_24, picture);

                    int offset = 0;
                    int bytesRead = 0;
                    do {
                        bytesRead += videoDecoder.decode(picture, packet, offset);
                        if (picture.isComplete()) {
                            BufferedImage image = null;

                            image = converter.toImage(image, picture);

                            if (frame % R.frameSkip == 0 ||
                                    Main.view.videoProcessingPopup.videoPlayer.getControlsPanel().playPauseButton.isPaused()) {

                                if (mappingChanged || colorsList.get(frame) == null) {
                                    Map<Integer, MyColor> colorMap = createColorMapParallel(image);
                                    uniqueColorsCounts.put(frame, colorMap.size());
                                    colorsList.put(frame, new ArrayList<MyColor>(colorMap.values()));
                                }

                                colors = colorsList.get(frame);
                                this.frame = image;
                            }

                            return image;
                        }
                        offset += bytesRead;
                    } while (offset < packet.getSize());
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    //assumes only one audio channel and one video channel
    public void getVideoInfo() {
        duration = demuxer.getDuration();
        bitRate = demuxer.getBitRate();
        startTime = demuxer.getStartTime();

        int streams;
        try {
            streams = demuxer.getNumStreams();
            for (int i = 0; i < streams; i++) {
                DemuxerStream stream = demuxer.getStream(i);
                Decoder decoder = stream.getDecoder();

                if(decoder != null) {
                    if (decoder.getCodecType() == MediaDescriptor.Type.MEDIA_AUDIO) {
                        audioStreamData = new AudioStreamInfoHumble(i, decoder);
                    } else if (decoder.getCodecType() == MediaDescriptor.Type.MEDIA_VIDEO) {
                        videoStreamData = new VideoStreamInfoHumble(i, decoder, stream.getFrameRate().getDouble(), duration);
                        videoDecoder = decoder;
                    }
                }
            }

            if (videoStreamData == null)
                throw new RuntimeException("could not find video stream in container: " + fileName);

            videoDecoder.open(null, null);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
