package com.mappingcolor.io.video;

import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IStreamCoder;

public class VideoStreamInfo {
    private int index;
    private int width;
    private int height;
    private IPixelFormat.Type pixelType;
    private double frameRate;
    private int frameTime;
    private int totalFrames;
    private int pixelCount;

    public VideoStreamInfo(int index, IStreamCoder coder, int totalFrames) {
        this.index = index;
        width = coder.getWidth();
        height = coder.getHeight();
        pixelType = coder.getPixelType();
        frameRate = coder.getFrameRate().getDouble();
        frameTime = (int) Math.round(1000 / frameRate);
        pixelCount = width * height;
        this.totalFrames = totalFrames;
    }

    public int getIndex() {
        return index;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public IPixelFormat.Type getPixelType() {
        return pixelType;
    }

    public double getFrameRate() {
        return frameRate;
    }

    public int getFrameTime() {
        return frameTime;
    }

    public int getPixelCount() {
        return pixelCount;
    }

    public int getTotalFrames() {
        return totalFrames;
    }

    @Override
    public String toString() {
        return "VideoStreamInfo{" +
                "index=" + index +
                ", width=" + width +
                ", height=" + height +
                ", pixelType=" + pixelType +
                ", frameRate=" + frameRate +
                ", frameTime=" + frameTime +
                ", totalFrames=" + totalFrames +
                ", pixelCount=" + pixelCount +
                '}';
    }
}
