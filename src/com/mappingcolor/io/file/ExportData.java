package com.mappingcolor.io.file;

import com.mappingcolor.R;
import com.mappingcolor.io.ColorMapData;
import com.mappingcolor.io.image.ImageData;
import com.mappingcolor.io.video.VideoData;
import com.mappingcolor.model.ColorModel;
import com.mappingcolor.model.MyColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

public class ExportData {
    public static final String EXCEL_EXTENSION = ".xlsx";
    public static final String TEXT_EXTENSION = ".txt";
    public static final String CSV_EXTENSION = ".csv";

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.0");

    private ExportData() {}

    public static void writeToFile(String outputFileName, MyColor.ModelType colorModelType, int countThreshold,
                                   ColorMapData data, boolean visibleData, int begin, int end)
    {
        ArrayList<MyColor> C = null;
        if(!(data instanceof VideoData)) {
            C = data.getColors();
            if (countThreshold > 0)
                C = getFilteredColors(data.getColors(), countThreshold, visibleData);

            sortByOccurrence(C);
        }

        String[] headerData;
        int c = 0;
        if(data instanceof ImageData)
        {
            ImageData imageData = (ImageData)data;
            headerData = new String[5];
            headerData[c++] = "Image file name:           " + imageData.getFileName();
            headerData[c++] = "Number of pixels:          " + imageData.getPixelCount();
        }
        else
        {
            headerData = new String[4];
            headerData[c++] = "File name:           " + data.getFileName();
        }

        headerData[c++] = "Number of unique colors:   " + data.getColors().size();
        headerData[c++] = "Occurrence threshold:      " + countThreshold;
        if(!(data instanceof VideoData))
            headerData[c] = "Colors above threshold:    " + C.size();

        if(R.imageData != null) {
            if (outputFileName.contains(CSV_EXTENSION) || outputFileName.contains(TEXT_EXTENSION))
                writeInCSVFormat(outputFileName, colorModelType, C, headerData);
            else
                writeToExcelFile(outputFileName, colorModelType, C, headerData);
        }
        else if (R.videoData != null) {
            if (outputFileName.contains(CSV_EXTENSION) || outputFileName.contains(TEXT_EXTENSION))
                writeVideoInCSVFormat(outputFileName, colorModelType, (VideoData) data, headerData, countThreshold, visibleData, begin, end);
            //else
            //    writeVideoToExcelFile(outputFileName, colorModelType, C, headerData, begin, end);
        }
    }

    private static void writeInCSVFormat(String outputFileName, MyColor.ModelType colorModelType, ArrayList<MyColor> colors, String[] headerData)
    {
        try
        {
            FileWriter fileWriter = new FileWriter(outputFileName);
            BufferedWriter out = new BufferedWriter(fileWriter);

            for (String d : headerData)
                out.write(d + "\n");

            String colorFormat = getColorFormat(colorModelType);
            String attributeRow = colorFormat.charAt(0) + "," + colorFormat.charAt(1) + "," + colorFormat.charAt(2) + ",Count,Name";
            out.write("\n" + attributeRow + "\n");

            for (MyColor color : colors) {
                String[] rowData = getData(color, colorModelType);

                String output = rowData[0];
                for(int i = 1; i < rowData.length; i++)
                    output += ", " + rowData[i];

                out.write(output + "\n");
            }

            out.close();
            fileWriter.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void writeToExcelFile(String outputFileName, MyColor.ModelType colorModelType, ArrayList<MyColor> colors, String[] headerData)
    {
        try {
            String sheetName = "Sheet1";//name of sheet

            int r = 0;
            XSSFWorkbook wb = new XSSFWorkbook();
            XSSFSheet sheet = wb.createSheet(sheetName);
            for(; r < headerData.length; r++)
                sheet.createRow(r).createCell(0).setCellValue(headerData[r]);

            r++;
            XSSFRow row = sheet.createRow(r++);
            String colorFormat = getColorFormat(colorModelType);
            row.createCell(0).setCellValue(colorFormat.charAt(0) + "");
            row.createCell(1).setCellValue(colorFormat.charAt(1) + "");
            row.createCell(2).setCellValue(colorFormat.charAt(2) + "");
            row.createCell(3).setCellValue("Count");
            row.createCell(4).setCellValue("Name");

            for (int i = 0; i < colors.size(); i++) {
                row = sheet.createRow(i + r);
                MyColor color = colors.get(i);
                String[] data = getData(color, colorModelType);

                // must convert String to double before entering value in excel file (or will cause problems)
                row.createCell(0).setCellValue(Double.parseDouble(data[0]));
                row.createCell(1).setCellValue(Double.parseDouble(data[1]));
                row.createCell(2).setCellValue(Double.parseDouble(data[2]));
                row.createCell(3).setCellValue(Integer.parseInt(data[3]));
                if(data[4].length() > 0)
                    row.createCell(4).setCellValue(data[4]);
            }

            FileOutputStream fileOut = new FileOutputStream(outputFileName);

            //write this workbook to an Outputstream.
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void writeVideoInCSVFormat(String outputFileName, MyColor.ModelType colorModelType, VideoData data, String[] headerData, int countThreshold, boolean visibleData, int begin, int end)
    {
        try
        {
            FileWriter fileWriter = new FileWriter(outputFileName);
            BufferedWriter out = new BufferedWriter(fileWriter);

            for (String d : headerData)
                out.write(d + "\n");

            String colorFormat = getColorFormat(colorModelType);

            for(int i = begin; i <= end; i += R.frameSkip) {
                ArrayList<MyColor> colors = data.getColorsAt(i);
                if (countThreshold > 0)
                    colors = getFilteredColors(data.getColors(), countThreshold, visibleData);

                sortByOccurrence(colors);

                String attributeRow = "Frame:    " + i + "\n" + "Colors above threshold:    " + colors.size();
                out.write("\n" + attributeRow + "\n");

                StringBuilder output1 = new StringBuilder().append(colorFormat.charAt(0)).append(", ");
                StringBuilder output2 = new StringBuilder().append(colorFormat.charAt(1)).append(", ");
                StringBuilder output3 = new StringBuilder().append(colorFormat.charAt(2)).append(", ");
                StringBuilder output4 = new StringBuilder().append("Count, ");
                StringBuilder output5 = new StringBuilder().append("Name, ");

                for (MyColor color : colors) {
                    String[] rowData = getData(color, colorModelType);

                    output1.append(rowData[0]).append(", ");
                    output2.append(rowData[1]).append(", ");
                    output3.append(rowData[2]).append(", ");
                    output4.append(rowData[3]).append(", ");
                    output5.append(rowData[4]).append(", ");
                }

                out.write(output1 + "\n");
                out.write(output2 + "\n");
                out.write(output3 + "\n");
                out.write(output4 + "\n");
                out.write(output5 + "\n");
            }

            out.close();
            fileWriter.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static ArrayList<MyColor> getFilteredColors(ArrayList<MyColor> colors, int countThreshold, boolean visibleData)
    {
        ArrayList<MyColor> filteredColors = new ArrayList<MyColor>(colors.size());
        for (MyColor color : colors)
            if (color.getCount() > countThreshold) {
                if (visibleData) {
                    if (ColorModel.withinSliderBounds(color))
                        filteredColors.add(color);
                } else
                    filteredColors.add(color);
            }

        return filteredColors;
    }

    private static void sortByOccurrence(ArrayList<MyColor> C)
    {
        Collections.sort(C, MyColor.getCountComparator());
        Collections.reverse(C);
    }

    private static String[] getData(MyColor color, MyColor.ModelType colorModelType)
    {
        String[] data = new String[5];

        if (colorModelType == MyColor.ModelType.VCH)
        {
            data[0] = DECIMAL_FORMAT.format(color.getValue());
            data[1] = DECIMAL_FORMAT.format(color.getChroma());
            data[2] = DECIMAL_FORMAT.format(color.getHue());
        }
        else if (colorModelType == MyColor.ModelType.LAB)
        {
            data[0] = DECIMAL_FORMAT.format(color.getL());
            data[1] = DECIMAL_FORMAT.format(color.getA());
            data[2] = DECIMAL_FORMAT.format(color.getB());
        }
        else
        {
            data[0] = DECIMAL_FORMAT.format(color.getRed());
            data[1] = DECIMAL_FORMAT.format(color.getGreen());
            data[2] = DECIMAL_FORMAT.format(color.getBlue());
        }

        data[3] = color.getCount() + "";
        data[4] = color.getName();

        return data;
    }

    private static String getColorFormat(MyColor.ModelType colorModelType)
    {
        if (colorModelType == MyColor.ModelType.VCH)
            return "VCH";
        else if (colorModelType == MyColor.ModelType.LAB)
            return "LAB";
        else
            return "RGB";
    }
}