package com.mappingcolor.io.file;

import com.mappingcolor.R;

import com.mappingcolor.io.ColorMapData;
import com.mappingcolor.model.MyColor;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.*;

public class FileData extends ColorMapData
{
    public FileData(File file) {
        super(file);

        /*String[] temp = fileName.split("\\.");
        String format = temp[temp.length-1];

        if(format.equalsIgnoreCase("csv") || format.equalsIgnoreCase("txt"))
            colors = scanCSVFile(file);
        else if(format.equalsIgnoreCase("xlsx"))
            colors = scanExcelFile(file);
        else
            colors = null;*/

        if(fileExtension.equalsIgnoreCase("csv") || fileExtension.equalsIgnoreCase("txt"))
            colors = scanCSVFile(file);
        else if(fileExtension.equalsIgnoreCase("xlsx"))
            colors = scanExcelFile(file);
        else
            colors = null;
    }

    public FileData(String fileName) {
        this(new File(fileName));
    }

    private ArrayList<MyColor> scanCSVFile(File file)
    {
        ArrayList<MyColor> colors = new ArrayList<MyColor>(1000);
        BufferedReader bufferedReader = null;
        try
        {
            bufferedReader = new BufferedReader(new FileReader(file));
            String line;

            while ((line = bufferedReader.readLine()) != null)
            {
                String[] row = line.split(",");
                for(int i = 0; i < row.length; i++)
                    row[i] = row[i].trim();

                if(rowContainsData(row))
                {
                    float f1 = Float.parseFloat(row[0]);
                    float f2 = Float.parseFloat(row[1]);
                    float f3 = Float.parseFloat(row[2]);

                    MyColor color;
                    if (R.pigmentColorModelType == MyColor.ModelType.VCH)
                        color = MyColor.fromVCH(f1, f2, f3);
                    else //(R.pigmentColorModelType == MyColor.ModelType.LAB)
                        color = MyColor.fromLAB(f1, f2, f3);

                    int col = 3;
                    if (row.length > col)
                        if(isNumber(row[col])) {
                            color.setCount(Integer.parseInt(row[col]));
                            col++;
                        }

                    if (row.length > col)
                        color.setName(row[col].trim());

                    colors.add(color);
                }
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (bufferedReader != null)
                    bufferedReader.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }

            return colors;
        }
    }

    private ArrayList<MyColor> scanExcelFile(File file)
    {
        ArrayList<MyColor> colors = new ArrayList<MyColor>(1000);
        BufferedInputStream bufferedInputStream = null;

        try
        {
            bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            XSSFWorkbook workbook = new XSSFWorkbook(bufferedInputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            int rows = sheet.getPhysicalNumberOfRows();

            for(int r = 0; r < rows; r++)
            {
                XSSFRow row = sheet.getRow(r);

                if(rowContainsData(row))
                {
                    float f1 = (float) row.getCell(0).getNumericCellValue();
                    float f2 = (float) row.getCell(1).getNumericCellValue();
                    float f3 = (float) row.getCell(2).getNumericCellValue();

                    MyColor color;
                    if (R.pigmentColorModelType == MyColor.ModelType.VCH)
                        color = MyColor.fromVCH(f1, f2, f3);
                    else //(R.pigmentColorModelType == MyColor.ModelType.LAB)
                        color = MyColor.fromLAB(f1, f2, f3);

                    int col = 3;
                    XSSFCell cell = row.getCell(col, XSSFRow.RETURN_BLANK_AS_NULL);
                    if (cell != null) {
                        int cellType = cell.getCellType();
                        if(cellType == XSSFCell.CELL_TYPE_NUMERIC) {
                            color.setCount((int) cell.getNumericCellValue());
                            col++;
                        }
                    }

                    cell = row.getCell(col, XSSFRow.RETURN_BLANK_AS_NULL);
                    if (cell != null)
                        color.setName(cell.getStringCellValue());

                    colors.add(color);
                }
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (bufferedInputStream != null)
                    bufferedInputStream.close(); // throws UnhandledFileException
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return colors;
        }
    }

    private boolean rowContainsData(String[] row)
    {
        if(row.length < 3)
            return false;

        for(int i = 0; i < 3; i++)
            if (!isNumber(row[i]))
                return false;

        return true;
    }

    private boolean rowContainsData(XSSFRow row)
    {
        if(row == null)
            return false;

        for(int i = 0; i < 3; i++) {
            XSSFCell cell = row.getCell(i, XSSFRow.RETURN_BLANK_AS_NULL);
            if(cell != null){
                int cellType = row.getCell(i).getCellType();
                if(cellType != XSSFCell.CELL_TYPE_NUMERIC)
                    return false;
            }
            else
                return false;
        }

        return true;
    }

    private boolean isNumber(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}
