package com.mappingcolor.io.image;

import com.mappingcolor.R;
import com.mappingcolor.io.ColorMapData;
import com.mappingcolor.model.MyColor;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class ImageData extends ColorMapData {
    private Map<Integer, MyColor> colorMap;

    private int pixelCount;
    private int uniqueColorsCount;

    public ImageData(File file) {
        super(file);

        try {
            frame = ImageIO.read(file);

            this.pixelCount = frame.getHeight() * frame.getWidth();
            this.colorMap = createColorMapParallel(frame);
            this.uniqueColorsCount = colorMap.size();
            this.colors = new ArrayList<MyColor>(colorMap.values());

            R.currentFrame.set(0);
            R.frameSkip = 1;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<Integer, MyColor> getColorMap() {
        return colorMap;
    }

    public int getPixelCount() {
        return pixelCount;
    }

    public int getUniqueColorsCount() {
        return uniqueColorsCount;
    }
}