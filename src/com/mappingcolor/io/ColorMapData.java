package com.mappingcolor.io;

import com.mappingcolor.R;
import com.mappingcolor.model.MyColor;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.*;

public class ColorMapData {
    protected String fileName;
    protected String fileExtension;
    protected ArrayList<MyColor> colors;
    protected BufferedImage frame;

    public ColorMapData(File file) {
        String filePath = file.getAbsolutePath();
        int beginFileName = filePath.lastIndexOf(File.separator);
        String fileName = filePath.substring(beginFileName + 1, filePath.length());
        int beginExtension = fileName.lastIndexOf('.');
        this.fileName = fileName.substring(0, beginExtension);
        this.fileExtension = fileName.substring(beginExtension + 1, fileName.length());
    }

    /*public ColorMapData(File file) {
        String filePath = file.getAbsolutePath();
        int beginFileName = filePath.lastIndexOf(File.separator);
        String fileName = filePath.substring(beginFileName + 1, filePath.length());
        int beginExtension = fileName.lastIndexOf('.');
        this.fileName = fileName.substring(0, beginExtension);
        this.fileExtension = fileName.substring(beginExtension + 1, fileName.length());

        colors = new ArrayList<MyColor>();

        FileInputStream inputStream = null;
        Scanner sc = null;
        try {

            inputStream = new FileInputStream(file);
            sc = new Scanner(inputStream, "UTF-8");
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] values = line.split(" ");
                colors.add(MyColor.fromVCH(Integer.parseInt(values[0]), Integer.parseInt(values[1]), Integer.parseInt(values[2])));
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (sc != null) {
                    sc.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/

    public String getFileName() {
        return fileName + "." + fileExtension;
    }

    public ArrayList<MyColor> getColors() {
        return colors;
    }

    public void setColors(ArrayList<MyColor> colors) {
        this.colors = colors;
    }

    public BufferedImage getFrame() {
        return frame;
    }

    public Map<Integer, MyColor> createColorMap(BufferedImage image) {
        Map<Integer, MyColor> colorMap = new HashMap<Integer, MyColor>(1000);

        for (int y = 0; y < image.getHeight(); y++)
            for (int x = 0; x < image.getWidth(); x++) {
                int c = image.getRGB(x,y);

                if(!colorMap.containsKey(c)) {
                    MyColor myColor = MyColor.fromColor(new Color(c));
                    colorMap.put(c, myColor);
                }
                else
                    colorMap.get(c).incrementCount(); // counts the re-occurrence of a specific color
            }

        return colorMap;
    }

    public Map<Integer, MyColor> createColorMapParallel(BufferedImage image) {
        ExecutorService threadPool = Executors.newFixedThreadPool((int) R.processor_cores);
        CompletionService<String> pool = new ExecutorCompletionService<String>(threadPool);
        ConcurrentHashMap<Integer, MyColor> colorMap = new ConcurrentHashMap<Integer, MyColor>(1000);
        int chunk = (int) Math.ceil(image.getHeight() / R.processor_cores);

        int i;
        int nextChunk;
        for(i = 0; i < image.getHeight() - chunk; i = nextChunk + 1) {
            nextChunk = i + chunk;
            pool.submit(new CreateColorMapTask(image, colorMap, i, nextChunk));
        }
        pool.submit(new CreateColorMapTask(image, colorMap, i, image.getHeight() - 1));


        for(i = 0; i < R.processor_cores; i++)
            try {
                pool.take().get();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            catch (ExecutionException e) {
                e.printStackTrace();
            }

        threadPool.shutdown();

        return colorMap;
    }

    private final class CreateColorMapTask implements Callable<String> {
        private BufferedImage image;
        private ConcurrentHashMap<Integer, MyColor> colorMap;
        private int begin;
        private int end;

        public CreateColorMapTask(BufferedImage image, ConcurrentHashMap<Integer, MyColor> colorMap, int begin, int end) {
            this.image = image;
            this.colorMap = colorMap;
            this.begin = begin;
            this.end = end;
        }

        public String call(){
            for (int y = begin; y <= end; y++)
                for (int x = 0; x < image.getWidth(); x++) {
                    int c = image.getRGB(x,y);

                    if(!colorMap.containsKey(c)) {
                        MyColor myColor = MyColor.fromColor(new Color(c));
                        colorMap.put(c, myColor);
                    }
                    else
                        colorMap.get(c).incrementCount(); // counts the re-occurrence of a specific color
                }

            return "Run";
        }
    }
}
