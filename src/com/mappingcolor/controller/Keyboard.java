package com.mappingcolor.controller;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;
import com.mappingcolor.Main;
import com.mappingcolor.R;
import com.mappingcolor.model.Camera;
import com.mappingcolor.model.ColorModel;
import com.mappingcolor.model.Renderer;

import java.util.HashMap;

public class Keyboard implements KeyListener {
    private static HashMap<Short, Boolean> pressed = new HashMap<Short, Boolean>();

    public static boolean isKeyPressed() {
        return pressed.size() > 0;
    }

    public static boolean isAltKeyPressed() {
        return isPressed(KeyEvent.VK_ALT);
    }

    public static boolean isCtrlKeyPressed() {
        return isPressed(KeyEvent.VK_CONTROL);
    }

    public static boolean isUpPressed() {
        return isPressed(KeyEvent.VK_UP);
    }

    public static boolean isRightPressed() {
        return isPressed(KeyEvent.VK_RIGHT);
    }

    public static boolean isDownPressed() {
        return isPressed(KeyEvent.VK_DOWN);
    }

    public static boolean isLeftPressed() {
        return isPressed(KeyEvent.VK_LEFT);
    }

    public static boolean isPressed(short key) {
        return pressed.get(key) != null;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if( e.isAutoRepeat() )
            return;

        pressed.put(e.getKeyCode(), true);
        R.selectColorFlag = false;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if( e.isAutoRepeat() )
            return;

        pressed.remove(e.getKeyCode());

        if(e.isControlDown()) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_F:
                    Renderer.clearColorTrackerLeft();
                    break;
                case KeyEvent.VK_G:
                    Renderer.clearColorTrackerRight();
                    break;
                case KeyEvent.VK_H:
                    Renderer.clearColorTrackers();
                    //Renderer.ray = new Ray(Camera.pos, Camera.focus);
                    //System.out.println(Renderer.ray.origin.toString() + ",  " + Renderer.ray.end.toString());
                    break;
                case KeyEvent.VK_R:
                    R.resetModelFlag = true;
                    break;
                case KeyEvent.VK_C:
                    Camera.reset();
                    break;
                case KeyEvent.VK_W:
                    Main.loadColorWheel();
                    break;
            }
        }

        if(e.isAltDown()) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_1:
                    ColorModel.drawMode = ColorModel.DrawMode.RGB;
                    break;
                case KeyEvent.VK_2:
                    ColorModel.drawMode = ColorModel.DrawMode.CMYK;
                    break;
                case KeyEvent.VK_3:
                    ColorModel.drawMode = ColorModel.DrawMode.ALL;
                    break;
                case KeyEvent.VK_A:
                    R.displayAxes = !R.displayAxes;
                    break;
            }
        }

        switch (e.getKeyCode()) {
            case KeyEvent.VK_O:
                Renderer.setEnableVBO(!R.enabledVBO);
                break;
            case KeyEvent.VK_D:
                R.debugData = !R.debugData;
                break;
        }
    }
}
