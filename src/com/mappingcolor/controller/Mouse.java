package com.mappingcolor.controller;

import com.jogamp.newt.event.MouseEvent;
import com.jogamp.newt.event.MouseListener;
import com.mappingcolor.Main;
import com.mappingcolor.R;
import com.mappingcolor.model.Camera;

import javax.swing.*;
import java.awt.*;

public class Mouse implements MouseListener {
    // BUTTON1 = left mouse click
    // BUTTON2 = mouse wheel click
    // BUTTON3 = right mouse click

    private static boolean button1Down = false;
    private static boolean button3Down = false;

    private static Point location = new Point();
    private static Point screenLocation = new Point();
    private static Point lastScreenLocation = new Point();
    private static Point startSelectionPoint = new Point();
    private static Point endSelectionPoint = new Point();
    private static Point clickPoint = new Point();

    private static Robot robot;

    public static boolean isButton1Down() {
        return button1Down;
    }

    public static boolean isButton3Down() {
        return button3Down;
    }

    public static int getLocationX() {
        return location.x;
    }

    public static int getLocationY() {
        return location.y;
    }

    public static int getStartSelectionPointX() {
        return startSelectionPoint.x;
    }

    public static int getStartSelectionPointY() {
        return startSelectionPoint.y;
    }

    public static int getEndSelectionPointX() {
        return endSelectionPoint.x;
    }

    public static int getEndSelectionPointY() {
        return endSelectionPoint.x;
    }

    public static int getClickPointX() {
        return clickPoint.x;
    }

    public static int getClickPointY() {
        return clickPoint.y;
    }

    static {
        try {
            robot = new Robot();
        } catch (AWTException awtE) {
            awtE.printStackTrace();
        }
    }

    @Override
    public void mouseWheelMoved(MouseEvent e) {
        Camera.zoom(e.getRotation()[1] * Camera.ZOOM_DELTA);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if(!R.dataChanged) {
            lastScreenLocation.setLocation(screenLocation);

            if (button1Down) { // left click
                if (Keyboard.isAltKeyPressed()) {
                    if (Keyboard.isCtrlKeyPressed())
                        Camera.move(e.getX() - location.x, e.getY() - location.y);
                    else
                        Camera.rotate(e.getY() - location.y, e.getX() - location.x);
                    //Camera.orbit(e.getX() - location.x, e.getY() - location.y);
                }
            }

            if (button3Down) { //right click
                if (Keyboard.isAltKeyPressed()) {
                    float x = e.getX() - location.x; // is location the last click?
                    float y = (location.y - e.getY());
                    Camera.move(x, y);
                }
            }

            location.setLocation(new Point(e.getX(), e.getY()));

            screenLocation.setLocation(e.getX(), e.getY());
            SwingUtilities.convertPointToScreen(screenLocation, Main.view.newtCanvasAWT);

            wrapMouseX();
            wrapMouseY();
        }
    }

    private void wrapMouseX() {
        if(screenLocation.x < lastScreenLocation.x && screenLocation.x == 0) {
            robot.mouseMove(R.SCREEN_WIDTH - 1, screenLocation.y);
            location.x += R.SCREEN_WIDTH - 1;
        }
        else if(screenLocation.x > lastScreenLocation.x && screenLocation.x == R.SCREEN_WIDTH) {
            robot.mouseMove(1, screenLocation.y);
            location.x -= R.SCREEN_WIDTH + 1;
        }
    }

    private void wrapMouseY() {
        if(screenLocation.y < lastScreenLocation.y && screenLocation.y == 0) {
            robot.mouseMove(screenLocation.x, R.SCREEN_HEIGHT - 1);
            location.y += R.SCREEN_HEIGHT - 1;
        }
        else if(screenLocation.y > lastScreenLocation.y && screenLocation.y == R.SCREEN_HEIGHT) {
            robot.mouseMove(screenLocation.x, 1);
            location.y -= R.SCREEN_HEIGHT + 1;
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        location.setLocation(new Point(e.getX(), e.getY()));
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        clickPoint.setLocation(new Point(e.getX(), e.getY()));
    }

    @Override
    public void mousePressed(MouseEvent e) {
        switch (e.getButton()) {
            case MouseEvent.BUTTON1:
                if (!Keyboard.isKeyPressed())
                    R.selectColorFlag = true;
                //else if(Keyboard.isAltKeyPressed())
                //    Camera.arcBall.click(e.getX(), e.getY());

                button1Down = true;
                break;
            case MouseEvent.BUTTON3:
                if (!Keyboard.isKeyPressed())
                    R.selectColorFlag = true;

                button3Down = true;
                break;
        }

        startSelectionPoint.setLocation(new Point(e.getX(), e.getY()));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        switch (e.getButton()) {
            case MouseEvent.BUTTON1:
                R.selectColorFlag = false;
                button1Down = false;
                break;
            case MouseEvent.BUTTON3:
                R.selectColorFlag = false;
                button3Down = false;
                break;
        }

        endSelectionPoint.setLocation(new Point(e.getX(), e.getY()));
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
}