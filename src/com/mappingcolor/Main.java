package com.mappingcolor;

import com.mappingcolor.gui.LoadingScreen;
import com.mappingcolor.gui.WindowActivationListener;
import com.mappingcolor.gui.colorwheel.ColorWheelPanel;
import com.mappingcolor.model.ColorModel;
import com.mappingcolor.gui.MyView;
import com.mappingcolor.model.MyColor;

import javax.swing.*;
import javax.swing.text.Position;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class Main
{
    public static final Dimension MIN_GL_WINDOW_SIZE = new Dimension(784, 539);
    public static final Dimension MIN_SLIDE_PANEL_SIZE = new Dimension(220, 354);

    public static MyView view;

    public static void main(String []args) throws Exception
    {
        LoadingScreen loadingScreen = new LoadingScreen();
        loadingScreen.showSplash();

        checkTempDir();

        //if(OsChecker.isMac())
        //    System.setProperty("com.apple.mrj.application.apple.menu.about.name", R.applicationName);

        // panel build 1

        view = new MyView(R.applicationName);

        view.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentMoved(ComponentEvent e) {
                view.aboutPopup.setLocationRelativeTo(view);
            }

            @Override
            public void componentResized(ComponentEvent e) {
                view.aboutPopup.setLocationRelativeTo(view);
            }
        });

        view.addWindowListener(new WindowActivationListener(view));

        view.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                exit();
            }
        });

        view.setVisible(true);
        view.setLocationRelativeTo(null);

        view.newtCanvasAWT.requestFocusInWindow();

        loadingScreen.hideSplash();
    }

    public static void exit() {
        view.dispose();
        view.glWindow.destroy();
        System.exit(0);
    }

    public static void loadColorWheel() {
        JFrame frame = new JFrame();
        frame.setTitle("Color Wheel");
        //frame.add(new ColorWheelPanel());
        frame.pack();
        frame.setVisible(true);
    }

    private static void checkTempDir() {
        File tempDir = new File(R.tempDir);

        if (!tempDir.exists()) {
            try{
                tempDir.mkdir();
                System.out.println("Temp directory created: " + R.tempDir);
            } catch(SecurityException e){
                e.printStackTrace();
            }
        }
        else {
            System.out.println("Temp directory exists: " + R.tempDir);
        }
    }
}
